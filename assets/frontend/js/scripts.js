 jQuery.event.special.touchstart = {
     setup: function(_, ns, handle) {
         if (ns.includes("noPreventDefault")) {
             this.addEventListener("touchstart", handle, { passive: false });
         } else {
             this.addEventListener("touchstart", handle, { passive: true });
         }
     }
 };
 $(window).on('load', function() {
     $('#preloader').fadeOut(1000);
 });

 $('.single-product-carousel').owlCarousel({
     loop: true,
     margin: 20,
     autoplay: true,
     autoplayTimeout: 7000,
     autoplayHoverPause: true,
     nav: true,
     dots: false,
     animateOut: "fadeOut",
     animateIn: "fadeIn",
     navText: ["<i class='la la-angle-left'></i>", "<i class='la la-angle-right'></i>"],
     items: 1,
     thumbs: true,
     thumbImage: true,
     thumbContainerClass: 'owl-thumbs',
     thumbItemClass: 'owl-thumb-item'
 });
 $('.product-carousel').owlCarousel({
     loop: true,
     margin: 10,
     smartSpeed: 700,
     autoplay: true,
     autoplayTimeout: 5000,
     autoplayHoverPause: true,
     nav: true,
     navText: ["<i class='la la-angle-left'></i>", "<i class='la la-angle-right'></i>"],
     dots: false,
     responsive: {
         0: {
             items: 2
         },
         600: {
             items: 3
         },
         1000: {
             items: 4
         }
     }
 });

 $(document).ready(function() {

     var pageActiveUrl = window.location.pathname;
     pageActiveUrl = pageActiveUrl.replace(/\//g, '-');

     $('.menu' + pageActiveUrl).addClass('active');

     if (Cookies.get('PersonalDataCookie') != null && Cookies.get('PersonalDataCookie') == '1') {
         $('.cookie-fixed').css('display', 'none');
     }

     $('.whatsapp-area .w-icon').click(function() {
         $('.whatsapp-area .w-popup').slideDown();
     });

     $('.whatsapp-area .w-popup .head .close').click(function() {
         $('.whatsapp-area .w-popup').slideUp();
     });

     $('.cookie-close').click(function() {
         $('.cookie-fixed').addClass("hide");
         Cookies.set('PersonalDataCookie', '1', {
             expires: 1
         })
     });

     $(".header").headroom({
         "offset": 205,
         "tolerance": 10,
     });

     $(".header .menu>ul>li").click(function(e) {

         let active_sebmenu = $(".sub-menu.open").attr("data-menu");
         let click_sebmenu = $(this).find(".sub-menu").attr("data-menu");

         $(".overlay").addClass("open");
         $(".sub-menu").removeClass("open");
         $(this).find(".sub-menu").addClass("open");

         if (click_sebmenu == active_sebmenu) {
             $(".sub-menu").removeClass("open");
             setTimeout(function() {
                 $(".overlay").removeClass("open");
             }, 500);
         }
     });

     $('.sub-page').click(function() {
         let overlay_class = $('.overlay').attr('class');
         if (overlay_class == 'overlay open') {
             $(".sub-menu").removeClass("open");
             $('.overlay').removeClass('open');
         }
     });

     // Mobile menu hamburger function
     if (window.matchMedia("(max-width: 768px)").matches) {
         mobileMenuTL = gsap.timeline({
             paused: true
         });
         mobileMenuTL.to(".header .menu", {
             opacity: 1,
             height: "auto",
         });
         mobileMenuTL.from(".header .menu>ul>li>a", {
             opacity: 0,
             y: 50,
             stagger: .02,
             delay: -0.50
         });
         $(".header").on("click", '.hamburger.bars', function() {
             mobileMenuTL.play();
             $(this).find(".la").removeClass("la-bars");
             $(this).find(".la").addClass("la-times");
             $(this).addClass("closed");
             $(this).removeClass("bars");
         });

         $(".header").on("click", '.hamburger.closed', function() {
             $(this).addClass("bars");
             $(this).removeClass("closed");
             $(this).find(".la").removeClass("la-times");
             $(this).find(".la").addClass("la-bars");
             mobileMenuTL.reverse();
         });


         $(".sub-page .filter .filter-box").on("click", 'h4', function() {
             $(this).parent().toggleClass("open")
             $(this).toggleClass("open")
         });

         $(".sub-page .filter").on("click", 'h2', function() {
             $(this).parent().toggleClass("open")
         });
     }


     // Just for close shopping cart
     $("#closeBasket").click(function() {
         $('.basket-modal').removeClass('modalOpen');
         $('.top-right-menu .shopping-cart').removeClass('active');
         $('.shopping-cart').attr('onclick', 'basket_view("open")');

     });
     // Search modal open/close function
     $("#searchBtn").click(function() {
         $('.search-modal').toggleClass('open');
         $('.search-key').focus();
         if ($('.top-right-menu #searchBtn').hasClass('active')) {
             $(this).removeClass('active');
         } else {
             $(this).addClass('active');
             basket_view('close');
         }
     });

     $('.search-key').keypress(function(e) {
         if (e.which == 13) {
             search();
             return false;
         }
     });

 })

 var getUrlParameter = function getUrlParameter(sParam) {
     var sPageURL = window.location.search.substring(1),
         sURLVariables = sPageURL.split('&'),
         sParameterName,
         i;

     for (i = 0; i < sURLVariables.length; i++) {
         sParameterName = sURLVariables[i].split('=');

         if (sParameterName[0] === sParam) {
             return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
         }
     }
 };

 function number_format(val) {
     val += '';
     x = val.split('.');
     x1 = x[0];
     x2 = x.length > 1 ? ',' + x[1] : ',00';
     var rgx = /(\d+)(\d{3})/;
     while (rgx.test(x1)) {
         x1 = x1.replace(rgx, '$1' + '.' + '$2');
     }
     return x1 + x2;
 }

 function basket_view(val) {

     if (val == 'open') {
         $('.products').html('<div class="lds-dual-ring"></div>');
         $('.basket-modal').addClass('modalOpen');
         $('.top-right-menu .shopping-cart').addClass('active');
         $('.shopping-cart').attr('onclick', 'basket_view("close")');

         $.ajax({
             url: base_url + 'ajax/view_basket',
             type: 'GET',
             dataType: 'JSON',
             success: function(data) {
                 if (data) {
                     $('.products').html('');

                     let photo = '';
                     let price = '';

                     $.each(data, function(index, item) {

                         if (item.PhotoName) {
                             photo = item.PhotoName
                         } else if (item.ProductPhoto) {
                             photo = item.ProductPhoto
                         } else {
                             photo = 'upload/product/null-photo.png'
                         }

                         if (currency == 'second') {
                             price = item.ProductOptionSecondLatestPrice + currency_icon;
                         } else {
                             price = item.ProductOptionLatestPrice + currency_icon;
                         }

                         $('.products').append(
                             '<div class="product-item">' +
                             '<div class="container">' +
                             '<div class="row">' +
                             '<div class="col-md-4 col-4">' +
                             '<img src="' + server_url + photo + '" class="d-block w-100">' +
                             '</div>' +
                             '<div class="col-md-8 col-8 pl-0">' +
                             '<div class="p-title"><b>' + item.ProductName + '</b></div>' +
                             '<div id="basket-options"></div>' +
                             '<div class="quantity"><b>Adet:</b> ' + item.BasketProductQuantity + '</div>' +
                             '<div class="price"><b>Fiyat:</b> ' + price + '</div>' +
                             '</div>' +
                             '</div>' +
                             '</div>' +
                             '</div>'
                         );

                         $.each(JSON.parse(item.ProductOptionDetails), function(key, val) {
                             $('#basket-options').append(
                                 '<div>' + val.group + ': ' + val.option + '</div>'
                             );
                         });
                     });
                 } else {
                     $('.products').html('<p class="text-center p-3">Sepette ürün bulunmamaktadır.</p>');
                 }
             },
             error: function(jqXHR, textStatus, errorThrown) {
                 alert('Error get data from ajax');
             }
         });
     } else {
         $('.top-right-menu .shopping-cart').removeClass('active');
         $('.basket-modal').removeClass('modalOpen');

         $('.shopping-cart').attr('onclick', 'basket_view("open")');
     }

 }

 function search() {
     let search_key = $('.search-key').val();
     window.location.href = base_url + 'kategori/tumu?search=' + search_key;
 }

 (function() {
     new InstagramFeed({
         'username': 'petshopevinde',
         'container': document.getElementById("instagram-feed"),
         'display_profile': true,
         'display_biography': true,
         'display_gallery': true,
         'callback': null,
         'styling': true,
         'items': 5,
         'items_per_row': 5,
         'margin': .5,
         'image_size': 240,
         'lazy_load': true
     });

     $('[data-toggle="tooltip"]').tooltip()

 })();