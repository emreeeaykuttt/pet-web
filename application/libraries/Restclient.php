<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restclient {
    var $API = "";

    function __construct() {
        $this->API = API_URL;
        // $this->apiKey = 'X-API-KEY';
        // $this->apiUser = "admin";
        // $this->apiPass = "1234";
    }

    function get($param = null, $end_point)
    {
        if ($param == null) 
        {
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $this->API . $end_point);
        } 
        else 
        {
            $params = '?';
            foreach($param as $key => $value) {
                $params .= $key . '='. $value . '&';
            }
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $this->API . $end_point . $params);
        }

        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            
        if (isset($_SESSION['UserToken'])) 
        {
            $headers = array(
                'Authorization: EA278602222342875160EA' . $_SESSION['UserToken'],
            );
            curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
        }

        $buffer = curl_exec($curl_handle);
        return $buffer;
    }

    function post($array, $end_point) 
    {
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $this->API . $end_point);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $array);

        if (isset($_SESSION['UserToken'])) 
        {
            $headers = array(
                'Authorization: EA278602222342875160EA' . $_SESSION['UserToken'],
            );
            curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
        }

        $buffer = curl_exec($curl_handle);
        return $buffer;
    }

    function put($array, $end_point) 
    {
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $this->API . $end_point);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, http_build_query($array));
        curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "PUT");

        if (isset($_SESSION['UserToken'])) 
        {
            $headers = array(
                'Authorization: EA278602222342875160EA' . $_SESSION['UserToken'],
            );
            curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
        }

        $buffer = curl_exec($curl_handle);
        return $buffer;
    }

    function delete($id, $end_point) 
    {
        $array = array("item_id" => $id);
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $this->API . $end_point);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, http_build_query($array));
        curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "DELETE");

        if (isset($_SESSION['UserToken'])) 
        {
            $headers = array(
                'Authorization: EA278602222342875160EA' . $_SESSION['UserToken'],
            );
            curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
        }
        
        $buffer = curl_exec($curl_handle);
        return $buffer;
    }
}