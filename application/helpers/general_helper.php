<?php 

function data_group_by($key, $data) 
{
    $result = array();

    foreach($data as $val) {
        if(array_key_exists($key, $val)){
            $result[$val[$key]][] = $val;
        }else{
            $result[""][] = $val;
        }
    }

    return $result;
}

function number_format_render($val)
{
	return number_format($val, 2, ',', '.');
}

function datetime_format_render($val)
{
    $datetime = date_create($val);
    return date_format($datetime, 'Y.m.d H:i');
}

function url_param_render($val)
{
    return str_replace(' ', '+', $val);
}

function discount_render($val)
{
    $discount = explode('.', $val);
    $discount_right = '';

    if (floatval($discount[1]) > 0)
    {
        $discount_right = '.' . $discount[1];
    }
    
    return $discount[0] . $discount_right;
}

function option_name($val)
{
    $ci =& get_instance();
    $ci->load->model('product/option_model', 'option');
    $get_lang_id = $ci->get_lang_id();
    $ci->option->lang_id = $get_lang_id;
    $data = $ci->option->getByContentID($val);

    return '<b>' . $data['GroupName'] . ':</b> ' . $data['OptionName'];
}

?>