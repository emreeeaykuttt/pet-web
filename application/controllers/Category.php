<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
		$this->load->model('product/product_model', 'product');
		$this->load->model('product/category_model', 'category');
	}

	public function index($cateory_title_and_id = '')
	{
		$this->load->model('product/option_group_model', 'option_group');
		$this->load->model('product/tag_model', 'tag');
		$this->load->library('pagination');

		$get_parameter = explode('-', $cateory_title_and_id);
		$category_content_id = end($get_parameter);

		// pagination config (start)
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;
		$config = array();
		$config['base_url'] = base_url() .'kategori/' . $cateory_title_and_id;
		$config['per_page'] = 21;
		$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav></div>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></li>';
		$config['prev_tag_open'] = '<li class="page-item"> ';
		$config['prev_tagl_close'] = '</li>';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tagl_close'] = '</li>';
		$config['prev_link'] = '<i class="la la-angle-left"></i>';
		$config['next_link'] = '<i class="la la-angle-right"></i>';
		$config['attributes'] = array('class' => 'page-link');
        // pagination config (end)

		$get_lang_id = $this->get_lang_id();
		$this->category->lang_id = $get_lang_id;
		$this->product->lang_id = $get_lang_id;
		$this->product->per_page = $config['per_page'];
		$this->product->page = $page;

		if ($this->input->get('filter')) 
		{
			$this->product->filter = $this->input->get('filter');
		}

		if ($this->input->get('min') && $this->input->get('max')) 
		{
			$this->product->min = $this->input->get('min');
			$this->product->max = $this->input->get('max');
		}

		if ($this->input->get('tag')) 
		{
			$this->product->tag = $this->input->get('tag');
		}

		if ($this->input->get('search')) 
		{
			$this->product->search = $this->input->get('search');
		}

		if ($this->input->get('sort')) 
		{
			$this->product->sort = $this->input->get('sort');
		}

		if ($category_content_id > 0) 
		{
			$products = $this->product->getAllByCategoryID($category_content_id);
		}
		elseif ($category_content_id == 'all' || $category_content_id == 'tumu') 
		{
			$products = $this->product->getAll();
		}
		else
		{
			redirect(base_url()); 
			exit;
		}

		$categories = $this->category->getAll();
		$data['categories'] = isset($categories['status']) ? NULL : json_encode($categories);
		$data['products_by_category'] = isset($products['status']) ? NULL : $products['products'];
		$data['parent_category_slug'] = isset($products['status']) ? NULL : $products['parent_category_slug'];
		$data['category_name'] = isset($products['status']) ? NULL : $products['category_name'];
		$data['options_group'] = isset($products['status']) ? NULL : $products['options_group'];

		$config['reuse_query_string'] = true;
		$config['total_rows'] = $products['products_total'];
		$config['first_link'] = 1;
    	$config['last_link'] = ceil($products['products_total'] / $config['per_page']);
		$this->pagination->initialize($config);
		$str_links = $this->pagination->create_links();
		$data['links'] = explode('&nbsp;', $str_links);

		$this->load->view('frontend/product/list_view', $data);		
	}
	
}


?>