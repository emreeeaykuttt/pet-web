<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function product_option_change()
	{
		$this->load->model('product/product_option_model','product_option');

		$post = $this->input->post(null, true);

		$get_lang_id = $this->get_lang_id();
		$this->product_option->lang_id = $get_lang_id;

		$product_option = $this->product_option->getByProductContentIDAndOptionContentIDs($post['ProductContentID'], implode(',',$post['ProductOptionOptionContentIDs']));
		$product_option = !isset($product_option['status']) ? $product_option : '';

		echo json_encode($product_option);
	}

	public function add_to_basket()
	{
		$this->load->model('product/product_option_model','product_option');
		$this->load->model('product/basket_model', 'basket');

		$post = $this->input->post(null, true);

		$get_lang_id = $this->get_lang_id();
		$this->product_option->lang_id = $get_lang_id;
		$product_option = $this->product_option->getByProductContentIDAndContentID($post['ProductContentID'], $post['ProductOptionContentID']);

		if ($product_option['ProductOptionQuantity'] > 0 || $product_option['ProductOptionUnlimited'] == 1) 
		{	
			if (empty($this->session->userdata('UserID'))) 
			{
				if (empty($this->session->userdata('UnregisteredID'))) 
				{
					$this->session->set_userdata('UnregisteredID', md5(md5(uniqid(mt_rand(), true)) . date("mdY_His")));	
				}

				$user_id = $this->session->userdata('UnregisteredID');
				$is_unregistered_id = true;
				$is_user_id = false;
			}
			else
			{
				$user_id = $this->session->userdata('UserID');
				$is_unregistered_id = false;
				$is_user_id = true;
			}

			$this->basket->product_option_content_id = $post['ProductOptionContentID'];
			$basket = $this->basket->getByUserID($user_id);
			$basket = isset($basket['status']) ? NULL : $basket;

			if ($basket) 
			{
				$quantity = intval($basket['BasketProductQuantity']) + intval($post['ProductOptionQuantity']);
			}
			else
			{
				$quantity = intval($post['ProductOptionQuantity']);	
			}

			if ($quantity > intval($product_option['ProductOptionQuantity']) && $product_option['ProductOptionUnlimited'] != 1)
			{
				echo json_encode(
					array(
						'status' => false, 
						'message' => 'Stokta bu kadar ürün yok. Stok sayısı: ' . $product_option['ProductOptionQuantity'], 
						'basket_total_product' => $this->session->userdata('UserBasketTotalProduct'),
					)
				);
				exit();
			}

			if ($basket) 
			{
				$data = array(
					'BasketProductID' => $post['ProductID'],
					'BasketProductContentID' => $post['ProductContentID'],
					'BasketProductOptionID' => $post['ProductOptionID'],
					'BasketProductOptionContentID' => $post['ProductOptionContentID'],		
					'BasketProductQuantity' => $quantity,
					'BasketUserID' => $is_user_id ? $user_id : NULL,
					'BasketUnregisteredID' => $is_unregistered_id ? $user_id : NULL,
				);
			
				$result = $this->basket->update($data, $basket['BasketID'], $user_id);
			}
			else
			{
				$data = array(
					'BasketProductID' => $post['ProductID'],
					'BasketProductContentID' => $post['ProductContentID'],
					'BasketProductOptionID' => $post['ProductOptionID'],
					'BasketProductOptionContentID' => $post['ProductOptionContentID'],
					'BasketProductQuantity' => $quantity,
					'BasketUserID' => $is_user_id ? $user_id : NULL,
					'BasketUnregisteredID' => $is_unregistered_id ? $user_id : NULL,
				);
				
				$result = $this->basket->save($data, $user_id);
			}

			$this->session->set_userdata('UserBasketTotalProduct', $result['basket_total_product']);
		}
		else
		{
			echo json_encode(
				array(
					'status' => false, 
					'message' => 'Ürün sepete eklenirken bir hata oluştu', 
					'basket_total_product' => $this->session->userdata('UserBasketTotalProduct')
				)
			);
			exit();
		}

		echo json_encode($result);
	}

	public function update_basket($basket_id)
	{
		$this->load->model('product/product_option_model','product_option');
		$this->load->model('product/basket_model', 'basket');

		$post = $this->input->post(null, true);
		$product_content_id = $post['ProductContentID'][$basket_id];
		$product_option_content_id = $post['ProductOptionContentID'][$basket_id];
		$product_option_quantity = $post['ProductOptionQuantity'][$basket_id];

		$get_lang_id = $this->get_lang_id();
		$this->product_option->lang_id = $get_lang_id;
		$product_option = $this->product_option->getByProductContentIDAndContentID($product_content_id, $product_option_content_id);

		if ($product_option['ProductOptionQuantity'] > 0 || $product_option['ProductOptionUnlimited'] == 1) 
		{
			if (empty($this->session->userdata('UserID'))) 
			{
				if (empty($this->session->userdata('UnregisteredID'))) 
				{
					$this->session->set_userdata('UnregisteredID', md5(md5(uniqid(mt_rand(), true)) . date("mdY_His")));	
				}

				$user_id = $this->session->userdata('UnregisteredID');
				$is_unregistered_id = true;
				$is_user_id = false;
			}
			else
			{
				$user_id = $this->session->userdata('UserID');
				$is_unregistered_id = false;
				$is_user_id = true;
			}

			$this->basket->product_option_content_id = $product_option_content_id;
			$basket = $this->basket->getByUserID($user_id);
			$basket = isset($basket['status']) ? NULL : $basket;

			if ($basket) 
			{
				$quantity = intval($product_option_quantity);

				if ($quantity > intval($product_option['ProductOptionQuantity']) && $product_option['ProductOptionUnlimited'] != 1)
				{
					echo json_encode(
						array(
							'status' => false, 
							'message' => 'Stokta bu kadar ürün yok. Stok sayısı: ' . $product_option['ProductOptionQuantity'], 
							'basket_total_product' => $this->session->userdata('UserBasketTotalProduct'),
						)
					);
					exit();
				}

				$data = array(
					'BasketProductID' => $post['ProductID'][$basket_id],
					'BasketProductContentID' => $product_content_id,
					'BasketProductOptionID' => $post['ProductOptionID'][$basket_id],
					'BasketProductOptionContentID' => $product_option_content_id,
					'BasketProductQuantity' => $quantity,
					'BasketUserID' => $is_user_id ? $user_id : NULL,
					'BasketUnregisteredID' => $is_unregistered_id ? $user_id : NULL,
				);
				
				$result = $this->basket->update($data, $basket_id, $user_id);

				echo json_encode($result);
			}
			else
			{
				echo json_encode(
					array(
						'status' => false, 
						'message' => 'Sepetteki ürün güncellenirken bir hata oluştu', 
						'basket_total_product' => $this->session->userdata('UserBasketTotalProduct')
					)
				);
				exit();
			}
		}
		else
		{
			echo json_encode(
				array(
					'status' => false, 
					'message' => 'Sepetteki ürün güncellenirken bir hata oluştu', 
					'basket_total_product' => $this->session->userdata('UserBasketTotalProduct')
				)
			);
			exit();
		}
	}

	public function view_basket()
	{
		$this->load->model('product/basket_model', 'basket');

		$user_id = $this->session->userdata('UserID') ? $this->session->userdata('UserID') : $this->session->userdata('UnregisteredID');
		$get_lang_id = $this->get_lang_id();
		$this->basket->lang_id = $get_lang_id;
		$products_in_basket = $this->basket->getAllByUserID($user_id);
		$products_in_basket = isset($products_in_basket['status']) ? NULL : $products_in_basket;

		echo json_encode($products_in_basket);
	}

	public function destroy_basket($basket_id)
	{
		$this->load->model('product/basket_model', 'basket');

		$user_id = $this->session->userdata('UserID') ? $this->session->userdata('UserID') : $this->session->userdata('UnregisteredID');
		$result = $this->basket->deleteByIDAndUserID($basket_id, $user_id);
		$total_product = $this->session->userdata('UserBasketTotalProduct');

		if ($result['status']) 
		{
			$total_product = intval($total_product) - 1;
			$this->session->set_userdata('UserBasketTotalProduct', $total_product);
		}

		$result['basket_total_product'] = $total_product;

		echo json_encode($result);
	}

	public function view_coupon($code)
	{
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}
		
		$this->load->model('user/coupon_model', 'coupon');

		$user_id = $this->session->userdata('UserID');
		$get_lang_id = $this->get_lang_id();
		$this->coupon->lang_id = $get_lang_id;
		$code = $code ? $code : 'none';
		$data = $this->coupon->getByCodeAndUserID($code, $user_id);
		$data = !isset($data['status']) ? $data : '';
		
		echo json_encode($data);
	}

	public function view_address($id)
	{	
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('user/address_model', 'address');

		$user_id = $this->session->userdata('UserID');
		$get_lang_id = $this->get_lang_id();
		$this->address->lang_id = $get_lang_id;
		$data = $this->address->getByIDAndUserID($id, $user_id);
		
		echo json_encode($data);
	}

	public function add_address()
	{
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('user/address_model', 'address');

		$user_id = $this->session->userdata('UserID');
		$post = $this->input->post(null,true);

		$data = array(
            'AddressTitle' => $post['AddressTitle'],
            'AddressType' => $post['AddressType'],
            'AddressFirstName' => $post['AddressFirstName'],
            'AddressLastName' => $post['AddressLastName'],
            'AddressCountryID' => $post['AddressCountryID'],
            'AddressCityID' => $post['AddressCityID'],
            'AddressDistrictID' => $post['AddressDistrictID'],
            'AddressAlternativeCity' => $post['AddressAlternativeCity'],
            'AddressOpenAddress' => $post['AddressOpenAddress'],
            'AddressIdentityNumber' => $post['AddressIdentityNumber'],
            'AddressZipCode' => $post['AddressZipCode'],
            'AddressPhone' => $post['AddressPhone'],
            'AddressEmail' => $post['AddressEmail'],
            'AddressBillType' => $post['AddressBillType'],
            'AddressCompanyName' => $post['AddressCompanyName'],
            'AddressCompanyTaxNumber' => $post['AddressCompanyTaxNumber'],
            'AddressCompanyTaxAdministration' => $post['AddressCompanyTaxAdministration'],
		);

		$result = $this->address->save($data, $user_id);
		
		echo json_encode($result);
	}

	public function update_address()
	{
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('user/address_model', 'address');

		$user_id = $this->session->userdata('UserID');
		$post = $this->input->post(null,true);

		$data = array(
			'AddressID' => $post['AddressID'],
            'AddressTitle' => $post['AddressTitle'],
            'AddressType' => $post['AddressType'],
            'AddressFirstName' => $post['AddressFirstName'],
            'AddressLastName' => $post['AddressLastName'],
            'AddressCountryID' => $post['AddressCountryID'],
            'AddressCityID' => $post['AddressCityID'],
            'AddressDistrictID' => $post['AddressDistrictID'],
            'AddressAlternativeCity' => $post['AddressAlternativeCity'],
            'AddressOpenAddress' => $post['AddressOpenAddress'],
            'AddressIdentityNumber' => $post['AddressIdentityNumber'],
            'AddressZipCode' => $post['AddressZipCode'],
            'AddressPhone' => $post['AddressPhone'],
            'AddressEmail' => $post['AddressEmail'],
            'AddressBillType' => $post['AddressBillType'],
            'AddressCompanyName' => $post['AddressCompanyName'],
            'AddressCompanyTaxNumber' => $post['AddressCompanyTaxNumber'],
            'AddressCompanyTaxAdministration' => $post['AddressCompanyTaxAdministration'],
		);
		
		$result = $this->address->update($data, $post['AddressID'], $user_id);
		
		echo json_encode($result);
	}

	public function destroy_address($address_id)
	{
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('user/address_model', 'address');

		$user_id = $this->session->userdata('UserID');
		$result = $this->address->deleteByIDAndUserID($address_id, $user_id);

		echo json_encode($result);
	}

	public function cities($country_id)
	{
		$this->load->model('db/city_model', 'city');
		$data = $this->city->getAllByCountryID($country_id);

		echo json_encode($data);
	}

	public function districts($city_id)
	{
		$this->load->model('db/district_model', 'district');
		$data = $this->district->getAllByCityID($city_id);

		echo json_encode($data);
	}

	public function update_order_address()
	{
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('order/order_model', 'order');

		$user_id = $this->session->userdata('UserID');
		$post = $this->input->post(null,true);

		$data = array(
			'UpdateWhich' => 'address',
			'OrderUserID' => $user_id,
            'OrderDeliveryFirstName' => $post['OrderDeliveryFirstName'],
            'OrderDeliveryLastName' => $post['OrderDeliveryLastName'],
            'OrderDeliveryIdentityNumber' => $post['OrderDeliveryIdentityNumber'],
            'OrderDeliveryZipCode' => $post['OrderDeliveryZipCode'],
            'OrderDeliveryEmail' => $post['OrderDeliveryEmail'],
            'OrderDeliveryPhone' => $post['OrderDeliveryPhone'],
            'OrderDeliveryCountry' => $post['OrderDeliveryCountry'],
            'OrderDeliveryCity' => $post['OrderDeliveryCity'],
            'OrderDeliveryDistrict' => $post['OrderDeliveryDistrict'],
            'OrderDeliveryOpenAddress' => $post['OrderDeliveryOpenAddress'],
            'OrderBillingFirstName' => $post['OrderBillingFirstName'],
            'OrderBillingLastName' => $post['OrderBillingLastName'],
            'OrderBillingIdentityNumber' => $post['OrderBillingIdentityNumber'],
            'OrderBillingZipCode' => $post['OrderBillingZipCode'],
            'OrderBillingEmail' => $post['OrderBillingEmail'],
            'OrderBillingPhone' => $post['OrderBillingPhone'],
            'OrderBillingCountry' => $post['OrderBillingCountry'],
            'OrderBillingCity' => $post['OrderBillingCity'],
            'OrderBillingDistrict' => $post['OrderBillingDistrict'],
            'OrderBillingOpenAddress' => $post['OrderBillingOpenAddress'],
            'OrderBillingCompanyName' => $post['OrderBillingCompanyName'],
            'OrderBillingCompanyTaxNumber' => $post['OrderBillingCompanyTaxNumber'],
            'OrderBillingCompanyTaxAdministration' => $post['OrderBillingCompanyTaxAdministration'],
		);
		
		$result = $this->order->update($data, $post['OrderID']);
		
		echo json_encode($result);
	}

	public function view_order($order_id)
	{	
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('order/order_model', 'order');

		$user_id = $this->session->userdata('UserID');
		$data = $this->order->getByIDAndUserID($order_id, $user_id);
		
		echo json_encode($data);
	}

	public function view_order_details($tracking_number)
	{	
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('order/detail_model', 'detail');

		$user_id = $this->session->userdata('UserID');
		$tracking_number_arr = explode('PSE', $tracking_number);
		$order_id = $tracking_number_arr[1];
		$data = $this->detail->getAllByOrderIDAndUserID($order_id, $user_id);
		
		echo json_encode($data);
	}

	public function view_coupon_detail($coupon_content_id)
	{	
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('user/coupon_model', 'coupon');

		$user_id = $this->session->userdata('UserID');
		$get_lang_id = $this->get_lang_id();
		$this->coupon->lang_id = $get_lang_id;
		$data = $this->coupon->getByContentIDAndUserID($coupon_content_id, $user_id);
		
		echo json_encode($data);
	}

	public function send_membership_invitation()
	{
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('user/auth_model', 'auth');

		$post = $this->input->post(null, true);
		$user_id = $this->session->userdata('UserID');

		$data = array(
			'UserID' => $user_id,
			'UserEmail' => $post['UserEmail'],
		);

		$result = $this->auth->invite($data);

		echo json_encode($result);
	}

	public function add_favorite($product_id, $product_content_id)
	{
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('user/favorite_model','favorite');

		$post = $this->input->post(null, true);
		$user_id = $this->session->userdata('UserID');

		$data = array(
			'FavoriteProductID' => $product_id,
			'FavoriteProductContentID' => $product_content_id,
		);

		$result = $this->favorite->save($data, $user_id);

		echo json_encode($result);
	}

	public function destroy_favorite($favorite_id)
	{
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('user/favorite_model','favorite');

		$user_id = $this->session->userdata('UserID');
		$result = $this->favorite->deleteByIDAndUserID($favorite_id, $user_id);

		echo json_encode($result);
	}

	public function cancel_payment($order_id)
	{
		if (!$this->session->userdata('UserID'))
		{
			$this->session->set_userdata('which_page', $_SERVER['HTTP_REFERER']);
			echo json_encode(array('status' => 'login'));
			die;
		}

		$this->load->model('order/order_model', 'order');
		$user_id = $this->session->userdata('UserID');

		$result = $this->order->update(array('UpdateWhich' => 'status', 'OrderStatus' => 'cancelled', 'OrderShippingStatus' => 'pending', 'OrderUserID' => $user_id), $order_id);

		echo json_encode($result);	
	}
}


?>