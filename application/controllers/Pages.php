<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
	}

	public function about()
	{
		$this->load->view('frontend/pages/about_view');
	}

	public function privacy_policy()
	{
		$this->load->view('frontend/pages/privacy_policy_view');
	}

	public function user_agreement()
	{
		$this->load->view('frontend/pages/user_agreement_view');
	}

	public function distance_sales_contract()
	{
		$this->load->view('frontend/pages/distance_sales_contract_view');
	}
	
	public function cancellation_and_refund_conditions()
	{
		$this->load->view('frontend/pages/cancellation_and_refund_conditions_view');
	}

	public function shipping_and_delivery_information()
	{
		$this->load->view('frontend/pages/shipping_and_delivery_information_view');
	}
	
	public function contact()
	{
		$this->load->view('frontend/pages/contact_view');
	}

	public function inspiration()
	{
		$this->load->view('frontend/pages/inspiration_view');
	}

	public function gift()
	{
		$this->load->view('frontend/pages/gift_view');
	}

	public function print_quality()
	{
		$this->load->view('frontend/pages/print_quality_view');
	}

	public function cookie_policy()
	{
		$this->load->view('frontend/pages/cookie_policy_view');
	}

}


?>