<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
	}

	public function index()
	{
		$this->load->model('slider/slider_model', 'slider');

		$get_lang_id = $this->get_lang_id();
		$this->slider->lang_id = $get_lang_id;

		$sliders = $this->slider->getAll();

		$data['sliders'] = isset($sliders['status']) ? NULL : $sliders;
		$data['products_by_tag1'] = $this->products_by_tag(8, 'gününfırsatları');
		$data['products_by_tag2'] = $this->products_by_tag(8, 'kampanyalar');
		$data['products_by_tag3'] = $this->products_by_tag(8, 'kedikumları');
		$data['products_by_tag4'] = $this->products_by_tag(8, 'avantajlıpaketler');
		$data['products_by_tag5'] = $this->products_by_tag(8, 'editörünseçimi');
		// $data['best_selling_products'] = $this->best_selling_products();
		$data['best_selling_products'] = $this->products_by_tag(8, 'çoksatanlar');

		$this->load->view('frontend/home_view', $data);
	}

	public function sitemap()
	{
		$this->load->model('product/product_model', 'product');
		$this->load->model('product/category_model', 'category');

		$this->product->lang_id = 1;
		$this->product->limit = 3000;
		$this->category->lang_id = 1;
		$result = $this->product->getAll();
		$products = !isset($result['status']) ? $result['products'] : '';
		$categories = $this->category->getAll();
		$categories = !isset($categories['status']) ? $categories : '';

		header('Content-Type: text/xml');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		echo "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";
		echo "<url><loc>" . base_url() . "</loc><changefreq>daily</changefreq><priority>1.0</priority></url>";
		echo "<url><loc>" . base_url() . "hakkimizda</loc><changefreq>daily</changefreq><priority>0.8</priority></url>";
		echo "<url><loc>" . base_url() . "iletisim</loc><changefreq>daily</changefreq><priority>0.8</priority></url>";
		echo "<url><loc>" . base_url() . "mesafeli-satis-sozlesmesi</loc><changefreq>daily</changefreq><priority>0.4</priority></url>";
		echo "<url><loc>" . base_url() . "kullanici-sozlesmesi</loc><changefreq>daily</changefreq><priority>0.4</priority></url>";
		echo "<url><loc>" . base_url() . "gizlilik-politikasi</loc><changefreq>daily</changefreq><priority>0.4</priority></url>";
		echo "<url><loc>" . base_url() . "iptal-ve-iade-sartlari</loc><changefreq>daily</changefreq><priority>0.4</priority></url>";
		echo "<url><loc>" . base_url() . "kargo-ve-teslimat-bilgileri</loc><changefreq>daily</changefreq><priority>0.4</priority></url>";
		echo "<url><loc>" . base_url() . "kategori/tumu</loc><changefreq>daily</changefreq><priority>0.8</priority></url>";

		foreach ($categories[3] as $key => $category)
		{
			echo "<url><loc>".  base_url() . "kategori/" . $category['CategorySlug'] . "</loc><changefreq>daily</changefreq><priority>0.6</priority></url>";
		}

		$product_arr['ProductName'] = '';
		foreach ($products as $key => $product)
		{
			if (!empty($product['ProductPhoto']))
			{
				if (!in_array($product['ProductName'], $product_arr))
				{
					echo "<url><loc>".  base_url() . "urun/" . $product['ProductSlug'] . "</loc><changefreq>daily</changefreq><priority>0.6</priority></url>";
				}

				$product_arr[] = $product['ProductName'];
			}
		}

		echo "</urlset>";
	}

}


?>