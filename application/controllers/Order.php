<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->session->userdata('UserToken'))
		{
			$uri_arr = explode('/', $_SERVER['REQUEST_URI']);
			if ($uri_arr['1'] == 'order' && $uri_arr['2'] != 'success') 
			{
				$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

				if ($_SERVER['REQUEST_URI'] == '/order/delivery') 
				{
					redirect(base_url() . 'uyelik/misafir');
				}
				else
				{
					redirect(base_url() . 'uyelik/giris-yap');
				}
				exit();
			}
		}

		$this->autorun();
		$this->load->model('order/order_model', 'order');
		$this->load->model('order/detail_model', 'detail');
	}

	public function delivery()
	{
		$this->load->model('user/address_model', 'address');
		$this->load->model('product/basket_model', 'basket');
		$this->load->model('db/country_model', 'country');
		$this->load->model('user/coupon_model', 'coupon');

		$user_id = $this->session->userdata('UserID');
		$get_lang_id = $this->get_lang_id();
		$coupon_code = $this->input->get('coupon') ? $this->input->get('coupon') : 'none';
		$this->address->lang_id = $get_lang_id;
		$this->basket->lang_id = $get_lang_id;
		$this->country->lang_id = $get_lang_id;
		$this->coupon->lang_id = $get_lang_id;

		$address = $this->address->getAllByUserID($user_id);
		$data['address'] = isset($address['status']) ? NULL : $address;
		$coupon = $this->coupon->getByCodeAndUserID($coupon_code, $user_id);
		$data['coupon'] = isset($coupon['status']) ? NULL : $coupon;
		$data['countries'] = $this->country->getAll();

		$products_in_basket = $this->basket->getAllByUserID($user_id);

		if (!isset($products_in_basket['status'])) 
		{
			$data['products_in_basket'] = $products_in_basket;
			
			$this->load->view('frontend/order/delivery_view', $data);
		}
		else
		{
			redirect(base_url());
			die;
		}
	}

	public function payment()
	{
		$this->load->model('user/address_model', 'address');

		$user_id = $this->session->userdata('UserID');
		$post = $this->input->post(null, true);
		if ($post) 
		{
			$coupon_code = isset($post['CouponCode']) ? $post['CouponCode'] : 'none'; 
			$delivery_address_id = $post['Delivery'];
			$billing_address_id = $post['Billing'];
			$payment_type = $post['PaymentType'];
			$distance_sales_contract = isset($post['DistanceSalesContract']) ? 1 : '';
			$get_lang_id = $this->get_lang_id();
			$this->address->lang_id = $get_lang_id;

			$delivery_address = $this->address->getByIDAndUserID($delivery_address_id, $user_id);
			$delivery_address = isset($delivery_address['status']) ? NULL : $delivery_address;

			if ($delivery_address_id == $billing_address_id)
			{
				$billing_address = $delivery_address;
			}
			else
			{
				$billing_address = $this->address->getByIDAndUserID($billing_address_id, $user_id);
				$billing_address = isset($billing_address['status']) ? NULL : $billing_address;
			}

			if (empty($payment_type) || $payment_type == '0') 
			{
				redirect(base_url('order/failed?error=payment'));
				die;
			}

			if (empty($distance_sales_contract) || $distance_sales_contract == '0') 
			{
				redirect(base_url('order/failed?error=contract'));
				die;
			}

			if (!empty($delivery_address) && !empty($billing_address)) 
			{
				$order_id = $this->add($coupon_code);
			}
			else
			{
				redirect(base_url('order/failed?error=address'));
				die;
			}

			$order_data = array(
				'UpdateWhich' => 'address_and_tracking_number',
				'OrderUserID' => $user_id,
				'OrderTrackingNumber' => 'PSE' . $order_id,
				'OrderPaymentType' => $payment_type,
			);

			if ($delivery_address) 
			{
				$order_data['OrderDeliveryFirstName'] = $delivery_address['AddressFirstName'];
				$order_data['OrderDeliveryLastName'] = $delivery_address['AddressLastName'];
				$order_data['OrderDeliveryCountry'] = $delivery_address['CountryName'];
				$order_data['OrderDeliveryCountryCode'] = $delivery_address['CountryCode'];
				$order_data['OrderDeliveryCity'] = !empty($delivery_address['CityName']) ? $delivery_address['CityName'] : $delivery_address['AddressAlternativeCity'];
				$order_data['OrderDeliveryDistrict'] = !empty($delivery_address['DistrictName']) ? $delivery_address['DistrictName'] : '';
				$order_data['OrderDeliveryOpenAddress'] = $delivery_address['AddressOpenAddress'];
				$order_data['OrderDeliveryIdentityNumber'] = $delivery_address['AddressIdentityNumber'];
				$order_data['OrderDeliveryZipCode'] = $delivery_address['AddressZipCode'];
				$order_data['OrderDeliveryPhone'] = $delivery_address['AddressPhone'];
				$order_data['OrderDeliveryEmail'] = $delivery_address['AddressEmail'];
			}

			if ($billing_address) 
			{
				$order_data['OrderBillingFirstName'] = $billing_address['AddressFirstName'];
				$order_data['OrderBillingLastName'] = $billing_address['AddressLastName'];
				$order_data['OrderBillingCountry'] = $billing_address['CountryName'];
				$order_data['OrderBillingCountryCode'] = $billing_address['CountryCode'];
				$order_data['OrderBillingCity'] = !empty($billing_address['CityName']) ? $billing_address['CityName'] : $billing_address['AddressAlternativeCity'];
				$order_data['OrderBillingDistrict'] = !empty($billing_address['DistrictName']) ? $billing_address['DistrictName'] : '';
				$order_data['OrderBillingOpenAddress'] = $billing_address['AddressOpenAddress'];
				$order_data['OrderBillingIdentityNumber'] = $billing_address['AddressIdentityNumber'];
				$order_data['OrderBillingZipCode'] = $billing_address['AddressZipCode'];
				$order_data['OrderBillingPhone'] = $billing_address['AddressPhone'];
				$order_data['OrderBillingEmail'] = $billing_address['AddressEmail'];
				$order_data['OrderBillingCompanyName'] = $billing_address['AddressCompanyName'];
				$order_data['OrderBillingCompanyTaxNumber'] = $billing_address['AddressCompanyTaxNumber'];
				$order_data['OrderBillingCompanyTaxAdministration'] = $billing_address['AddressCompanyTaxAdministration'];
			}
			
			$result = $this->order->update($order_data, $order_id);

			if ($result['status']) 
			{
				$this->basket->deleteByUserID($user_id);
				$this->session->unset_userdata('UserBasketTotalProduct');

				redirect(base_url('order/success/' . $order_id . '?payment=' . $payment_type));
			}
			else
			{
				redirect(base_url('order/failed'));
			}
		}
		else
		{
			redirect(base_url());
		}
	}

	public function success($order_id = '')
	{
		$user_id = $this->session->userdata('UserID');
		$token = $this->input->post('token');

		if (empty($token)) 
		{
			$detail = $this->detail->getAllByOrderIDAndUserID($order_id, $user_id);

			if (isset($detail['status'])) 
			{
				$order = NULL;
				$detail = NULL;
			}
			else
			{
				$order = $detail['order'];
				$detail = $detail['details'];
			}

			if (!empty($order) && $order['OrderStatus'] == 'pendingPayment') 
			{
				$iyzico = $this->iyzico($order, $detail);

				$data['order'] = $order;
				$data['iyzico'] = $iyzico;
				$this->load->view('frontend/order/success_view', $data);
			}
			else
			{
				redirect(base_url());
			}
		}
		else
		{
			IyzipayBootstrap::init();
			$option = new \Iyzipay\Options();
			$option->setApiKey("iWZdF42RbMXtSSMMRSO3AoGhcq9V8Fpm");
			$option->setSecretKey("FqM7OBaUmA9wpzZylo2ZxTHHMEjKms1e");
			$option->setBaseUrl("https://api.iyzipay.com");

			$return = new \Iyzipay\Request\RetrieveCheckoutFormRequest();
			$return->setLocale(\Iyzipay\Model\Locale::TR);
			$return->setToken($token);
	
			$checkoutForm = \Iyzipay\Model\CheckoutForm::retrieve($return, $option);

			$request = $checkoutForm;

			if($request->getPaymentStatus() === "SUCCESS")
			{
				$result = $this->order->getByIDAndIyzico($order_id, $token);
				$user = $result['user'];
				$currency = $result['currency']['CurrencyID'] == 1 ? '' : 'Second';
				$this->session->set_userdata('UserID', $user['UserID']);
				$this->session->set_userdata('UserEmail', $user['UserEmail']);
				$this->session->set_userdata('UserFullname', $user['UserFirstName'] . ' ' . $user['UserLastName']);
				$this->session->set_userdata('UserToken', $result['token']);
				$this->session->set_userdata('UserCurrency', $currency);
				$this->session->set_userdata('UserCurrencyShortName', $result['currency']['CurrencyShortName']);
				$this->session->set_userdata('UserCurrencyCode', $result['currency']['CurrencyCode']);
				$user_id = $this->session->userdata('UserID');

				$this->order->update(array('UpdateWhich' => 'status', 'OrderUserID' => $user_id, 'OrderStatus' => 'completed'), $order_id);
				redirect(base_url() . 'order/result/' . $order_id);
			} 
			else 
			{
				redirect(base_url() . 'order/result/0');
			}
		}
	}

	public function failed()
	{
		$this->load->view('frontend/order/failed_view');
	}

	public function result($order_id = '')
	{
		$user_id = $this->session->userdata('UserID');
		$order = $this->order->getByIDAndUserID($order_id, $user_id);
		$order = isset($order['status']) ? NULL : $order;

		if(!empty($order))
		{
			$data['tracking_number'] = $order['OrderTrackingNumber'];
		} 
		else 
		{
			$data['tracking_number'] = NULL;
		}

		$this->load->view('frontend/order/payment_result_view', $data);
	}

	private function add($coupon_code = '')
	{
		$this->load->model('product/basket_model', 'basket');
		$this->load->model('user/coupon_model', 'coupon');

		$currency = $this->session->userdata('UserCurrency'); 
        $currency_icon = $this->session->userdata('UserCurrencyCode');
		$user_id = $this->session->userdata('UserID');
		$get_lang_id = $this->get_lang_id();
		$this->basket->lang_id = $get_lang_id;
		$this->coupon->lang_id = $get_lang_id;
		$products_in_basket = $this->basket->getAllByUserID($user_id);
		$products_in_basket = isset($products_in_basket['status']) ? NULL : $products_in_basket;
		$coupon_code = $coupon_code ? $coupon_code : 'none';
		$coupon = $this->coupon->getByCodeAndUserID($coupon_code, $user_id);
		$coupon = isset($coupon['status']) ? NULL : $coupon;

		if ($products_in_basket)
		{
			$order_amount = 0;
			$order_vat_amount = 0;
			$order_vat_inclusive_amount = 0;
			$order_discount_rate = 0;
			$order_discount_amount = 0;
			$order_discount_inclusive_vat_amount = 0;
			$order_discount_inclusive_amount_and_without_vat = 0;
			$order_discount_inclusive_amount = 0;
			$order_latest_amount = 0;

			$order_data = array(
				'OrderUserID' => $user_id,
				'OrderShippingStatus' => 'pending',
				'OrderStatus' => 'pendingPayment',
			);

			$order_item = $this->order->save($order_data);
			$order_id = $order_item['OrderID'];

			foreach ($products_in_basket as $key => $value)
			{	
				$detail_data = array(
					'DetailUserID' => $user_id,
					'DetailProductContentID' => $value['ProductContentID'],
					'DetailProductOptionContentID' => $value['ProductOptionContentID'],
					'DetailProductOptionContent' => $value['ProductOptionDetails'],
					'DetailProductName' => $value['ProductName'],
					'DetailCategoryName' => $value['CategoryName'],
					'DetailProductPhoto' => isset($value['PhotoName']) ? $value['PhotoName'] : $value['ProductPhoto'],
					'DetailPrice' => $value['ProductOption'.$currency.'Price'],
					'DetailVatRate' => $value['ProductOption'.$currency.'VatRate'],
					'DetailVatPrice' => $value['ProductOption'.$currency.'VatPrice'],
					'DetailVatInclusivePrice' => $value['ProductOption'.$currency.'VatInclusivePrice'],
					'DetailDiscountRate' => $value['ProductOption'.$currency.'DiscountRate'],
					'DetailDiscountPrice' => $value['ProductOption'.$currency.'DiscountPrice'],
					'DetailDiscountInclusiveVatPrice' => $value['ProductOption'.$currency.'DiscountInclusiveVatPrice'],
					'DetailDiscountInclusivePriceAndWithoutVat' => $value['ProductOption'.$currency.'DiscountInclusivePriceAndWithoutVat'],
					'DetailDiscountInclusivePrice' => $value['ProductOption'.$currency.'LatestPrice'],
					'DetailQuantity' => $value['BasketProductQuantity'],
					'DetailLatestPrice' => $value['ProductOption'.$currency.'LatestPrice'] * $value['BasketProductQuantity'],
					'DetailCurrencyCode' => $currency_icon,
					'DetailSKU' => $value['ProductOptionSKU'],
				);
				
				$order_amount += $value['ProductOption'.$currency.'Price'] * $value['BasketProductQuantity'];
				$order_vat_amount += $value['ProductOption'.$currency.'VatPrice'] * $value['BasketProductQuantity'];
				$order_vat_inclusive_amount += $value['ProductOption'.$currency.'VatInclusivePrice'] * $value['BasketProductQuantity'];
				$order_discount_amount += $value['ProductOption'.$currency.'DiscountPrice'] * $value['BasketProductQuantity'];
				$order_discount_inclusive_vat_amount += $value['ProductOption'.$currency.'DiscountInclusiveVatPrice'] * $value['BasketProductQuantity'];
				$order_discount_inclusive_amount_and_without_vat += $value['ProductOption'.$currency.'DiscountInclusivePriceAndWithoutVat'] * $value['BasketProductQuantity'];
				$order_discount_inclusive_amount += $value['ProductOption'.$currency.'LatestPrice'] * $value['BasketProductQuantity'];
				$order_latest_amount += $value['ProductOption'.$currency.'LatestPrice'] * $value['BasketProductQuantity'];

				$this->detail->save($detail_data, $order_id);
			}

			$order_discount_rate = (($order_vat_inclusive_amount - $order_discount_inclusive_amount) / $order_vat_inclusive_amount) * 100;

			if ($coupon) 
			{
				$coupon_min_status = false;
				if ($order_latest_amount > $coupon['CouponMinAmount']) 
				{
					$coupon_min_status = true;

					if ($coupon['CouponAmount']) 
					{
						$order_latest_amount = $order_latest_amount - $coupon['CouponAmount'];
					}
					else
					{
						$discount_amount = ($order_latest_amount / 100) * $coupon['CouponRate'];
						$order_latest_amount = $order_latest_amount - $discount_amount;
					}
				}
			}

			$cargo_price = 0;
			if ($order_latest_amount < 140) {
				$cargo_price = 9.89;
			}

			$order_data = array(
				'UpdateWhich' => 'amount',
				'OrderUserID' => $user_id,
				'OrderAmount' => $order_amount,
				'OrderVatAmount' => $order_vat_amount,
				'OrderVatInclusiveAmount' => $order_vat_inclusive_amount,
				'OrderDiscountRate' => $order_discount_rate,
				'OrderDiscountAmount' => $order_discount_amount,
				'OrderDiscountInclusiveVatAmount' => $order_discount_inclusive_vat_amount,
				'OrderDiscountInclusiveAmountAndWithoutVat' => $order_discount_inclusive_amount_and_without_vat,
				'OrderDiscountInclusiveAmount' => $order_discount_inclusive_amount,
				'OrderCargoAmount' => $cargo_price,
				'OrderLatestAmount' => $order_latest_amount + $cargo_price,
				'OrderCurrencyCode' => $currency_icon,
				'OrderCurrencyShortName' => $this->session->userdata('UserCurrencyShortName'),
			);

			if ($coupon)
			{
				if ($coupon_min_status) 
				{
					$order_data['OrderCouponContentID'] = $coupon['CouponContentID'];
					$order_data['OrderCouponAmount'] = $coupon['CouponAmount'];
					$order_data['OrderCouponRate'] = $coupon['CouponRate'];

					if (isset($coupon['UserCouponID'])) 
					{
						$this->coupon->update(
							array('UserCouponIsUsed' => 1), 
							$user_id,
							$coupon['UserCouponID']
						);
					}
					else
					{
						$this->coupon->save(
							array(
								'UserCouponIsUsed' => 1, 
								'UserCouponCouponID' => $coupon['CouponID'],
								'UserCouponCouponContentID' => $coupon['CouponContentID']
							), 
							$user_id
						);
					}
				}
			}

			$result = $this->order->update($order_data, $order_id);	

			return $order_id;
		}
		else
		{
			redirect(base_url());
		}
	}

	private function iyzico($order, $detail)
	{
		IyzipayBootstrap::init();

        if(!empty($order) && !empty($detail))
        {
			$iyzico = new \Iyzipay\Request\CreateCheckoutFormInitializeRequest();
			$iyzico->setLocale(\Iyzipay\Model\Locale::TR);
			$iyzico->setConversationId($order['OrderTrackingNumber']);
			$iyzico->setPrice($order['OrderDiscountInclusiveAmount']);
			$iyzico->setPaidPrice($order['OrderLatestAmount']);
			$iyzico->setCurrency(\Iyzipay\Model\Currency::TL);
			$iyzico->setBasketId($order['OrderTrackingNumber']);
			$iyzico->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);
			$iyzico->setCallbackUrl(base_url() . 'order/success/' . $order['OrderID']);
			$iyzico->setEnabledInstallments(array(2, 3, 6, 9));

			$buyer = new \Iyzipay\Model\Buyer();
			$buyer->setId($order['OrderUserID']);
			$buyer->setName($order['OrderBillingFirstName']);
			$buyer->setSurname($order['OrderBillingLastName']);
			$buyer->setGsmNumber($order['OrderBillingPhone']);
			$buyer->setEmail($order['OrderBillingEmail']);
			$buyer->setIdentityNumber($order['OrderBillingIdentityNumber']);
			$buyer->setLastLoginDate(date('Y-m-d H:i:s'));
			$buyer->setRegistrationDate($order['OrderCreatedAt']);
			$buyer->setRegistrationAddress($order['OrderBillingOpenAddress']);
			$buyer->setIp($this->input->ip_address());
			$buyer->setCity($order['OrderBillingCity']);
			$buyer->setCountry($order['OrderBillingCountry']);
			$buyer->setZipCode($order['OrderBillingZipCode']);
			$iyzico->setBuyer($buyer);

			$shippingAddress = new \Iyzipay\Model\Address();
			$shippingAddress->setContactName($order['OrderDeliveryFirstName'] . ' ' . $order['OrderDeliveryLastName']);
			$shippingAddress->setCity($order['OrderDeliveryCity']);
			$shippingAddress->setCountry($order['OrderDeliveryCountry']);
			$shippingAddress->setAddress($order['OrderDeliveryOpenAddress']);
			$shippingAddress->setZipCode($order['OrderDeliveryZipCode']);
			$iyzico->setShippingAddress($shippingAddress);

			$billingAddress = new \Iyzipay\Model\Address();
			$billingAddress->setContactName($order['OrderBillingFirstName'] . ' ' . $order['OrderBillingLastName']);
			$billingAddress->setCity($order['OrderBillingCity']);
			$billingAddress->setCountry($order['OrderBillingCountry']);
			$billingAddress->setAddress($order['OrderBillingOpenAddress']);
			$billingAddress->setZipCode($order['OrderBillingZipCode']);
			$iyzico->setBillingAddress($billingAddress);
			
			$basketItems = array(); 

			foreach ($detail as $key => $value)
			{
				$basketItem = new \Iyzipay\Model\BasketItem();
				$basketItem->setId($value['DetailID']);
				$basketItem->setName($value['DetailProductName']);
				$basketItem->setCategory1($value['DetailCategoryName']);
				$basketItem->setItemType(\Iyzipay\Model\BasketItemType::PHYSICAL);
				$basketItem->setPrice($value['DetailLatestPrice']);
				$basketItems[$key] = $basketItem;
			}

			$iyzico->setBasketItems($basketItems);

			$option = new \Iyzipay\Options();
			$option->setApiKey("iWZdF42RbMXtSSMMRSO3AoGhcq9V8Fpm");
			$option->setSecretKey("FqM7OBaUmA9wpzZylo2ZxTHHMEjKms1e");
			$option->setBaseUrl("https://api.iyzipay.com");

			$checkoutFormInitialize = \Iyzipay\Model\CheckoutFormInitialize::create($iyzico, $option);

			return $checkoutFormInitialize;
        }
        else
        {
        	return FALSE;
        }
	}
}


?>