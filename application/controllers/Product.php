<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
		$this->load->model('product/product_model', 'product');
	}

	public function index($title_and_id = '')
	{
		$this->load->model('product/product_option_model','product_option');
		$this->load->model('product/option_model','option');
		$this->load->model('product/option_group_model','option_group');

		$get_parameter = explode('-', $title_and_id);
		$product_content_id = end($get_parameter);

		$get_lang_id = $this->get_lang_id();
		$this->product->lang_id = $get_lang_id;
		$this->product_option->lang_id = $get_lang_id;

		$result = $this->product->getByContentID($product_content_id);
		$result = !isset($result['status']) ? $result : '';

		if (!empty($result)) 
		{
			$product_options = $this->product_option->getAllByProductContentID($product_content_id);
			$product_options = !isset($product_options['status']) ? $product_options : '';

			if (!empty($product_options)) 
			{
				$option_arr = array(); 

				foreach ($product_options as $options) 
				{
					foreach (json_decode($options['ProductOptionOptionContentIDs'], true) as $option) 
					{
						$option_arr[] = $option;
					}
				}
				
				$product_group_arr = json_decode($product_options[0]['ProductOptionGroupContentIDs'], true);
				$option_groups = array();
				$join_option_groups = array();

				foreach ($product_group_arr as $key => $group)
				{
					$this->option_group->lang_id = $get_lang_id;
					$option_groups[] = $this->option_group->getByContentID($group);
					$join_option_groups[] = $this->option_group->getByContentIDAndJoinOption($group);
				}

				$data['options'] = $option_arr;
				$data['option_groups'] = $option_groups;
				$data['join_option_groups'] = $join_option_groups;
				$data['product'] = $result['product'];
				$data['gallery'] = $result['gallery'];
				$data['tags'] = $result['tags'];
				$data['products_by_category'] = $this->products_by_category($result['product']['ProductCategoryContentID']);

				$this->load->view('frontend/product/detail_view', $data);
			}
			else
			{
				redirect(base_url());
			}
		}
		else
		{
			redirect(base_url());
		}
	}

}


?>