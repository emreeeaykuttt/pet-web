<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Basket extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
		$this->load->model('product/basket_model', 'basket');
	}

	public function index()
	{
		$user_id = $this->session->userdata('UserID') ? $this->session->userdata('UserID') : $this->session->userdata('UnregisteredID');
		$get_lang_id = $this->get_lang_id();
		$this->basket->lang_id = $get_lang_id;
		$products_in_basket = $this->basket->getAllByUserID($user_id);
		$data['products_in_basket'] = isset($products_in_basket['status']) ? NULL : $products_in_basket;

		$basket_total = 0;
		if ($data['products_in_basket']) {
			foreach ($data['products_in_basket'] as $key => $value) 
			{
				$basket_total++;
			}
		}

		$this->session->set_userdata('UserBasketTotalProduct', $basket_total);

		$this->load->view('frontend/product/basket_view', $data);
	}

}


?>