<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends MY_Controller {

	public function change($code = '')
	{
		$this->load->model('language/language_model', 'language');
		
		$default_lang = $this->language->getByDefault();

		if (!$code) 
		{	
			$lang_id = $default_lang['LanguageID'];
			$lang_code = $default_lang['LanguageCode'];
		}
		else
		{	
			$langs = $this->language->getByCode($code);

			if (!$langs) 
			{
				$lang_id = $default_lang['LanguageID'];
				$lang_code = $default_lang['LanguageCode'];
			}
			else
			{
				$lang_id = $langs['LanguageID'];
				$lang_code = $langs['LanguageCode'];
			}
		}

		$this->session->set_userdata('Lang', $lang_id);
		$this->session->set_userdata('LangCode', $lang_code);
		
		header('location: ' . base_url());
		// $this->load->library('user_agent');
		
		// if (!$this->agent->is_mobile())
		// {
		// 	header('location: ' . $this->agent->referrer());
		// }
		// else
		// {
		// 	header('location: ' . base_url());
		// }		
		
	}

}