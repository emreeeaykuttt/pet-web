<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->autorun();
		$this->load->model('user/auth_model', 'auth');
	}

	public function index()
	{
		redirect(base_url());
	}

	public function login()
	{
		$this->load->model('product/basket_model', 'basket');
		
		$post = $this->input->post(null, true);
		
		if ($post) 
		{
			$result = $this->auth->validate($post);

			if ($result['status'] == TRUE) 
			{
				$user = $result['user'];
				$currency = $result['currency']['CurrencyID'] == 1 ? '' : 'Second';
				
				$this->session->unset_userdata(array('UserID', 'UserEmail', 'UserFullname', 'UserToken', 'UserCurrency', 'UserCurrencyShortName', 'UserCurrencyCode'));
				
				$this->session->set_userdata('UserID', $user['UserID']);
				$this->session->set_userdata('UserEmail', $user['UserEmail']);
				$this->session->set_userdata('UserFullname', $user['UserFirstName'] . ' ' . $user['UserLastName']);
				$this->session->set_userdata('UserToken', $result['token']);
				$this->session->set_userdata('UserCurrency', $currency);
				$this->session->set_userdata('UserCurrencyShortName', $result['currency']['CurrencyShortName']);
				$this->session->set_userdata('UserCurrencyCode', $result['currency']['CurrencyCode']);

				$remember = empty($post['UserRemember']) ? 0 : 1;
				if($remember == 1)
				{
					$this->session->set_tempdata('RememberUserEmail', $post['UserEmail'], 604800);
					$this->session->set_tempdata('RememberUserPassword', $post['UserPassword'], 604800);
				}
				else
				{
					$this->session->unset_tempdata(array('RememberUserEmail', 'RememberUserPassword'));
				}

				if ($this->session->userdata('UnregisteredID')) 
				{
					$baskets = $this->basket->getAllByUserID($this->session->userdata('UnregisteredID'));
					$baskets = isset($baskets['status']) ? NULL : $baskets;
					$this->session->unset_userdata(array('UnregisteredID'));

					foreach ($baskets as $key => $value) 
					{
						$this->basket->update(array('BasketUserID' => $this->session->userdata('UserID')), $value['BasketID'], $user['UserID']);
					}
				}
				else
				{
					$baskets = $this->basket->getAllByUserID($user['UserID']);
					$baskets = isset($baskets['status']) ? NULL : $baskets;

					$basket_total = 0;
					if ($baskets) {
						foreach ($baskets as $key => $value) 
						{
							$basket_total++;
						}
					}

					$this->session->set_userdata('UserBasketTotalProduct', $basket_total);
				}

				if ($this->session->userdata('which_page'))
				{
					$which_page = $this->session->userdata('which_page');
					$this->session->unset_userdata(array('which_page'));
					
					redirect($which_page);
				}
				else
				{
					redirect(base_url() . 'profil');
				}
			}
			else
			{
				$this->session->set_flashdata('login_status', $result);

				$this->load->view('frontend/auth/login_view', $result);
			}
		}
		else
		{
			$token = $this->input->get('token');
			$user_id = $this->input->get('user_id');
			$reference_code = $this->input->get('code');

			if (isset($token) && isset($user_id)) 
			{
				$result = $this->auth->verification($token, $user_id, $reference_code);
				$this->session->set_flashdata('account_enabled', $result);
				$this->session->unset_userdata(array('UserID', 'UserEmail', 'UserFullname', 'UserToken', 'UserCurrency', 'UserCurrencyShortName', 'UserCurrencyCode'));

				redirect(base_url() . 'uyelik/giris-yap');
			}
			else
			{
				if ($this->session->userdata('UserToken'))
				{
					redirect(base_url() . 'profil');
				}
				else
				{
					$this->load->view('frontend/auth/login_view');
				}
			}
		}
	}

	public function register()
	{	
		if (!$this->session->userdata('UserToken'))
		{
			$post = $this->input->post(null, true);
			$data = array();

			if ($post)
			{
				$register_data = array(
					'UserReferenceCode' => $post['UserReferenceCode'],
					'UserFirstName' => $post['UserFirstName'],
					'UserLastName' => $post['UserLastName'],
					'UserEmail' => $post['UserEmail'],
					'UserPassword' => $post['UserPassword'],
					'UserPasswordAgain' => $post['UserPasswordAgain'],
					'UserDistanceSalesContract' => isset($post['UserDistanceSalesContract']) ? 1 : '',
					'UserCommunicationPermit' => isset($post['UserCommunicationPermit']) ? 1 : '',
				);

				$result = $this->auth->save($register_data);
				$this->session->set_flashdata('create_status', $result);

				if ($result['status'] == TRUE) 
				{
			        redirect(base_url().'uyelik/giris-yap');
				}
				else
				{
					$result['code'] = $post['UserReferenceCode'];
					$this->load->view('frontend/auth/register_view', $result);
				}
			}
			else
			{
				$data['code'] = '';
				$this->load->view('frontend/auth/register_view', $data);
			}
		}
		else
		{
			redirect(base_url() . 'profil');
		}
	}

	public function guest()
	{
		if (!$this->session->userdata('UserToken'))
		{
			$this->load->model('db/country_model', 'country');
			$get_lang_id = $this->get_lang_id();
			$post = $this->input->post(null, true);

			if ($post)
			{
				$guest_data = array(
					'UserFirstName' => $post['UserFirstName'],
					'UserLastName' => $post['UserLastName'],
					'UserEmail' => $post['UserEmail'],
					'UserPhone' => $post['UserPhone'],
					'AddressCountryID' => $post['AddressCountryID'],
					'AddressCityID' => $post['AddressCityID'],
					'AddressDistrictID' => $post['AddressDistrictID'],
					'AddressAlternativeCity' => $post['AddressAlternativeCity'],
					'AddressOpenAddress' => $post['AddressOpenAddress'],
					'AddressIdentityNumber' => $post['AddressIdentityNumber'],
					'AddressZipCode' => $post['AddressZipCode'],
					'UserDistanceSalesContract' => isset($post['UserDistanceSalesContract']) ? 1 : '',
					'UserCommunicationPermit' => isset($post['UserCommunicationPermit']) ? 1 : '',
				);

				$data = $this->auth->guest($guest_data);
				$this->session->set_flashdata('guest_status', $data);

				if ($data['status'] == TRUE) 
				{
					$this->load->model('product/basket_model', 'basket');
					$currency = $data['currency']['CurrencyID'] == 1 ? '' : 'Second';

					$this->session->set_userdata('UserID', $data['user_id']);
					$this->session->set_userdata('UserEmail', $post['UserEmail']);
					$this->session->set_userdata('UserToken', $data['token']);
					$this->session->set_userdata('UserCurrency', $currency);
					$this->session->set_userdata('UserCurrencyShortName', $data['currency']['CurrencyShortName']);
					$this->session->set_userdata('UserCurrencyCode', $data['currency']['CurrencyCode']);

					if ($this->session->userdata('UnregisteredID')) 
					{
						$baskets = $this->basket->getAllByUserID($this->session->userdata('UnregisteredID'));
						$baskets = isset($baskets['status']) ? NULL : $baskets;
						$this->session->unset_userdata(array('UnregisteredID'));

						foreach ($baskets as $key => $value) 
						{
							$this->basket->update(array('BasketUserID' => $this->session->userdata('UserID')), $value['BasketID'], $data['user_id']);
						}
					}

			        redirect(base_url().'order/delivery');
			        die;
				}
			}

			$this->country->lang_id = $get_lang_id;
			$data['countries'] = $this->country->getAll();

			$this->load->view('frontend/auth/guest_view', $data);
		}
		else
		{
			redirect(base_url() . 'profil');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata(array('UserID', 'UserEmail', 'UserFullname', 'UserToken', 'UserCurrency', 'UserCurrencyShortName', 'UserCurrencyCode', 'UserBasketTotalProduct'));

		redirect(base_url().'uyelik/giris-yap');
	}

	public function forgot()
	{
		if (!$this->session->userdata('UserToken'))
		{
			$post = $this->input->post(null, true);
			$data = '';

			if ($post)
			{
				$result = $this->auth->forgot($post['UserEmail']);

				$this->session->set_flashdata('forgot_status', $result);

				if ($result['status'] == TRUE)
				{   
			        $this->session->unset_tempdata(array('RememberUserEmail', 'RememberUserPassword'));

			        redirect(base_url() . 'uyelik/giris-yap');
				}
				else
				{
					$this->load->view('frontend/auth/forgot_view', $result);
				}
			}
			else
			{	
				$this->load->view('frontend/auth/forgot_view', $data);
			}
		}
		else
		{
			redirect(base_url() . 'profil');
		}
	}

	public function reset()
	{
		$remember_token = $this->input->get('token');
		$user_id = $this->input->get('user_id');

		if (isset($remember_token) && isset($user_id)) 
		{
			$post = $this->input->post(null, true);
			$data = '';

			if ($post)
			{
				$data_arr = array(
					'UserRememberToken' => $remember_token,
					'UserID' => $user_id,
					'UserPassword' => $post['UserPassword'],
					'UserPasswordAgain' => $post['UserPasswordAgain'],
				);

				$result = $this->auth->passwordUpdate($data_arr);
				
				$this->session->set_flashdata('reset_status', $result);

				if ($result['status'] == TRUE)
				{
					$this->session->unset_tempdata(array('RememberUserEmail', 'RememberUserPassword'));

					redirect(base_url() . 'uyelik/giris-yap');
				}
				else
				{
					$this->load->view('frontend/auth/reset_view', $result);
				}
			}
			else
			{
				$this->load->view('frontend/auth/reset_view', $data);
			}
		}
		else
		{
			redirect(base_url() . 'uyelik/giris-yap');
		}
	}
}

?>