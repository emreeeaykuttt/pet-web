<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->session->userdata('UserToken'))
		{
			$this->session->set_userdata('which_page', (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

			redirect(base_url() . 'uyelik/giris-yap');
			exit();
		}

		$this->autorun();
		$this->load->model('user/user_model', 'user');
	}

	public function index()
	{
		$post = $this->input->post(null, true);

		$user_id = $this->session->userdata('UserID');
		$data['profile'] = $this->user->getByID($user_id);
		
		if ($post)
		{
			$post_data = array(
				'UserFirstName' => $post['UserFirstName'],
				'UserLastName' => $post['UserLastName'],
				'UserEmail' => $post['UserEmail'],
				'UserEmailOld' => $this->session->userdata('UserEmail'),
				'UserPasswordOld' => $post['UserPasswordOld'],
				'UserPassword' => $post['UserPassword'],
				'UserPasswordAgain' => $post['UserPasswordAgain'],
				'UserPhone' => $post['UserPhone'],
				'UserDateBirth' => $post['UserDateBirth'],
				'UserGender' => isset($post['UserGender']) ? $post['UserGender'] : NULL,
			);

			$data['result'] = $this->user->update($post_data, $user_id);
			
			$this->session->set_flashdata('profile_status', $data['result']);

			if ($data['result']['status'] == TRUE) 
			{
				$this->session->unset_userdata('UserEmail');
				$this->session->set_userdata('UserEmail', $post['UserEmail']);

				redirect(base_url() . 'profil');
				die;
			}
		}
		
		$this->load->view('frontend/user/profile_view', $data);
	}

	public function address()
	{
		$this->load->model('user/address_model', 'address');
		$this->load->model('db/country_model', 'country');

		$user_id = $this->session->userdata('UserID');
		$get_lang_id = $this->get_lang_id();
		$this->address->lang_id = $get_lang_id;
		$this->country->lang_id = $get_lang_id;

		$address = $this->address->getAllByUserID($user_id);
		$data['address'] = isset($address['status']) ? NULL : $address;
		$data['countries'] = $this->country->getAll();

		$this->load->view('frontend/user/address_view', $data);
	}

	public function orders()
	{
		$this->load->model('order/order_model', 'order');

		$user_id = $this->session->userdata('UserID');
		$orders = $this->order->getAllByUserID($user_id);
		$data['orders'] = isset($orders['status']) ? NULL : $orders;

		$this->load->view('frontend/user/orders_view', $data);
	}
	
	public function coupons()
	{
		$this->load->model('user/coupon_model', 'coupon');

		$user_id = $this->session->userdata('UserID');
		$get_lang_id = $this->get_lang_id();
		$this->coupon->lang_id = $get_lang_id;

		$coupons = $this->coupon->getAllByUserID($user_id);
		$data['coupons'] = isset($coupons['status']) ? NULL : $coupons;

		$this->load->view('frontend/user/coupons_view', $data);
	}

	public function favorites()
	{
		$this->load->model('user/favorite_model','favorite');

		$user_id = $this->session->userdata('UserID');
		$get_lang_id = $this->get_lang_id();
		$this->favorite->lang_id = $get_lang_id;

		$favorites = $this->favorite->getAllByUserID($user_id);
		$data['favorites'] = isset($favorites['status']) ? NULL : $favorites;

		$this->load->view('frontend/user/favorites_view', $data);
	}
}

?>