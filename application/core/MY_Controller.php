<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

	public $treeHtml = '';

	public function __construct()
	{
		parent::__construct();
	}

	public function categories($data, $parent_id, $is_sub = 1, $selected_menus = array(), $li_class = '', $status = 'new', $category_content_id = '')
    {
        if ($status == 'new') 
        {
            $this->treeHtml = '';
        }

        if (isset($data[$parent_id])) 
        {
            $this->treeHtml .= '<ul>';

            foreach ($data[$parent_id] as $key => $value) {

                if ($value['CategoryParentID'] == 1) 
                {
                    if (in_array($value['CategoryContentID'], $selected_menus) || empty($selected_menus)) 
                    {
                        $single_menu = '';
                        if ($value['CategoryContentID'] != 5) {
                            $single_menu = $li_class;
                        }

                        $this->treeHtml .= '<li class="'.$single_menu.'"><a href="'. base_url('kategori/' . $value['CategorySlug']) .'">' . $value['CategoryName'] . '</a>';
                        $this->categories($data, $value['CategoryContentID'], $is_sub, $selected_menus, $li_class, 'old', $category_content_id);
                        $this->treeHtml .= '</li>';
                    }
                } 
                else 
                {
                    if ($is_sub == 1) 
                    {
                        $li_active_class = '';
                        if ($category_content_id == $value['CategoryContentID']) {
                            $li_active_class = 'active';
                        }

                        $this->treeHtml .= '<li class="'. $li_active_class .'"><a href="'. base_url('kategori/' . $value['CategorySlug']).'">' . $value['CategoryName'] . '</a>';
                        $this->categories($data, $value['CategoryContentID'], $is_sub, $selected_menus, $li_class, 'old', $category_content_id);
                        $this->treeHtml .= '</li>';
                    }
                }
            }

            $this->treeHtml .= '</ul>';   
        }

        return $this->treeHtml;
    }

	public function autorun()
	{	
		$this->load->model('product/category_model', 'category');

		$get_lang_id = $this->get_lang_id();
		
		$this->category->lang_id = $get_lang_id;
		$categories = $this->category->getAll();
		$categories = isset($categories['status']) ? NULL : $categories;

		define('CATEGORIES', serialize($categories));
		define('FREE_SHIPPING_PRICE', 140);
		define('CARGO_PRICE', 9.89);

		if (!$this->session->userdata('UserCurrency')) 
		{
			$this->session->set_userdata('UserCurrency', '');
			$this->session->set_userdata('UserCurrencyShortName', 'TRY');
			$this->session->set_userdata('UserCurrencyCode', '₺');
		}
	}

	public function get_lang_id()
	{
		$lang = $this->session->userdata('Lang');

		if(!$lang)
		{
			$this->load->model('language/language_model', 'language');
			$default_lang = $this->language->getByDefault();

			$this->session->set_userdata('Lang', $default_lang['LanguageID']);
			$this->session->set_userdata('LangCode', $default_lang['LanguageCode']);	
		}

		return $this->session->userdata('Lang');
	}

	public function products_by_tag($limit = 12, $tag)
	{
		$this->load->model('product/product_model', 'product');

		$get_lang_id = $this->get_lang_id();

		$this->product->lang_id = $get_lang_id;
		$this->product->limit = $limit;
		$this->product->tag = $tag;

		$products = $this->product->getAll();

		if (!isset($products['status']))
		{
			$data = $products['products'];
		}
		else
		{
			$data = '';
		}

		return $data;
	}

	public function recently_added_products($limit = 4)
	{
		$this->load->model('product/product_model', 'product');

		$get_lang_id = $this->get_lang_id();

		$this->product->lang_id = $get_lang_id;
		$this->product->limit = $limit;

		$products = $this->product->getAll();

		if (!isset($products['status']))
		{
			$data = $products['products'];
		}
		else
		{
			$data = '';
		}

		return $data;
	}

	public function products_by_category($category_id, $limit = 8)
	{
		$this->load->model('product/product_model', 'product');

		$get_lang_id = $this->get_lang_id();

		$this->product->lang_id = $get_lang_id;
		$this->product->limit = $limit;

		$products = $this->product->getAllByCategoryID($category_id);

		if (!isset($products['status']))
		{
			$data = $products['products'];
		}
		else
		{
			$data = '';
		}

		return $data;
	}

	public function best_selling_products($limit = 8)
	{
		$this->load->model('product/product_model', 'product');

		$get_lang_id = $this->get_lang_id();

		$this->product->lang_id = $get_lang_id;
		$this->product->limit = $limit;
		$this->product->tag = '';

		$products = $this->product->getAllBybestSelling();

		if (!isset($products['status']))
		{
			$data = $products['products'];
		}
		else
		{
			$data = '';
		}

		return $data;
	}

}





?>