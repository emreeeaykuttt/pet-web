<?php 
class Page_model extends CI_Model {

	var $API_END_POINT = 'pages';

	function __construct()
	{
		parent::__construct();
	}

	function getAll(){
		$params = array('lang_id' => $this->lang_id);
		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}
}
?>