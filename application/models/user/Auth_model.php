<?php 
class Auth_model extends CI_Model {

	var $API_END_POINT = 'authentication';

	function __construct()
	{
		parent::__construct();
	}

	function save($array)
	{
		return json_decode($this->restclient->post($array, $this->API_END_POINT), true);
	}

	function validate($array)
	{
		return json_decode($this->restclient->post($array, $this->API_END_POINT . '/token'), true);
	}

	function passwordUpdate($array)
	{
		return json_decode($this->restclient->post($array, $this->API_END_POINT . '/reset'), true);
	}

	function verification($token, $id, $refence_code)
	{
		$params = array('UserVerificationToken' => $token, 'UserID' => $id, 'UserReferenceCode' => $refence_code);
		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/verification'), true);
	}

	function forgot($email)
	{
		$params = array('UserEmail' => $email);
		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/forgot'), true);	
	}

	function invite($array)
	{
		return json_decode($this->restclient->post($array, $this->API_END_POINT . '/invite'), true);
	}

	function guest($array)
	{
		return json_decode($this->restclient->post($array, $this->API_END_POINT . '/guest'), true);
	}
}
?>