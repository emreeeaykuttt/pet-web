<?php 
class Favorite_model extends CI_Model {

	var $API_END_POINT = 'favorites';
	var $API_USERS_END_POINT = 'users';

	function __construct()
	{
		parent::__construct();
	}

	function deleteByIDAndUserID($id, $user_id)
	{
		return json_decode($this->restclient->delete(null, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT . '/' . $id), true);
	}

	function save($array, $user_id)
	{
		return json_decode($this->restclient->post($array, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT), true);
	}

	function getAllByUserID($user_id)
	{	
		$params = null;

		if (!empty($this->lang_id)) 
		{
			$params = array('lang_id' => $this->lang_id);
		}

		return json_decode($this->restclient->get($params, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT), true);
	}

}
?>