<?php 
class User_model extends CI_Model {

	var $API_END_POINT = 'users';

	function __construct()
	{
		parent::__construct();
	}

	function getByID($id)
	{
		$params = NULL;

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/' . $id), true);
	}

	function update($array, $id)
	{
		return json_decode($this->restclient->put($array, $this->API_END_POINT . '/' . $id), true);
	}

}
?>