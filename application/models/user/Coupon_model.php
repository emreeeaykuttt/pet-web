<?php 
class Coupon_model extends CI_Model {

	var $API_END_POINT = 'coupons';
	var $API_USERS_END_POINT = 'users';

	function __construct()
	{
		parent::__construct();
	}

	function getByContentIDAndUserID($content_id, $user_id)
	{	
		$params = null;

		if (!empty($this->lang_id)) 
		{
			$params = array('lang_id' => $this->lang_id);
		}

		return json_decode($this->restclient->get($params, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT . '/' . $content_id), true);
	}

	function getAllByUserID($user_id)
	{	
		$params = null;

		if (!empty($this->lang_id)) 
		{
			$params = array('lang_id' => $this->lang_id);
		}

		return json_decode($this->restclient->get($params, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT), true);
	}

	function getByCodeAndUserID($code, $user_id)
	{	
		$params = array('code' => $code);

		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}

		return json_decode($this->restclient->get($params, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT), true);
	}

	function update($array, $user_id, $user_coupon_id)
	{
		return json_decode($this->restclient->put($array, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT . '/' . $user_coupon_id), true);
	}

	function save($array, $user_id)
	{
		return json_decode($this->restclient->post($array, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT), true);
	}

}
?>