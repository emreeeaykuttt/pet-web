<?php 
class Address_model extends CI_Model {

	var $API_END_POINT = 'addresses';
	var $API_USERS_END_POINT = 'users';

	function __construct()
	{
		parent::__construct();
	}

	function getAllByUserID($user_id)
	{
		$params = NULL;

		if (!empty($this->lang_id)) 
		{
			$params = array('lang_id' => $this->lang_id);
		}

		return json_decode($this->restclient->get($params, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT), true);
	}

	function getByIDAndUserID($id, $user_id)
	{
		$params = NULL;

		if (!empty($this->lang_id)) 
		{
			$params = array('lang_id' => $this->lang_id);
		}

		return json_decode($this->restclient->get($params, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT . '/' . $id), true);
	}

	function deleteByIDAndUserID($id, $user_id)
	{
		return json_decode($this->restclient->delete(null, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT . '/' . $id), true);
	}

	function save($array, $user_id)
	{
		return json_decode($this->restclient->post($array, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT), true);
	}

	function update($array, $id, $user_id)
	{
		return json_decode($this->restclient->put($array, $this->API_USERS_END_POINT . '/' . $user_id . '/' . $this->API_END_POINT . '/' . $id), true);
	}

}
?>