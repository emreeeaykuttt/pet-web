<?php 
class Detail_model extends CI_Model {

	var $API_END_POINT = 'details';
	var $API_ORDERS_END_POINT = 'orders';

	function __construct()
	{
		parent::__construct();
	}

	function getAllByOrderIDAndUserID($order_id, $user_id)
	{
		$params = NULL;

		if (!empty($user_id)) 
		{
			$params = array('user_id' => $user_id);
		}

		return json_decode($this->restclient->get($params, $this->API_ORDERS_END_POINT . '/' . $order_id . '/' . $this->API_END_POINT), true);
	}

	function save($array, $order_id)
	{
		return json_decode($this->restclient->post($array, $this->API_ORDERS_END_POINT . '/' . $order_id . '/' . $this->API_END_POINT), true);
	}

}
?>