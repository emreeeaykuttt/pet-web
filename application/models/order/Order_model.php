<?php 
class Order_model extends CI_Model {

	var $API_END_POINT = 'orders';

	function __construct()
	{
		parent::__construct();
	}

	function getByID($id)
	{
		$params = NULL;

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/' . $id), true);
	}

	function getByIDAndIyzico($id, $iyzico_token)
	{
		$params = array('iyzico_token' => $iyzico_token);

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/' . $id), true);
	}

	function getByIDAndUserID($id, $user_id)
	{
		$params = array('user_id' => $user_id);

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/' . $id), true);
	}

	function getAllByUserID($user_id)
	{
		$params = array('user_id' => $user_id);

		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}

	function save($array)
	{
		return json_decode($this->restclient->post($array, $this->API_END_POINT), true);
	}

	function update($array, $id)
	{
		return json_decode($this->restclient->put($array, $this->API_END_POINT . '/' . $id), true);
	}
}
?>