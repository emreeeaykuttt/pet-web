<?php 
class Language_model extends CI_Model {

	var $API_END_POINT = 'languages';

	function __construct()
	{
		parent::__construct();
	}

	function getByCode($code)
	{
		$params = array('code' => $code);
		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}

	function getByDefault()
	{
		$params = array('default' => 1);
		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}

	function getAllByActive()
	{
		$params = array('active' => 1);
		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}

	function getCount()
	{
		$params = array('count' => 'status');
		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}

	function save($array)
	{
		$this->restclient->post($array, $this->API_END_POINT);
	}

	function update($array)
	{
		$this->restclient->put($array, $this->API_END_POINT);
	}

	function delete($id)
	{
		$this->restclient->delete($id, $this->API_END_POINT);
	}
}
?>