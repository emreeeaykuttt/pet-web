<?php 
class City_model extends CI_Model {

	var $API_END_POINT = 'cities';
	var $API_COUNTRIES_END_POINT = 'countries';

	function __construct()
	{
		parent::__construct();
	}

	function getAllByCountryID($country_id)
	{
		$params = null;

		return json_decode($this->restclient->get($params, $this->API_COUNTRIES_END_POINT . '/' . $country_id . '/' . $this->API_END_POINT), true);
	}
}
?>