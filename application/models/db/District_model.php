<?php 
class District_model extends CI_Model {

	var $API_END_POINT = 'districts';
	var $API_CITIES_END_POINT = 'cities';

	function __construct()
	{
		parent::__construct();
	}

	function getAllByCityID($city_id)
	{
		$params = null;

		return json_decode($this->restclient->get($params, $this->API_CITIES_END_POINT . '/' . $city_id . '/' . $this->API_END_POINT), true);
	}
}
?>