<?php 
class Category_model extends CI_Model {

	var $API_END_POINT = 'categories';

	function __construct()
	{
		parent::__construct();
	}

	function getAll()
	{
		$params = array('lang_id' => $this->lang_id);
		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}

	function getAllByParentID($parent_id)
	{
		$params = array('parent_id' => $parent_id);

		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}

		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}
}
?>