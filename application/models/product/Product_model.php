<?php 
class Product_model extends CI_Model {

	var $API_END_POINT = 'products';

	function __construct()
	{
		parent::__construct();
	}

	function getByContentID($content_id)
	{
		$params = null;
		
		if (!empty($this->lang_id)) 
		{
			$params = array('lang_id' => $this->lang_id);
		}

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/' . $content_id), true);
	}

	function getAll()
	{
		$params = array();

		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}

		if (!empty($this->limit)) 
		{
			$params['limit'] = $this->limit;
			$params['offset'] = 0;
		}
		elseif (!empty($this->per_page)) 
		{
			$params['per_page'] = $this->per_page;
			$params['page'] = $this->page;
		}

		if (!empty($this->filter)) 
		{
			$params['filter'] = str_replace(' ', '%20', $this->filter);
		}

		if (!empty($this->min) && !empty($this->max)) 
		{
			$params['min'] = str_replace(' ', '%20', $this->min);
			$params['max'] = str_replace(' ', '%20', $this->max);
		}

		if (!empty($this->tag)) 
		{
			$params['tag'] = str_replace(' ', '%20', $this->tag);
		}

		if (!empty($this->search)) 
		{
			$params['search'] = str_replace(' ', '%20', $this->search);
		}

		if (!empty($this->sort)) 
		{
			$params['sort'] = str_replace(' ', '%20', $this->sort);
		}

		if (empty($params)) 
		{
			$params = null;
		}

		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}

	function getAllByCategoryID($category_content_id)
	{
		$params = array('category_content_id' => $category_content_id);
		
		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}

		if (!empty($this->limit)) 
		{
			$params['limit'] = $this->limit;
			$params['offset'] = 0;
		}
		elseif (!empty($this->per_page)) 
		{
			$params['per_page'] = $this->per_page;
			$params['page'] = $this->page;
		}

		if (!empty($this->filter)) 
		{
			$params['filter'] = str_replace(' ', '%20', $this->filter);
		}

		if (!empty($this->min) && !empty($this->max)) 
		{
			$params['min'] = str_replace(' ', '%20', $this->min);
			$params['max'] = str_replace(' ', '%20', $this->max);
		}

		if (!empty($this->tag)) 
		{
			$params['tag'] = str_replace(' ', '%20', $this->tag);
		}

		if (!empty($this->search)) 
		{
			$params['search'] = str_replace(' ', '%20', $this->search);
		}

		if (!empty($this->sort)) 
		{
			$params['sort'] = str_replace(' ', '%20', $this->sort);
		}

		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);	
	}

	function getAllBybestSelling()
	{
		$params = array('best_selling' => 1);

		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}

		if (!empty($this->limit)) 
		{
			$params['limit'] = $this->limit;
			$params['offset'] = 0;
		}
		elseif (!empty($this->per_page)) 
		{
			$params['per_page'] = $this->per_page;
			$params['page'] = $this->page;
		}

		if (!empty($this->filter)) 
		{
			$params['filter'] = str_replace(' ', '%20', $this->filter);
		}

		if (!empty($this->min) && !empty($this->max)) 
		{
			$params['min'] = str_replace(' ', '%20', $this->min);
			$params['max'] = str_replace(' ', '%20', $this->max);
		}

		if (!empty($this->tag)) 
		{
			$params['tag'] = $this->tag;
		}

		if (!empty($this->search)) 
		{
			$params['search'] = str_replace(' ', '%20', $this->search);
		}

		if (!empty($this->sort)) 
		{
			$params['sort'] = str_replace(' ', '%20', $this->sort);
		}

		if (empty($params)) 
		{
			$params = null;
		}

		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}
}
?>