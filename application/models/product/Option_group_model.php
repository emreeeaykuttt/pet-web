<?php 
class Option_group_model extends CI_Model {

	var $API_END_POINT = 'option_groups';

	function __construct()
	{
		parent::__construct();
	}

	function getByContentID($content_id)
	{	
		$params = array();

		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/' . $content_id), true);
	}

	function getByContentIDAndJoinOption($content_id)
	{	
		$params = array('option_join' => 1);

		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/' . $content_id), true);
	}

	function getAllAndJoinOption()
	{	
		$params = array('option_join' => 1);

		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}

		return json_decode($this->restclient->get($params, $this->API_END_POINT), true);
	}

}
?>