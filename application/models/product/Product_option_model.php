<?php 
class Product_option_model extends CI_Model {

	var $API_END_POINT = 'options';
	var $API_PRODUCTS_END_POINT = 'products';

	function __construct()
	{
		parent::__construct();
	}

	function getAllByProductContentID($product_content_id)
	{	
		$params = array();

		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}
		else
		{
			$params = null;
		}

		return json_decode($this->restclient->get($params, $this->API_PRODUCTS_END_POINT . '/' . $product_content_id . '/' . $this->API_END_POINT), true);
	}

	function getByProductContentIDAndOptionContentIDs($product_content_id, $option_content_ids)
	{
		$params = array('option_content_ids' => $option_content_ids);

		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}
		
		return json_decode($this->restclient->get($params, $this->API_PRODUCTS_END_POINT . '/' . $product_content_id . '/' . $this->API_END_POINT), true);
	}

	function getByProductContentIDAndContentID($product_content_id, $content_id)
	{	
		$params = array();

		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}
		else
		{
			$params = null;
		}

		return json_decode($this->restclient->get($params, $this->API_PRODUCTS_END_POINT . '/' . $product_content_id . '/' . $this->API_END_POINT . '/' . $content_id), true);
	}

}
?>