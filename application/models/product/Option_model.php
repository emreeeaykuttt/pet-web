<?php 
class Option_model extends CI_Model {

	var $API_END_POINT = 'options';

	function __construct()
	{
		parent::__construct();
	}

	function getByContentID($content_id)
	{	
		$params = array();

		if (!empty($this->lang_id)) 
		{
			$params['lang_id'] = $this->lang_id;
		}

		return json_decode($this->restclient->get($params, $this->API_END_POINT . '/' . $content_id), true);
	}

}
?>