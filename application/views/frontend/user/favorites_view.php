<!DOCTYPE html>
<html lang="tr">
    
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Favorilerim">
    <meta name="keywords" content="Favorilerim">

    <title>Favorilerim</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
    
    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page profile">
        <div class="container">
            <div class="sub-title">Favorilerim</div>
            <div class="row">
                <div class="col-md-3">

                    <?php include(dirname(__DIR__) . '/inc/profile_left.php'); ?>

                </div>
                <div class="col-md-9">
                    
                    <div class="list-area">

                        <div class="title-area head">
                            <div class="row">
                                <div class="col-md-9">Ürün</div>
                                <div class="col-md-3 text-right">Çıkar</div>
                            </div>
                        </div>

                        <div class="list-detail">

                            <?php 
                                $currency = $this->session->userdata('UserCurrency'); 
                                $currency_icon = $this->session->userdata('UserCurrencyCode'); 
                            ?>
                            <?php if ($favorites): ?>
                            <?php foreach ($favorites as $key => $value): ?>
                            <div class="row-item" id="favorite-item-<?=$value['FavoriteID']?>">                        
                                <div class="row p-3">

                                    <div class="col-md-9 ">
                                        <a href="<?=base_url() . 'urun/' . $value['ProductSlug']?>" class="favorite-link">
                                            <div class="row">
                                                <div class="col-md-3 pr-0">
                                                    <?php 
                                                        if (!empty($value['ProductPhoto'])) {
                                                            $image = SERVER_URL . 'resize/xs/' . $value['ProductPhoto'];
                                                        }elseif (!empty($value['PhotoName'])) {
                                                            $image = SERVER_URL . 'resize/xs/' . $value['PhotoName'];
                                                        }else{
                                                            $image = SERVER_URL . 'upload/product/null-photo.png';
                                                        } 
                                                    ?>

                                                    <img src="<?=$image?>" class="favorite-img"> 
                                                </div>
                                                <div class="col-md-9 pl-1 d-flex flex-column">
                                                    <div class="my-auto">
                                                        <b><?=$value['ProductName']?></b><br />
                                                        <?= $value['ProductOptionSKU'] ? $value['ProductOptionSKU'] : $value['ProductCode'] ?><br />

                                                        <?php if ($value['ProductOptionQuantity'] > 0 || $value['ProductOptionUnlimited'] == 1) { ?>
                                                        <div> 
                                                            <?=number_format_render($value['ProductOption'.$currency.'LatestPrice'])?>
                                                            <span><?= $currency_icon ?></span>
                                                        </div>
                                                        <?php } else { ?>
                                                        <div>STOKTA YOK</div>
                                                        <?php } ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-md-3 d-flex flex-column">
                                        <div class="float-right ml-auto my-auto">
                                            <a href="javascript:void(0)" onclick="destroy_favorite(<?=$value['FavoriteID'] . ', ' . $value['ProductID'] . ', ' . $value['ProductContentID']?>)" class="btn btn-light mr-1">
                                                <i class="las la-times m-0"></i>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <?php endforeach ?>
                            <?php endif ?>

                        </div>

                    </div>

                </div>
            </div>


        </div>

    </div>
  
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <script type="text/javascript">

        function destroy_favorite(favorite_id, product_id, product_content_id)
        {
            $('#preloader').fadeIn();

            $.ajax({
                url : base_url + 'ajax/destroy_favorite/' + favorite_id,
                type: 'GET',
                dataType: 'JSON',
                success: function(data)
                {
                    if (typeof(data.status) != "undefined" && data.status !== null)
                    {
                        if(data.status == 'login')
                        {
                            window.location.href = base_url + 'auth/login';
                        }
                    }

                    $('#preloader').fadeOut();

                    message = data.message;

                    if(data.status)
                    {
                        notify_type = 'success';
                        icon = 'la la-check';
                    }
                    else
                    {
                        notify_type = 'danger';
                        icon = 'la la-close';
                    }

                    $.notify(
                        {
                            icon: icon,
                            message: message
                        },
                        {
                            type: notify_type,
                            animate: {
                                enter: 'animated fadeInRight',
                                exit: 'animated fadeOutRight'
                            }
                        }
                    );

                    $('#favorite-item-' + favorite_id).slideUp(300, function() { $(this).remove(); });
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Ürün favorilere eklenirken bir sorun oluştu.');
                }
            });
        }
    </script>

</body>
    
</html>