<!DOCTYPE html>
<html lang="tr">
	
<head>
	<meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Temel Bilgiler">
    <meta name="keywords" content="Temel Bilgiler">

    <title>Temel Bilgiler</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
	
	<?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <?php 
        if (isset($result['errors'])) 
        {
            $errors = $result['errors'];
        }

        $profile_status = $this->session->flashdata('profile_status');
        $flashdata = null;
        if (isset($profile_status)) {
            $flashdata = $profile_status;
        }
    ?>

    <div class="sub-page profile">
        <div class="container">
            <div class="sub-title">Profil</div>
            <div class="row">
                <div class="col-md-3">

                    <?php include(dirname(__DIR__) . '/inc/profile_left.php'); ?>

                </div>
                <div class="col-md-9">
                    
                    <form method="POST" action="<?=base_url()?>profile" class="adress-form">
                        <div class="row">

                            <?php if ($flashdata): ?>
                            <div class="col-md-12">
                                <div class="alert special-alert <?=$flashdata['status'] ? 'alert-success' : 'alert-danger'?> mb-2">
                                   <span><?=$flashdata['message']?></span>
                                </div>
                            </div> 
                            <?php endif ?>

                            <div class="col-md-6">
                                <input 
                                    type="text" 
                                    name="UserFirstName" 
                                    class="form-control" 
                                    placeholder="İsim" 
                                    value="<?= set_value('UserFirstName') ? set_value('UserFirstName') : $profile['UserFirstName'] ?>"
                                />
                                <div class="help-feedback"><?=isset($errors['UserFirstName']) ? $errors['UserFirstName'] : ''?></div>
                            </div>

                            <div class="col-md-6">
                                <input 
                                    type="text" 
                                    name="UserLastName" 
                                    class="form-control" 
                                    placeholder="Soyisim" 
                                    value="<?= set_value('UserLastName') ? set_value('UserLastName') : $profile['UserLastName'] ?>"
                                />
                                <div class="help-feedback"><?=isset($errors['UserLastName']) ? $errors['UserLastName'] : ''?></div>
                            </div>

                            <div class="col-md-12">
                                <input 
                                    type="text" 
                                    name="UserEmail" 
                                    class="form-control" 
                                    placeholder="E-Posta" 
                                    value="<?= set_value('UserEmail') ? set_value('UserEmail') : $profile['UserEmail'] ?>"
                                />
                                <div class="help-feedback"><?=isset($errors['UserEmail']) ? $errors['UserEmail'] : ''?></div>
                            </div>
                            
                            <div class="col-md-6">
                                <input 
                                    type="tel" 
                                    name="UserPhone" 
                                    class="form-control" 
                                    placeholder="Telefon" 
                                    value="<?= set_value('UserPhone') ? set_value('UserPhone') : $profile['UserPhone'] ?>"
                                />
                                <div class="help-feedback"><?=isset($errors['UserPhone']) ? $errors['UserPhone'] : ''?></div>
                            </div>

                            <div class="col-md-6">
                                <input 
                                    type="date" 
                                    name="UserDateBirth"
                                    class="form-control" 
                                    placeholder="Doğum Tarihi" 
                                    value="<?= set_value('UserDateBirth') ? set_value('UserDateBirth') : $profile['UserDateBirth'] ?>"
                                />
                                <div class="help-feedback"><?=isset($errors['UserDateBirth']) ? $errors['UserDateBirth'] : ''?></div>
                            </div>

                            <div class="col-md-12">
                                <label for="gender" class="w-100">Cinsiyet</label>

                                <div class="form-check-inline">
                                    <label class="form-check-label 1em">
                                        <input 
                                            type="radio" 
                                            name="UserGender" 
                                            class="form-check-input" 
                                            value="Kadın" 
                                            <?=$profile['UserGender'] == 'Kadın' ? 'checked' : ''?>
                                        />Kadın
                                    </label>
                                </div>

                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input 
                                            type="radio" 
                                            name="UserGender" 
                                            class="form-check-input" 
                                            value="Erkek" 
                                            <?=$profile['UserGender'] == 'Erkek' ? 'checked' : ''?>
                                        />Erkek
                                    </label>
                                </div>

                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input 
                                            type="radio" 
                                            name="UserGender" 
                                            class="form-check-input" 
                                            value="Diğer" 
                                            <?=$profile['UserGender'] == 'Diğer' ? 'checked' : ''?>
                                        />Diğer
                                    </label>
                                </div>

                                <div class="help-feedback"><?=isset($errors['UserGender']) ? $errors['UserGender'] : ''?></div>

                            </div>

                            <div class="col-md-12">
                                <input 
                                    type="password" 
                                    name="UserPasswordOld" 
                                    class="form-control" 
                                    placeholder="Şifre değiştirmek istiyorsanız şu anki şifrenizi bu alana girmeniz gerekiyor." 
                                    value=""
                                />
                                <div class="help-feedback"><?=isset($errors['UserPasswordOld']) ? $errors['UserPasswordOld'] : ''?></div>
                            </div>

                            <div class="col-md-12">
                                <input 
                                    type="password" 
                                    name="UserPassword" 
                                    class="form-control" 
                                    placeholder="Yeni Şifre" 
                                    value=""
                                />
                                <div class="help-feedback"><?=isset($errors['UserPassword']) ? $errors['UserPassword'] : ''?></div>
                            </div>

                            <div class="col-md-12">
                                <input 
                                    type="password" 
                                    name="UserPasswordAgain" 
                                    class="form-control" 
                                    placeholder="Yeni Şifre Tekrar" 
                                    value=""
                                />
                                <div class="help-feedback"><?=isset($errors['UserPasswordAgain']) ? $errors['UserPasswordAgain'] : ''?></div>
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-black w-100 float-right p-3">GÜNCELLE</button>
                            </div>

                        </div>
                    </form>

                </div>
            </div>


        </div>

    </div>
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

</body>
	
</html>