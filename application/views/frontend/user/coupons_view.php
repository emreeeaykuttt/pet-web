<!DOCTYPE html>
<html lang="tr">
    
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Kuponlarım">
    <meta name="keywords" content="Kuponlarım">

    <title>Kuponlarım</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
    
    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page profile">
        <div class="container">
            <div class="sub-title">Kuponlarım</div>
            <div class="row">
                <div class="col-md-3">

                    <?php include(dirname(__DIR__) . '/inc/profile_left.php'); ?>

                </div>
                <div class="col-md-9">
                    
                    <div class="list-area">

                         <div class="row mb-3">
                            <div class="col-md-12">
                                <input type="text" name="UserEmail" placeholder="Kupon kazanmak için arkadaşına davet maili gönder (Email)" class="invitation-input">
                                <a href="javascript:void(0)" onclick="send_membership_invitation()" class="invitation-btn">
                                    DAVET ET 
                                </a>
                            </div>
                        </div>

                        <div class="title-area">
                            <div class="row">
                                <div class="col-5">Kupon</div>
                                <div class="col-4">Min Sepet Tutarı</div>
                                <div class="col-3">Durumu</div>
                            </div>
                        </div>

                        <div class="list-detail">

                            <?php if ($coupons): ?>
                            <?php foreach ($coupons as $key => $value): ?>
                            <div class="row-item" id="coupon-item-<?=$value['CouponContentID']?>">                        
                                <div class="row p-3">

                                    <div class="col-5 d-flex">
                                        <a href="javascript:void(0)" onclick="coupon_view(<?=$value['CouponContentID']?>)">
                                            <?=$value['CouponName']?>
                                        </a>
                                    </div>

                                    <div class="col-4 d-flex">
                                        <?=$value['CouponMinAmount'] . $this->session->userdata('UserCurrencyCode')?>
                                    </div>

                                    <div class="col-3">
                                        <div class="float-left">
                                           <?=$value['UserCouponIsUsed'] == 1 ? '<span class="badge badge-secondary p-2">Kullanıldı</span>' : '<span class="badge badge-success p-2">Kullanılmadı</span>'?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <?php endforeach ?>
                            <?php endif ?>

                        </div>

                    </div>

                </div>
            </div>


        </div>

    </div>
  
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <!-- modal start -->
    <div class="modal" id="view-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Kupon Detayı</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div id="coupon"></div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
                </div>

            </div>
        </div>
    </div>
    <!-- End modal -->

    <script type="text/javascript">

        function send_membership_invitation()
        {
            let url = base_url + 'ajax/send_membership_invitation';
            let email = $('[name="UserEmail"]').val();
            let notify_message;
            let notify_type;

            $('.invitation-btn').text('Gönderiliyor...');
            $('.invitation-btn').attr('disabled',true);

            $.ajax({
                url : url,
                type: 'POST',
                data: {'UserEmail': email},
                
                dataType: 'JSON',
                success: function(data)
                {
                    if(data.status == 'login')
                    {
                        window.location.href = base_url + 'auth/login';
                    }

                    if(data.status) 
                    {
                        notify_message = data.message;
                        notify_type = 'success';
                    }
                    else
                    {   
                        notify_message = data.errors.UserEmail;
                        notify_type = 'warning';
                    }

                    Swal.fire({
                        title: notify_message,
                        icon: notify_type,
                    })
                   
                    $('.invitation-btn').text('Kaydet'); 
                    $('.invitation-btn').attr('disabled',false);
                    
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Email gönderilirken bir sorun oluştu');
                    $('.invitation-btn').text('Kaydet'); 
                    $('.invitation-btn').attr('disabled',false);
                }
            });
        }

        function coupon_view(coupon_content_id)
        {
            $('#coupon').html('');

            $.ajax({
                url : base_url + 'ajax/view_coupon_detail/' + coupon_content_id,
                type: 'GET',
                dataType: 'JSON',
                success: function(data)
                {
                    if (typeof(data.status) != "undefined" && data.status !== null)
                    {
                        if(data.status == 'login')
                        {
                            window.location.href = base_url + 'auth/login';
                        }
                    }
                    
                    let coupon = data;
                    let discount;
                    let end_date;
                    let is_used_text;

                    if (coupon.CouponAmount)
                    {
                        discount = number_format(coupon.CouponAmount) + '₺';
                    }
                    else
                    {
                        discount = '%' + coupon.CouponRate;
                    }

                    if (coupon.CouponIsEndDate == 1)
                    {
                        end_date = 'Zaman kısıtlaması yok';
                    }
                    else
                    {
                        end_date = coupon.CouponEndDate   
                    }

                    if (coupon.UserCouponIsUsed == 1)
                    {
                        is_used_text = '<span class="badge badge-secondary p-2">Kullanıldı</span>';
                    }
                    else
                    {
                        is_used_text = '<span class="badge badge-success p-2">Kullanılmadı</span>';
                    }

                    $('#coupon').append(
                        '<div class="row">' +

                            '<div class="col-md-12">' +
                                '<div class="card mb-3">' +
                                    '<div class="card-body">' +
                                        '<div><b>Kupon:</b> ' + coupon.CouponName + '</div>' +
                                        '<div><b>Kupon Kodu:</b> ' + coupon.CouponCode + '</div>' +
                                        '<div><b>Açıklama:</b> ' + coupon.CouponDescription + '</div>' +
                                        '<div><b>İndirim:</b> ' + discount + '</div>' +
                                        '<div><b>Sepetin Minumum Tutarı:</b> ' + coupon.CouponMinAmount + '₺' + '</div>' +
                                        '<div><b>Başlangıç Tarihi:</b> ' + coupon.CouponStartDate + '</div>' +
                                        '<div><b>Bitiş Tarihi:</b> ' + end_date + '</div>' +
                                        '<div><b>Durumu:</b> ' + is_used_text + '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +

                        '</div>'
                    );


                    $('#view-modal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error get data from ajax');
                }
            });
        }

    </script>

</body>
    
</html>