<!DOCTYPE html>
<html lang="tr">
	
<head>
	<meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Adreslerim">
    <meta name="keywords" content="Adreslerim">

    <title>Adreslerim</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
	
	<?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page profile">
        <div class="container">
            <div class="sub-title">Adreslerim</div>
            <div class="row">
                <div class="col-md-3">

                    <?php include(dirname(__DIR__) . '/inc/profile_left.php'); ?>

                </div>
                <div class="col-md-9">
                    
                    <div class="list-area">

                        <div class="title-area">
                            <div class="row">
                                <div class="col-md-3 col-3">Başlık</div>
                                <div class="col-md-7 col-6">Adres</div>
                                <div class="col-md-2 col-3 text-center">İşlemler</div>
                            </div>
                        </div>

                        <div class="cart-adresses">

                            <div class="row-item">
                                <div class="row pt-2">

                                    <div class="col-md-12 d-flex align-self-center">
                                        <a href="javascript:void(0)" onclick="address_add()" class="btn btn-light w-100">
                                            Yeni Ekle <i class="las la-plus"></i>
                                        </a>
                                    </div>

                                </div>
                            </div>

                            <?php if ($address): ?>
                            <?php foreach ($address as $key => $value): ?>
                            <div class="row-item" id="address-item-<?=$value['AddressID']?>">
                                <div class="row p-3">

                                    <div class="col-md-3 col-3 d-flex">
                                        <span class="align-self-center"><?=$value['AddressTitle']?></span>
                                    </div>

                                    <div class="col-md-7 col-6 d-flex">
                                        <span class="align-self-center">
                                            <?php 
                                                $city_and_district = !empty($value['CityName']) ? $value['DistrictName'] . '/' . $value['CityName'] : $value['AddressAlternativeCity'];
                                            ?>
                                            <?=$value['AddressOpenAddress'] . ' ' . $city_and_district?>
                                        </span>
                                    </div>

                                    <div class="col-md-2 col-3">
                                        <div class="float-right">
                                            <a href="javascript:void(0)" onclick="address_edit(<?=$value['AddressID']?>)" class="btn btn-light mr-1">
                                                <i class="las la-pen m-0"></i>
                                            </a>
                                            <a href="javascript:void(0)" onclick="address_destroy(<?=$value['AddressID']?>)" class="btn btn-light mr-1">
                                                <i class="las la-times m-0"></i>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <?php endforeach ?>
                            <?php endif ?>

                        </div>

                    </div>

                </div>
            </div>


        </div>

    </div>
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <?php include(dirname(__DIR__) . '/inc/address_transactions.php'); ?>

</body>
	
</html>