<!DOCTYPE html>
<html lang="tr">
    
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Siparişlerim">
    <meta name="keywords" content="Siparişlerim">

    <title>Siparişlerim</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
    
    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page profile">
        <div class="container">
            <div class="sub-title">Siparişlerim</div>
            <div class="row">
                <div class="col-md-3">

                    <?php include(dirname(__DIR__) . '/inc/profile_left.php'); ?>

                </div>
                <div class="col-md-9">
                    
                    <div class="list-area">

                        <div class="title-area">
                            <div class="row">
                                <div class="col-md-2">Sipariş Numarası</div>
                                <div class="col-md-4">Sipariş Özeti</div>
                                <div class="col-md-2">Tutar</div>
                                <div class="col-md-2">Ödeme Durumu</div>
                                <div class="col-md-2">Kargo Durumu</div>
                            </div>
                        </div>

                        <div class="list-detail">

                            <?php if ($orders): ?>
                            <?php foreach ($orders as $key => $value): ?>
                            <div class="row-item" id="order-item-<?=$value['OrderID']?>">                        
                                <div class="row p-3">

                                    <div class="col-md-2 d-flex">
                                        <a href="javascript:void(0)" onclick="order_view('<?=$value['OrderTrackingNumber']?>')">
                                            <?=$value['OrderTrackingNumber']?>
                                        </a>
                                    </div>

                                    <div class="col-md-4">

                                        <span class="d-block">
                                            <?='<u>Adet:</u> ' . $value['OrderDetailTotal'] . '<br />'?>
                                            <?='Tarih: ' . datetime_format_render($value['OrderCreatedAt']) . '<br />'?>
                                            <?='Teslimat Adresi: ' . $value['OrderDeliveryOpenAddress'] . ' ' . $value['OrderDeliveryDistrict'] . '/' . $value['OrderDeliveryCity']?>
                                        </span>

                                        <?php if ($value['OrderShippingStatus'] == 'pending' || $value['OrderShippingStatus'] == 'preparing'): ?>
                                        <a href="javascript:void(0)" onclick="order_address_view(<?=$value['OrderID']?>)" class="btn btn-secondary small-btn">
                                            Adresi Düzenle
                                        </a>
                                        <?php endif ?>

                                    </div>

                                    <div class="col-md-2">
                                        <div class="float-left">
                                           <?=number_format_render($value['OrderLatestAmount']) . $value['OrderCurrencyCode']?>
                                        </div>
                                    </div>

                                    <div class="col-md-2 pl-2 pr-2">
                                        <div class="float-left">
                                            <?php if ($value['OrderStatus'] == 'pendingPayment'){ ?>
                                                <a href="<?=base_url() . 'order/success/' . $value['OrderID'] . '?payment=' . $value['OrderPaymentType']?>" class="btn btn-warning small-btn">
                                                    Ödeme Yapınız
                                                </a>
                                            <?php }elseif ($value['OrderShippingStatus'] == 'pending' && $value['OrderStatus'] == 'completed'){ ?>
                                                <a href="javascript:void(0)" onclick="cancel_payment(<?=$value['OrderID']?>)" class="btn btn-danger small-btn">
                                                    Ödemeyi İptal Et
                                                </a>
                                            <?php }else{ ?>
                                                <?=$value['OrderStatusText']?>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="float-left">
                                           <?=$value['OrderShippingStatusText']?><br />
                                           <?=!empty($value['OrderCargoNumber']) ? 'Kargo Takip No: ' . $value['OrderCargoNumber'] : '' ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <?php endforeach ?>
                            <?php endif ?>

                        </div>

                    </div>

                </div>
            </div>


        </div>

    </div>

    <!-- order address update start -->
    <div class="modal" id="order-address-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Sipariş Adresi</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body form-area">
                    <form action="#" id="order-address-form" class="row">

                        <input type="hidden" value="" name="OrderID"/> 

                        <div class="col-md-12">
                            <h6 class="modal-title"><u>Teslimat Adresi</u></h6>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Adınız</label>
                                <input name="OrderDeliveryFirstName" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Soyadınız</label>
                                <input name="OrderDeliveryLastName" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">TC Kimlik No</label>
                                <input name="OrderDeliveryIdentityNumber" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Posta Kodu</label>
                                <input name="OrderDeliveryZipCode" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Email</label>
                                <input name="OrderDeliveryEmail" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Telefon</label>
                                <input name="OrderDeliveryPhone" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Ülke</label>
                                <input name="OrderDeliveryCountry" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Şehir</label>
                                <input name="OrderDeliveryCity" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">İlçe</label>
                                <input name="OrderDeliveryDistrict" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="input-label">Açık Adres</label>
                                <input name="OrderDeliveryOpenAddress" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h6 class="modal-title"><u>Fatura Adresi</u></h6>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Adınız</label>
                                <input name="OrderBillingFirstName" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Soyadınız</label>
                                <input name="OrderBillingLastName" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">TC Kimlik No</label>
                                <input name="OrderBillingIdentityNumber" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Posta Kodu</label>
                                <input name="OrderBillingZipCode" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Email</label>
                                <input name="OrderBillingEmail" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Telefon</label>
                                <input name="OrderBillingPhone" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Ülke</label>
                                <input name="OrderBillingCountry" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Şehir</label>
                                <input name="OrderBillingCity" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">İlçe</label>
                                <input name="OrderBillingDistrict" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="input-label">Açık Adres</label>
                                <input name="OrderBillingOpenAddress" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Firma Adı</label>
                                <input name="OrderBillingCompanyName" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Vergi Numarası</label>
                                <input name="OrderBillingCompanyTaxNumber" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="input-label">Vergi Dairesi</label>
                                <input name="OrderBillingCompanyTaxAdministration" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" id="btn-save" onclick="order_address_update()" class="btn btn-primary">Güncelle</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
                </div>

            </div>
        </div>
    </div>
    <!-- order address update end -->

    <!-- order view start -->
    <div class="modal" id="view-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Sipariş Detayı</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div id="order"></div>
                    <div id="order-detail"></div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
                </div>

            </div>
        </div>
    </div>
    <!-- order view end -->
  
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <script type="text/javascript">
        $(document).ready(function(){
            var tracking_number = getUrlParameter('tracking_number');

            if (typeof(tracking_number) != "undefined" && tracking_number !== null)
            {
                order_view(tracking_number);
            }
        });

        function cancel_payment(order_id)
        {
            Swal.fire({
                title: 'Ödemeyi iptal etmek istediğinize emin misiniz?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonText: 'Vazgeç',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ödemeyi iptal et'
            }).then((result) => {
                if (result.value) 
                {
                    $('#preloader').fadeIn();

                    let message = '';
                    let icon = '';
                    let text = '';

                    $.ajax({
                        url : base_url + 'ajax/cancel_payment/' + order_id,
                        type: 'GET',
                        success: function(data)
                        {
                            if(data.status == 'login')
                            {
                                window.location.href = base_url + 'auth/login';
                            }
                            
                            $('#preloader').fadeOut();

                            data = JSON.parse(data);

                            if(data.status)
                            {
                                icon = 'success';
                                message = 'Siparişiniz iptal edildi.';
                                text = 'En kısa zamanda para iadeniz gerçekleşecektir.';
                            }
                            else
                            {
                                icon = 'warning';
                                message = data.message;
                            }
                            
                            Swal.fire({
                                title: message,
                                icon: icon,
                                text: text,
                            }).then((result) => {
                                if (result.value) 
                                {
                                    window.location.href = window.location.href;
                                }
                            });
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Ödeme iptal edilirken bir sorun oluştu.');
                        }
                    });
                }
            });
        }

        function order_view(tracking_number)
        {
            $('#order, #order-detail').html('');

            $.ajax({
                url : base_url + 'ajax/view_order_details/' + tracking_number,
                type: 'GET',
                dataType: 'JSON',
                success: function(data)
                {
                    if (typeof(data.status) != "undefined" && data.status !== null)
                    {
                        if(data.status == 'login')
                        {
                            window.location.href = base_url + 'auth/login';
                        }
                    }
                    
                    let order = data.order;
                    let delivery_district = '';
                    let billing_district = '';
                    let coupon_discount = '';

                    if (order.OrderDeliveryDistrict != '')
                    {
                        delivery_district = '/' + order.OrderDeliveryDistrict;
                    }

                    if (order.OrderBillingDistrict != '')
                    {
                        billing_district = '/' + order.OrderBillingDistrict;
                    }

                    if (order.OrderCouponContentID)
                    {
                        if (order.OrderCouponAmount)
                        {
                            coupon_discount = '<div>Kupon İndirimi: ' + order.OrderCouponAmount + order.OrderCurrencyCode + '</div>';
                        }
                        else
                        {
                            coupon_discount = '<div>Kupon İndirimi: ' + order.OrderCouponRate + '%' + '</div>';
                        }
                    }   

                    $('#order').append(
                        '<div class="row">' +

                            '<div class="col-md-12">' +
                                '<div class="card mb-3">' +
                                    '<div class="card-body">' +
                                        '<div><b>Ödeme Bilgileri</b></div>' +   
                                        '<div>Sipariş No: ' + order.OrderTrackingNumber + '</div>' +
                                        '<div>Sipariş Tutarı: ' + order.OrderVatInclusiveAmount + order.OrderCurrencyCode + '</div>' +
                                        '<div>İndirim Tutarı: ' + order.OrderDiscountAmount + order.OrderCurrencyCode + '</div>' +
                                        coupon_discount +
                                        '<div>Kargo Ücreti: ' + order.OrderCargoAmount + order.OrderCurrencyCode + '</div>' +
                                        '<div>Toplam Tutar: ' + order.OrderLatestAmount + order.OrderCurrencyCode + '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +                     

                            '<div class="col-md-6">' +
                                '<div class="card mb-3">' +
                                    '<div class="card-body">' +
                                        '<div><b>Teslimat Adresi</b></div>' +
                                        '<div>' + order.OrderDeliveryFirstName + ' ' + order.OrderDeliveryLastName + '</div>' +
                                        '<div>' + order.OrderDeliveryPhone + '</div>' +
                                        '<div>' + order.OrderDeliveryOpenAddress + '</div>' +
                                        '<div>' + order.OrderDeliveryCity + delivery_district + '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +

                            '<div class="col-md-6">' +
                                '<div class="card mb-3">' +
                                    '<div class="card-body">' +
                                        '<div><b>Fatura Adresi</b></div>' +
                                        '<div>' + order.OrderBillingFirstName + ' ' + order.OrderBillingLastName + '</div>' +
                                        '<div>' + order.OrderBillingPhone + '</div>' +
                                        '<div>' + order.OrderBillingOpenAddress + '</div>' +
                                        '<div>' + order.OrderBillingCity + billing_district + '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +

                        '</div>'
                    );

                    $.each(data.details, function(count, item) {

                        let discount_area = '';
                        let detail_photo = '';
                        if (parseFloat(item.DetailDiscountPrice) > 0)
                        {
                            discount_area = '<div>İndirim: ' + item.DetailDiscountPrice + item.DetailCurrencyCode + '</div>' +
                                            '<div>İndirimli Fiyatı: ' + item.DetailDiscountInclusivePrice + item.DetailCurrencyCode + '</div>';
                        }

                        if (item.DetailProductPhoto)
                        {
                            detail_photo = server_url + item.DetailProductPhoto
                        }
                        else
                        {
                            detail_photo = server_url + 'upload/product/null-photo.png';
                        }

                        $('#order-detail').append(
                            '<div class="card mb-2">' +
                                '<div class="card-body">' +
                                    '<div class="row">' +
                                        '<div class="col-md-2">' +
                                            '<img src="'+ detail_photo +'" class="w-100">' +
                                        '</div>' +
                                        '<div class="col-md-5">' +
                                            '<div><b>' + item.DetailProductName + ' (' + item.DetailSKU + ')</b></div>' +
                                            '<div>Birim Fiyatı: ' + item.DetailVatInclusivePrice + item.DetailCurrencyCode + '</div>' +
                                            discount_area +
                                            '<div>Adet: ' + item.DetailQuantity + '</div>' +
                                            '<div>Toplam Fiyat: ' + item.DetailLatestPrice + item.DetailCurrencyCode + '</div>' +
                                        '</div>' +
                                        '<div class="col-md-5">' +
                                            '<div><b>Ürün Detayı</b></div>' +
                                            '<div id="order-p-option"></div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>'
                        );

                        $.each(JSON.parse(item.DetailProductOptionContent), function(key, val){
                            $('#order-p-option').append(
                                '<div>' + val.group + ': ' + val.option + '</div>'
                            );
                        });
                        
                    });

                    $('#view-modal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    Swal.fire({
                        title: 'Böyle bir siparişiniz bulunmaktadır.',
                        icon: 'warning',
                    })
                }
            });
        }

        function order_address_view(order_id)
        {
            $.ajax({
                url : base_url + 'ajax/view_order/' + order_id,
                type: 'GET',
                dataType: 'JSON',
                success: function(data)
                {
                    if (typeof(data.status) != "undefined" && data.status !== null)
                    {
                        if(data.status == 'login')
                        {
                            window.location.href = base_url + 'auth/login';
                        }
                    }

                    $('[name="OrderID"]').val(data.OrderID);
                    $('[name="OrderDeliveryFirstName"]').val(data.OrderDeliveryFirstName);
                    $('[name="OrderDeliveryLastName"]').val(data.OrderDeliveryLastName);
                    $('[name="OrderDeliveryIdentityNumber"]').val(data.OrderDeliveryIdentityNumber);
                    $('[name="OrderDeliveryZipCode"]').val(data.OrderDeliveryZipCode);
                    $('[name="OrderDeliveryEmail"]').val(data.OrderDeliveryEmail);
                    $('[name="OrderDeliveryPhone"]').val(data.OrderDeliveryPhone);
                    $('[name="OrderDeliveryCountry"]').val(data.OrderDeliveryCountry);
                    $('[name="OrderDeliveryCity"]').val(data.OrderDeliveryCity);
                    $('[name="OrderDeliveryDistrict"]').val(data.OrderDeliveryDistrict);
                    $('[name="OrderDeliveryOpenAddress"]').val(data.OrderDeliveryOpenAddress);
                    $('[name="OrderBillingFirstName"]').val(data.OrderBillingFirstName);
                    $('[name="OrderBillingLastName"]').val(data.OrderBillingLastName);
                    $('[name="OrderBillingIdentityNumber"]').val(data.OrderBillingIdentityNumber);
                    $('[name="OrderBillingZipCode"]').val(data.OrderBillingZipCode);
                    $('[name="OrderBillingEmail"]').val(data.OrderBillingEmail);
                    $('[name="OrderBillingPhone"]').val(data.OrderBillingPhone);
                    $('[name="OrderBillingCountry"]').val(data.OrderBillingCountry);
                    $('[name="OrderBillingCity"]').val(data.OrderBillingCity);
                    $('[name="OrderBillingDistrict"]').val(data.OrderBillingDistrict);
                    $('[name="OrderBillingOpenAddress"]').val(data.OrderBillingOpenAddress);
                    $('[name="OrderBillingCompanyName"]').val(data.OrderBillingCompanyName);
                    $('[name="OrderBillingCompanyTaxNumber"]').val(data.OrderBillingCompanyTaxNumber == '0' ? '' : data.OrderBillingCompanyTaxNumber);
                    $('[name="OrderBillingCompanyTaxAdministration"]').val(data.OrderBillingCompanyTaxAdministration);

                    $('#order-address-modal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    Swal.fire({
                        title: 'Sipariş adresi bulunamadı.',
                        icon: 'warning',
                    })
                }
            });
        }

        function order_address_update()
        {
            let formData = new FormData($('#order-address-form')[0]);

            $('#btn-save').text('Güncelleniyor...');
            $('#btn-save').attr('disabled',true);

            $.ajax({
                url : base_url + 'ajax/update_order_address',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data)
                {
                    if (typeof(data.status) != "undefined" && data.status !== null)
                    {
                        if(data.status == 'login')
                        {
                            window.location.href = base_url + 'auth/login';
                        }
                    }

                    if(data.status) 
                    {
                        Swal.fire({
                            title: 'Sipariş adresi başarılı bir şekilde güncellendi.',
                            icon: 'success',
                        }).then((result) => {
                            if (result.value) 
                            {
                                window.location.href = window.location.href;
                            }
                        });
                    }
                    else
                    {   
                        for (var i = 0; i < data.error.inputerror.length; i++) 
                        {
                            $('[name="'+data.error.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                            $('[name="'+data.error.inputerror[i]+'"]').next().text(data.error.error_string[i]); 
                        }
                        
                        $('#order-address-modal').stop().animate({
                            scrollTop:0
                        });
                    }
                   
                    $('#btn-save').text('Güncelle'); 
                    $('#btn-save').attr('disabled',false);
                    
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btn-save').text('Güncelle'); 
                    $('#btn-save').attr('disabled',false);
                }
            });
        }

    </script>

</body>
    
</html>