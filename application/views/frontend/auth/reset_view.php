<!DOCTYPE html>
<html lang="tr">
    
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Şifre Yenile">
    <meta name="keywords" content="Şifre Yenile">
    <title>Şifre Yenile | petshopevinde.com</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
    
    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <?php  
        $reset_status = $this->session->flashdata('reset_status');
        $flashdata = null;
       if (isset($reset_status)) {
            $flashdata = $reset_status;
        }
    ?>

    <div class="sub-page login">
        <div class="container">

            <form method="post" autocomplete="off">

                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <div class="login-form">
                            <div class="sub-title text-center mb-4">Şifreyi Yenile</div>
                            <div class="row">

                                <div class="col-md-12">
                                    <?php if ($flashdata): ?>
                                    <div class="alert special-alert <?=$flashdata['status'] ? 'alert-success' : 'alert-danger'?> mb-2">
                                       <span><?=$flashdata['message']?></span>
                                    </div>
                                    <?php endif ?>
                                </div> 

                                <div class="col-md-12 mb-2">
                                    <div class="form-group <?=isset($errors['UserPassword']) ? 'has-error' : ''?>">
                                        <label for="pwd">Şifre:</label>
                                        <input type="password" name="UserPassword" class="form-control" value=""/>
                                        <div class="help-feedback"><?=isset($errors['UserPassword']) ? $errors['UserPassword'] : ''?></div>
                                        <div class="clear"></div>
                                        <small>
                                           Şifrenizin güçlü ve hatırlanması kolay olduğundan emin olun.
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group<?=isset($errors['UserPasswordAgain']) ? 'has-error' : ''?>">
                                        <label for="pwd">Şifre Tekrar:</label>
                                        <input type="password" name="UserPasswordAgain" class="form-control" value=""/>
                                        <div class="help-feedback"><?=isset($errors['UserPasswordAgain']) ? $errors['UserPasswordAgain'] : ''?></div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button class="btn btn-black w-100 p-3 mt-3" id="send-btn">ŞİFREYİ YENİLE</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>
  
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <script type="text/javascript">
        
        $(document).ready(function(){
            
            $('#send-btn').click(function(){
                $('#preloader').fadeIn();
            });

        })

    </script>

</body>
    
</html>
