<!DOCTYPE html>
<html lang="tr">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Giriş Yap">
	<title>Giriş Yap | petshopevinde.com</title>

	<?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

	<?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

	<?php
	$create_status = $this->session->flashdata('create_status');
	$reset_status = $this->session->flashdata('reset_status');
	$forgot_status = $this->session->flashdata('forgot_status');
	$account_enabled = $this->session->flashdata('account_enabled');
	$login_status = $this->session->flashdata('login_status');
	$flashdata = null;
	if (isset($create_status)) {
		$flashdata = $create_status;
	} elseif (isset($reset_status)) {
		$flashdata = $reset_status;
	} elseif (isset($forgot_status)) {
		$flashdata = $forgot_status;
	} elseif (isset($account_enabled)) {
		$flashdata = $account_enabled;
	} elseif (isset($login_status)) {
		$flashdata = $login_status;
	}
	?>

	<div class="sub-page login-register">
		<div class="container">
			<form action="<?= base_url() ?>auth/login" method="post" autocomplete="off">
				<div class="form-wrapper">
					<nav>
						<div class="nav nav-tabs d-flex" id="nav-tab" role="tablist">
							<a class="nav-item nav-link show flex-fill text-center active" data-toggle="tab" role="tab" aria-selected="true"> <i class="la la-lock-open"></i> GİRİŞ YAP</a>
							<a class="nav-item nav-link flex-fill text-center" href="<?= base_url() ?>uyelik/uye-ol" title="Üye Olun" aria-selected="false"><i class="las la-user-plus"></i> ÜYE OL</a>
						</div>
					</nav>
					<div class="tab-content border p-3" id="nav-tabContent">
						<div class="tab-pane fade active show" id="login" role="tabpanel">
							<div class="login-form">
								<div class="sub-title text-center mb-4">Lütfen Giriş Yapın</div>
								<div class="row">
									<div class="col-md-12">
										<?php if ($flashdata) : ?>
											<div class="alert special-alert <?= $flashdata['status'] ? 'alert-success' : 'alert-danger' ?> mb-2">
												<span><?= $flashdata['message'] ?></span>
											</div>
										<?php endif ?>
									</div>
									<div class="col-md-12 mb-2">
										<div class="form-group <?= isset($errors['UserEmail']) ? 'has-error' : '' ?>">
											<label for="email">E-Posta</label>
											<input type="text" name="UserEmail" class="form-control" value="<?= set_value('UserEmail') ? set_value('UserEmail') : $this->session->tempdata('RememberUserEmail') ?>" />
											<div class="help-feedback"><?= isset($errors['UserEmail']) ? $errors['UserEmail'] : '' ?></div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group <?= isset($errors['UserPassword']) ? 'has-error' : '' ?>">
											<label for="email">Şifre</label>
											<input type="password" name="UserPassword" class="form-control" value="<?= set_value('UserPassword') ? '' : $this->session->tempdata('RememberUserPassword') ?>" />
											<div class="help-feedback"><?= isset($errors['UserPassword']) ? $errors['UserPassword'] : '' ?></div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="form-check-label">
												<input type="checkbox" name="UserRemember" <?= $this->session->tempdata('RememberUserEmail') ? 'checked="checked"' : '' ?> /> Beni hatırla
											</label>
										</div>
									</div>
									<div class="col-md-12">
										<button class="btn btn-black w-100 p-3 mt-3" id="send-btn">GİRİŞ YAP</button>
									</div>
									<div class="col-md-6">
										<a href="<?= base_url() ?>uyelik/sifremi-unuttum" class="btn-block pt-3 pb-3">Şifremi Unuttum</a>
									</div>
									<div class="col-md-6">
										<a href="<?= base_url() ?>uyelik/uye-ol" class="btn-block pt-3 pb-3 text-right">Üye Olun</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>

		</div>
	</div>

	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#send-btn').click(function() {
				$('#preloader').fadeIn();
			});

		})
	</script>

</body>

</html>