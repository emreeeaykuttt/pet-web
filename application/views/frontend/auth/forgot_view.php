<!DOCTYPE html>
<html lang="tr">
    
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Şifrenizi mi Unuttunuz?">
    <meta name="keywords" content="Şifrenizi mi Unuttunuz?">
    <title>Şifrenizi mi Unuttunuz? | petshopevinde.com</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
    
    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <?php  
        $forgot_status = $this->session->flashdata('forgot_status');
        $flashdata = null;
       if (isset($forgot_status)) {
            $flashdata = $forgot_status;
        }
    ?>

    <div class="sub-page login">
        <div class="container">

            <form action="<?=base_url()?>auth/forgot" method="post" autocomplete="off">

                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <div class="login-form">
                            <div class="sub-title text-center mb-4">Şifrenizi mi unuttunuz?</div>
                            <div class="row">

                                <div class="col-md-12">
                                    <?php if ($flashdata): ?>
                                    <div class="alert special-alert <?=$flashdata['status'] ? 'alert-success' : 'alert-danger'?> mb-2">
                                       <span><?=$flashdata['message']?></span>
                                    </div>
                                    <?php endif ?>
                                </div> 

                                <div class="col-md-12 mb-2">
                                    <div class="form-group <?=isset($errors['UserEmail']) ? 'has-error' : ''?>">
                                        <label for="email">E-Posta:</label>
                                        <input type="text" name="UserEmail" class="form-control" value="" />
                                        <div class="help-feedback"><?=isset($errors['UserEmail']) ? $errors['UserEmail'] : ''?></div>
                                        <div class="clear"></div>
                                        <small>
                                           "Şifreyi Sıfırla" butonuna tıklayınca şifre sıfırlama bağlantısı gönderilecektir.
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button class="btn btn-black w-100 p-3 mt-3" id="send-btn">ŞİFREYİ SIFIRLA</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>

  
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <script type="text/javascript">
        
        $(document).ready(function(){

            $('#send-btn').click(function(){
                $('#preloader').fadeIn();
            });

        });

    </script>

</body>
    
</html>
