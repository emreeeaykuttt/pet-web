 <!DOCTYPE html>
<html lang="tr">
	
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Üye Olmadan Devam Et">
    <meta name="keywords" content="Üye Olmadan Devam Et">
    <title>Üye Olmadan Devam Et | petshopevinde.com</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
	
	<?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

	<div class="sub-page login-register">
        <div class="container">

        	<form action="<?=base_url()?>auth/guest" method="post" autocomplete="off">

	            <div class="row">

	                <div class="col-md-12">
	                    <div class="register-form">

	                    	<div class="row">
		                    	<div class="col-md-12 sign-buttons">
		                    		<a href="<?=base_url()?>uyelik/uye-ol" class="btn-block">Üye Ol</a>
			                        <a href="<?=base_url()?>uyelik/giris-yap" class="btn-block">Üyeysen Giriş Yap</a>
			                    </div>
		                    </div>

	                        <div class="sub-title text-center mb-4">Üye Olmadan Devam Et</div>
	                        <div class="row">

	                            <div class="col-md-6">
	                                <div class="form-group <?=isset($errors['UserFirstName']) ? 'has-error' : ''?>">
										<label for="email">Adınız</label>
										<input type="text" name="UserFirstName" class="form-control" value="<?=set_value('UserFirstName')?>"  />
										<div class="help-feedback"><?=isset($errors['UserFirstName']) ? $errors['UserFirstName'] : ''?></div>
									</div>
	                            </div>

	                            <div class="col-md-6">
	                                <div class="form-group <?=isset($errors['UserLastName']) ? 'has-error' : ''?>">
										<label for="email">Soyadınız</label>
										<input type="text" name="UserLastName" class="form-control" value="<?=set_value('UserLastName')?>"  />
										<div class="help-feedback"><?=isset($errors['UserLastName']) ? $errors['UserLastName'] : ''?></div>
									</div>
	                            </div>

	                            <div class="col-md-12">
	                                <div class="form-group <?=isset($errors['UserEmail']) ? 'has-error' : ''?>">
										<label for="email">E-Posta Adresi</label>
										<input type="text" name="UserEmail" class="form-control" value="<?=$this->input->get('email') ? $this->input->get('email') : set_value('UserEmail')?>"  />
										<div class="help-feedback"><?=isset($errors['UserEmail']) ? $errors['UserEmail'] : ''?></div>
									</div>
	                            </div>

	                            <div class="col-md-4">
	                                <div class="form-group <?=isset($errors['AddressIdentityNumber']) ? 'has-error' : ''?>">
										<label for="email">TC Kimlik No</label>
										<input type="text" name="AddressIdentityNumber" class="form-control" value="<?=set_value('AddressIdentityNumber')?>"  />
										<div class="help-feedback"><?=isset($errors['AddressIdentityNumber']) ? $errors['AddressIdentityNumber'] : ''?></div>
									</div>
	                            </div>

	                            <div class="col-md-4">
			                        <div class="form-group <?=isset($errors['UserPhone']) ? 'has-error' : ''?>">
			                            <label class="input-label">Telefon</label>
			                            <input name="UserPhone" class="form-control" type="text" value="<?=set_value('UserPhone')?>">
			                            <div class="help-feedback"><?=isset($errors['UserPhone']) ? $errors['UserPhone'] : ''?></div>
			                        </div>
			                    </div>

			                    <div class="col-md-4">
	                                <div class="form-group <?=isset($errors['AddressZipCode']) ? 'has-error' : ''?>">
										<label for="email">Posta Kodu</label>
										<input type="text" name="AddressZipCode" class="form-control" value="<?=set_value('AddressZipCode')?>"  />
										<div class="help-feedback"><?=isset($errors['AddressZipCode']) ? $errors['AddressZipCode'] : ''?></div>
									</div>
	                            </div>

	                            <div class="col-md-4">
			                        <div class="form-group <?=isset($errors['AddressCountryID']) ? 'has-error' : ''?>">
			                            <label class="input-label">Ülke</label>
			                            <select class="form-control" id="country-id" name="AddressCountryID">
			                                <option value="">Seçiniz...</option>
			                                <?php foreach ($countries as $key => $country): ?>
			                                    <option value="<?=$country['CountryContentID']?>" <?=set_value('AddressCountryID') == $country['CountryContentID'] ? 'selected' : ''?>>
			                                    	<?=$country['CountryName']?>
			                                    </option>
			                                <?php endforeach ?>
			                            </select>
			                            <div class="help-feedback"><?=isset($errors['AddressCountryID']) ? $errors['AddressCountryID'] : ''?></div>
			                        </div>
			                    </div>

			                    <div class="col-md-4 city-area" style="<?=set_value('AddressCountryID') == 65 || set_value('AddressCountryID') == 0 ? 'display:block' : 'display:none'?>">
			                        <div class="form-group <?=isset($errors['AddressCityID']) ? 'has-error' : ''?>">
			                            <label class="input-label">Şehir</label>
			                            <select class="form-control" id="city-id" name="AddressCityID">
			                                <option value="">Seçiniz...</option>
			                            </select>
			                            <div class="help-feedback"><?=isset($errors['AddressCityID']) ? $errors['AddressCityID'] : ''?></div>
			                        </div>
			                    </div>

			                    <div class="col-md-4 district-area" style="<?=set_value('AddressCountryID') == 65 || set_value('AddressCountryID') == 0 ? 'display:block' : 'display:none'?>">
			                        <div class="form-group <?=isset($errors['AddressDistrictID']) ? 'has-error' : ''?>">
			                            <label class="input-label">İlçe</label>
			                            <select class="form-control" id="district-id" name="AddressDistrictID">
			                                <option value="">Seçiniz...</option>
			                            </select>
			                            <div class="help-feedback"><?=isset($errors['AddressDistrictID']) ? $errors['AddressDistrictID'] : ''?></div>
			                        </div>
			                    </div>

			                    <div class="col-md-8 alternative-city-area" style="<?=set_value('AddressCountryID') == 65 || set_value('AddressCountryID') == 0 ? 'display:none' : 'display:block'?>">
			                        <div class="form-group <?=isset($errors['AddressAlternativeCity']) ? 'has-error' : ''?>">
			                            <label class="input-label">Şehir</label>
			                            <input name="AddressAlternativeCity" id="alternative-city" class="form-control" type="text">
			                            <div class="help-feedback"><?=isset($errors['AddressAlternativeCity']) ? $errors['AddressAlternativeCity'] : ''?></div>
			                        </div>
			                    </div>

			                    <div class="col-md-12">
			                        <div class="form-group <?=isset($errors['AddressOpenAddress']) ? 'has-error' : ''?>">
			                            <label class="input-label">Açık Adres</label>
			                            <textarea name="AddressOpenAddress" placeholder="Mahalle, sokak, cadde ve diğer bilgilerinizi giriniz" cols="3" class="form-control"><?=set_value('AddressOpenAddress')?></textarea>
			                            <div class="help-feedback"><?=isset($errors['AddressOpenAddress']) ? $errors['AddressOpenAddress'] : ''?></div>
			                        </div>
			                    </div>

	                            <div class="col-md-12">
	                            	<div class="form-group mb-1">
										<label class="form-check-label">
											<input type="checkbox" name="UserDistanceSalesContract" value="1" <?=set_value('UserDistanceSalesContract') ? 'checked' : '' ?> /> 
											<a href="javascript:void(0)" data-toggle="modal" data-target="#user-agreement-modal">Üyelik sözleşmesini kabul ediyorum</a>
											<div class="help-feedback"><?=isset($errors['UserDistanceSalesContract']) ? $errors['UserDistanceSalesContract'] : ''?></div>
										</label>
									</div>
								</div>

								<div class="col-md-12">
	                            	<div class="form-group">
										<label class="form-check-label">
											<input type="checkbox" name="UserCommunicationPermit" value="1" <?=set_value('UserCommunicationPermit') ? 'checked' : '' ?> /> 
											<a href="javascript:void" data-toggle="modal" data-target="#communication-permit-modal">Gizlilik politikasını kabul ediyorum</a>
											<div class="help-feedback"><?=isset($errors['UserCommunicationPermit']) ? $errors['UserCommunicationPermit'] : ''?></div>
										</label>
									</div>
								</div>

	                            <div class="col-md-12">
	                                <button class="btn btn-black w-100 p-3 mt-3" id="send-btn">Devam Et</button>
	                            </div>

	                        </div>
	                    </div>
	                </div>
	            </div>

	        </form>

        </div>
    </div>

    <!-- The UserDistanceSalesContract Modal -->
	<div class="modal" id="user-agreement-modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Üyelik Sözleşmesi</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					
					<p><strong>&Uuml;yelik S&ouml;zleşmesi</strong></p>

                    <p><strong>Taraflar</strong></p>

                    <p>İşbu &uuml;yelik s&ouml;zleşmesi, www.petshopevinde.com (Kısaca &quot;site&quot; olarak anılacaktır) ile www.petshopevinde.com&nbsp; sitesine &uuml;ye olurken &quot;&Uuml;ye&quot;nin elektronik olarak onay vermesi ile karşılıklı olarak kabul edilerek y&uuml;r&uuml;rl&uuml;ğe girmiştir.</p>

                    <p>Kullanıcı, Site&#39;ye &uuml;ye olurken verdiği elektronik onayla, &Uuml;yelik S&ouml;zleşmesi&#39;nin tamamını okuduğunu, i&ccedil;eriğini b&uuml;t&uuml;n&uuml; ile anladığını ve t&uuml;m h&uuml;k&uuml;mlerini onayladığını kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><strong>Tanımlar</strong></p>

                    <p>www.petshopevinde.com internet sitesinin faaliyetlerini y&uuml;r&uuml;ten Doa Satış Pazarlama ve Ticaret Ltd. Şti., Yenidoğan Mah. Halit Cansever Sok. No:32/1 İzmit Kocaeli adresinde mukim (Bundan b&ouml;yle &ldquo;satıcı&rdquo; olarak anılacaktır)</p>

                    <p>Site: www.petshopevinde.com tarafından belirlenen &ccedil;er&ccedil;eve i&ccedil;erisinde &ccedil;eşitli hizmetlerin ve i&ccedil;eriklerin sunulduğu &ccedil;evrimi&ccedil;i (on-line) ortamdan erişimi m&uuml;mk&uuml;n olan web sitesidir.</p>

                    <p>&Uuml;ye: Site&#39;ye &uuml;ye olan ve Site&#39;de sunulan Hizmet&#39;lerden, işbu s&ouml;zleşmede belirtilen koşullar dahilinde yararlanan ger&ccedil;ek veya t&uuml;zel kişi.</p>

                    <p>Alıcı: Site&#39;de sunulan Hizmet&#39;leri kullanmak suretiyle, satışa arz edilen mal ve/veya hizmetleri satın alan &Uuml;ye.</p>

                    <p>Kullanıcı: www.petshopevinde.com web sitesini alışveriş yaparak ya da alışveriş yapmaksızın ziyaret eden kişidir.</p>

                    <p>Onaylama İşlemi: &Uuml;ye tarafından, &uuml;yelik formunda www.petshopevinde.com sitesinden mesaj g&ouml;ndermesi ile Kullanıcı&#39; nın &uuml;yeliğini aktif hale getirilebilmesi i&ccedil;in yapılması gereken işlemdir.</p>

                    <p>Web Sitesi Kullanım Koşulları Ve &Uuml;yelik S&ouml;zleşmesi: Site vasıtasıyla sunulmakta olan ticari ve kişiye &ouml;zel nitelikteki hizmetlerden yararlanacak ger&ccedil;ek ve/veya t&uuml;zel kişilerle www.petshopevinde.com internet sitesi arasında elektronik ortamda akdedilen işbu s&ouml;zleşmedir.</p>

                    <p><strong>S&ouml;zleşmenin Konusu ve Kapsamı</strong></p>

                    <p>&Uuml;yelik S&ouml;zleşmesi&#39;nin konusu, Site&#39;de sunulan Hizmet&#39;lerin, bu Hizmet&#39;lerden yararlanma şartlarının ve tarafların hak ve y&uuml;k&uuml;ml&uuml;l&uuml;klerinin tespitidir.</p>

                    <p>&Uuml;ye S&ouml;zleşmesi&#39;nin kapsamı, işbu s&ouml;zleşme ve ekleri ile Site i&ccedil;erisinde yer alan, kullanıma, &uuml;yeliğe ve Hizmet&#39;lere ilişkin olarak satıcı tarafından yapılmış olan bilc&uuml;mle uyarı, yazı ve a&ccedil;ıklama gibi beyanlardır. &Uuml;ye, &Uuml;yelik S&ouml;zleşmesi&#39;nin h&uuml;k&uuml;mlerini kabul etmekle, Site i&ccedil;inde yer alan, kullanıma, &uuml;yeliğe ve Hizmet&#39;lere ilişkin olarak satıcı tarafından a&ccedil;ıklanan her t&uuml;rl&uuml; beyanı da kabul etmiş olmaktadır. &Uuml;ye, bahsi ge&ccedil;en beyanlarda belirtilen her t&uuml;rl&uuml; hususa uygun olarak davranacağını kabul, beyan ve taahh&uuml;t eder.</p>

                    <p>www.petshopevinde.com internet sitesi, Site &uuml;zerinden sunacağı hizmetler genel itibariyle T&uuml;ketici Hukuku mevzuatında tanımlanan elektronik ticaretten ibarettir.</p>

                    <p>www.petshopevinde.com internet sitesi, Site &uuml;zerinden vereceği hizmetler sınırlı sayıda olmamak &uuml;zere; www.petshopevinde.com adresinde satışa sunulan &uuml;r&uuml;nlerin; &Uuml;ye tarafından bedeli &ouml;dendikten sonra, tedarik&ccedil;inin stok durumunun m&uuml;sait olması halinde yani s&ouml;zleşmenin satıcıya y&uuml;klediği; malın teslim edilmesi borcunun ifa edilebilir olması durumunda; taahh&uuml;t edilen s&uuml;rede malın m&uuml;şteriye kargo firması tarafından www.petshopevinde.com adına ayıpsız olarak teslimidir.</p>

                    <p>www.petshopevinde.com Site &uuml;zerinden sunacağı hizmetlerin kapsamını ve niteliğini belirlemekte tamamen serbest olup, hizmetlere ilişkin olarak yapacağı değişiklikleri Site&#39;de yayınlamasıyla y&uuml;r&uuml;rl&uuml;ğe koymuş addedilir.</p>

                    <p>Site b&uuml;nyesinde sunulacak hizmetlerden yararlanabilmek i&ccedil;in kullanıcıların www.petshopevinde.com tarafından belirlenecek ve Site&#39;nin ilgili b&ouml;l&uuml;m&uuml;n&uuml;n i&ccedil;eriğinde belirtilecek &ouml;zellikleri taşıması gereklidir. www.petshopevinde.com bu &ouml;zellikleri belirlemekte tamamen serbest olup, &ouml;zelliklere ilişkin olarak yapacağı değişiklikleri Site&#39;de yayınlamasıyla y&uuml;r&uuml;rl&uuml;ğe koymuş addedilir.</p>

                    <p><strong>&Uuml;yelik ve Hizmet Kullanımı Şartları</strong></p>

                    <p>&Uuml;yelik, Site&#39;nin ilgili b&ouml;l&uuml;m&uuml;nden, &Uuml;ye olmak isteyen kişi tarafından Site&#39;ye &uuml;ye olmak i&ccedil;in gerekli kimlik bilgilerinin g&ouml;nderilmesi suretiyle kayıt işleminin yaptırılması ve satıcı tarafından kayıt işleminin onaylanması ile tamamlanır. &Uuml;yelik işlemi tamamlanmadan, işbu s&ouml;zleşmede tanımlanan &Uuml;ye olma hak ve yetkisine sahip olunamaz.</p>

                    <p>Siteye &uuml;ye olabilmek i&ccedil;in reşit olmak, t&uuml;zel kişi &uuml;yeler i&ccedil;in t&uuml;zel kişiyi temsil ve ilzam etmeye yetkili olmak ve satıcı tarafından işbu s&ouml;zleşme kapsamında ge&ccedil;ici olarak &uuml;yelikten uzaklaştırılmamış veya &uuml;yelikten s&uuml;resiz yasaklanmış olmamak gerekmektedir. Reşit olmayan veya t&uuml;zel kişi &uuml;yeler i&ccedil;in t&uuml;zel kişiyi temsil ve ilzam etmeye yetkili olmayan kişiler kanalıyla yapılan başvurular veya yukarıda belirtildiği gibi satıcı tarafından işbu s&ouml;zleşme kapsamında ge&ccedil;ici olarak &uuml;yelikten uzaklaştırılmış veya &uuml;yeliği askıya alınmış; &uuml;yelikten s&uuml;resiz yasaklanmış olan kişiler tarafından yapılan başvurular Site kayıt işlemlerini tamamlamış olsa dahi &Uuml;yeliğin doğurduğu hakların kullanılmasına engeldir.</p>

                    <p>Satıcı her zaman herhangi bir gerek&ccedil;e g&ouml;stermeden, herhangi bir bildirimde bulunmadan ve herhangi bir tazminat &ouml;deme y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml; bulunmadan ve derhal y&uuml;r&uuml;rl&uuml;ğe girecek şekilde işbu s&ouml;zleşmeyi tek taraflı olarak feshedebilir ve &Uuml;ye&rsquo;nin &uuml;yeliğine son verebilir veya s&ouml;zleşmeyi feshetmeden &Uuml;ye&rsquo;nin &uuml;yeliğini ge&ccedil;ici olarak durdurabilir. İşbu madde i&ccedil;erisinde belirtilen fesih, &uuml;yeliğe son verme ve &uuml;yeliği durdurma kararları tamamen işbu s&ouml;zleşmeye aykırılık, sitede belirtilen kurallara aykırılık, &Uuml;ye&rsquo;nin işlemlerinin veya işlemleri sonucunda ortaya &ccedil;ıkan durumun satıcı bilgi g&uuml;venliği sistemi uyarınca bir risk oluşturduğunun satıcı tarafından tespit edilmesi veya satıcının ticari kararları veya satıcı tarafından ortaya &ccedil;ıkan durumun hukuki bir risk oluşturduğunun değerlendirilmesi doğrultusunda satıcının kendi iradesi ile verilecektir.</p>

                    <p><strong>Hak ve Y&uuml;k&uuml;ml&uuml;l&uuml;kler</strong></p>

                    <p><strong>&Uuml;ye Hak ve Y&uuml;k&uuml;ml&uuml;l&uuml;kleri</strong></p>

                    <p>&Uuml;ye, &uuml;yelik prosed&uuml;rlerini yerine getirirken, Site&#39;nin Hizmet&#39;lerinden faydalanırken ve Site&#39;deki Hizmet&#39;lerle ilgili herhangi bir işlemi yerine getirirken, &Uuml;yelik S&ouml;zleşmesi&#39;nde yer alan t&uuml;m şartlara, Site&#39;nin ilgili yerlerinde belirtilen kurallara ve y&uuml;r&uuml;rl&uuml;kteki t&uuml;m mevzuata uygun hareket edeceğini, işbu s&ouml;zleşmede belirtilen t&uuml;m şart ve kuralları anladığını ve onayladığını kabul, beyan ve taahh&uuml;t eder.</p>

                    <p>&Uuml;ye, y&uuml;r&uuml;rl&uuml;kteki emredici mevzuat h&uuml;k&uuml;mleri gereğince veya diğer &uuml;yeler ile &uuml;&ccedil;&uuml;nc&uuml; şahısların haklarının ihlal edildiğinin iddia edilmesi durumlarında, satıcının kendisine ait gizli/&ouml;zel/ticari bilgileri gerek resmi makamlara ve gerekse hak sahibi kişilere a&ccedil;ıklamaya yetkili olacağını ve bu sebeple satıcıdan her ne nam altında olursa olsun tazminat talep edilemeyeceğini kabul, beyan ve taahh&uuml;t eder.</p>

                    <p>&Uuml;ye&rsquo;lerin satıcı tarafından sunulan Hizmet&#39;lerden yararlanabilmek amacıyla kullandıkları sisteme erişim ara&ccedil;larının (Kullanıcı ismi, şifre v.b.) g&uuml;venliği, saklanması, &uuml;&ccedil;&uuml;nc&uuml; kişilerin bilgisinden uzak tutulması ve kullanılması durumlarıyla ilgili hususlar tamamen &Uuml;ye&rsquo;lerin sorumluluğundadır. &Uuml;ye&rsquo;lerin, sisteme giriş ara&ccedil;larının g&uuml;venliği, saklanması, &uuml;&ccedil;&uuml;nc&uuml; kişilerin bilgisinden uzak tutulması, kullanılması gibi hususlardaki t&uuml;m ihmal ve kusurlarından dolayı &Uuml;ye&rsquo;lerin ve/veya &uuml;&ccedil;&uuml;nc&uuml; kişilerin uğradığı veya uğrayabileceği zararlara istinaden satıcının, doğrudan veya dolaylı, herhangi bir sorumluluğu yoktur.</p>

                    <p>&Uuml;ye&rsquo;ler, Site dahilinde kendileri tarafından sağlanan bilgi ve i&ccedil;eriklerin doğru ve hukuka uygun olduğunu kabul, beyan ve taahh&uuml;t ederler. Satıcı, &Uuml;ye&rsquo;ler tarafından satıcıya iletilen veya Site &uuml;zerinden kendileri tarafından y&uuml;klenen, değiştirilen veya sağlanan bilgi ve i&ccedil;eriklerin doğruluğunu araştırma, bu bilgi ve i&ccedil;eriklerin g&uuml;venli, doğru ve hukuka uygun olduğunu taahh&uuml;t ve garanti etmekle y&uuml;k&uuml;ml&uuml; ve sorumlu olmadığı gibi, s&ouml;z konusu bilgi ve i&ccedil;eriklerin yanlış veya hatalı olmasından dolayı ortaya &ccedil;ıkacak hi&ccedil;bir zarardan da sorumlu tutulamaz.</p>

                    <p>&Uuml;ye&rsquo;ler, satıcının yazılı onayı olmadan, &Uuml;yelik S&ouml;zleşmesi kapsamındaki hak ve y&uuml;k&uuml;ml&uuml;l&uuml;klerini, kısmen veya tamamen, herhangi bir &uuml;&ccedil;&uuml;nc&uuml; kişiye devredemezler.</p>

                    <p>Satıcının sunduğu Hizmet&#39;lerden yararlananlar ve Site&#39;yi kullananlar, yalnızca hukuka uygun ama&ccedil;larla Site &uuml;zerinde işlem yapabilirler. &Uuml;ye&rsquo;lerin, Site dahilinde yaptığı her işlem ve eylemdeki hukuki ve cezai sorumluluk kendilerine aittir. Her &Uuml;ye, satıcı ve/veya başka bir &uuml;&ccedil;&uuml;nc&uuml; şahsın aynı veya şahsi haklarına veya malvarlığına tecav&uuml;z teşkil edecek şekilde, Site dahilinde bulunan resimleri, metinleri, g&ouml;rsel ve işitsel imgeleri, video kliplerini, dosyaları, veritabanlarını, katalogları ve listeleri &ccedil;oğaltmayacağını, kopyalamayacağını, dağıtmayacağını, işlemeyeceğini, gerek bu eylemleri ile gerekse de başka yollarla satıcı ile doğrudan ve/veya dolaylı olarak rekabete girmeyeceğini kabul, beyan ve taahh&uuml;t eder. Satıcı &Uuml;ye&rsquo;lerin &Uuml;yelik S&ouml;zleşmesi h&uuml;k&uuml;mlerine ve/veya hukuka aykırı olarak Site &uuml;zerinde ger&ccedil;ekleştirdikleri faaliyetler nedeniyle &uuml;&ccedil;&uuml;nc&uuml; kişilerin uğradıkları veya uğrayabilecekleri zararlardan doğrudan ve/veya dolaylı olarak, hi&ccedil;bir şekilde sorumlu tutulamaz.</p>

                    <p>&Uuml;ye&rsquo;ler de dahil olmak &uuml;zere &uuml;&ccedil;&uuml;nc&uuml; kişiler tarafından Site&#39;de sağlanan hizmetlerden ve yayınlanan i&ccedil;eriklerden dolayı satıcının, satıcı &ccedil;alışanlarının veya y&ouml;neticilerinin sorumluluğu bulunmamaktadır. Herhangi bir &uuml;&ccedil;&uuml;nc&uuml; kişi tarafından sağlanan ve yayınlanan bilgilerin, i&ccedil;eriklerin, g&ouml;rsel ve işitsel imgelerin doğruluğu ve hukuka uygunluğunun taahh&uuml;d&uuml;, b&uuml;t&uuml;n&uuml;yle bu eylemleri ger&ccedil;ekleştiren kişilerin sorumluluğundadır. Satıcı, &Uuml;ye&rsquo;ler de dahil olmak &uuml;zere &uuml;&ccedil;&uuml;nc&uuml; kişiler tarafından sağlanan hizmetlerin ve i&ccedil;eriklerin g&uuml;venliğini, doğruluğunu ve hukuka uygunluğunu taahh&uuml;t ve garanti etmemektedir.</p>

                    <p>&Uuml;ye, satıcının Site &uuml;zerinden ger&ccedil;ekleştireceği her t&uuml;rl&uuml; kampanya ve &ccedil;ekiliş kapsamında, kampanyalara ve &ccedil;ekilişlere katılmaya hak kazanan &Uuml;ye&rsquo;lerin, &uuml;yelik bilgilerini kampanya ve &ccedil;ekiliş ile ilgili kişi ve kurumlarla paylaşacağını ve bu sebeple satıcıdan herhangi bir tazminat talebinde bulunmayacağını kabul, beyan ve taahh&uuml;t eder.</p>

                    <p>&Uuml;ye, Portal &uuml;zerinde ger&ccedil;ekleştirdiği işlemler ilgili olarak, reklam veren, &uuml;retici, vergi m&uuml;kellefi veya benzeri sıfatlarla ilgili mevzuatın gerektirdiği &ouml;nlem ve prosed&uuml;rleri yerine getirmek zorunda olduğunu, bu &ouml;nlem ve prosed&uuml;rlerle ilgili satıcının herhangi bir yetki ve sorumluluğu bulunmadığını, işbu madde i&ccedil;erisinde belirtilen kapsamda &ouml;nlem ve prosed&uuml;rlerin yerine getirilmemesi sonucunu doğuracak t&uuml;m ihmal ve kusurlarından dolayı diğer &Uuml;yelerin, satıcının ve/veya &uuml;&ccedil;&uuml;nc&uuml; kişilerin uğradığı veya uğrayabileceği zararlardan sorumlu olacağını kabul, beyan ve taahh&uuml;t eder.</p>

                    <p>&Uuml;ye, kayıt i&ccedil;in gerekli olan b&ouml;l&uuml;mleri doldurup elektronik posta adresini onayladıktan sonra işbu s&ouml;zleşmede belirtilen şartlara uymak koşuluyla, elektronik posta adresini ve şifresini girerek Siteyi kullanmaya başlayabilir.</p>

                    <p>&Uuml;ye, Site ve hizmetlerinden yararlanırken, T&uuml;rk Ceza Kanunu, T&uuml;rk Ticaret Kanunu, Fikir ve Sanat Eserleri Kanunu, Marka ve Patent Haklarının Korunması ile ilgili Kanun H&uuml;km&uuml;nde Kararnameler ve yasal d&uuml;zenlemeler, Bor&ccedil;lar Yasası, diğer ilgili mevzuat h&uuml;k&uuml;mleri ile Sitenin hizmetlerine ilişkin olarak yayımlayacağı her t&uuml;rl&uuml; duyuru ve bildirimlere uymayı kabul eder. Bu bildirimlere ve yasalara aykırı kullanım sebebiyle doğabilecek hukuki, cezai ve mali her t&uuml;rl&uuml; sorumluluk &uuml;yeye aittir.</p>

                    <p>Siteyi ziyaret eden &uuml;yelerin bilgileri (ziyaret sıklığı, ziyaret zamanları vb.) onlara daha iyi hizmet edebilmek amacı ile takip edilmektedir. Bu bilgiler, gizlilik şartlarına bağlı kalınarak, i&ccedil;eriği genişletmek ve iyileştirmek amacı ile reklam vb. konularda işbirliği yapılan firmalarla paylaşılmaktadır. Buradaki ama&ccedil;, sitenin kullanıcılarına sunduğu hizmeti geliştirmek ve Sitenin sunduğu i&ccedil;eriği zenginleştirmektir.</p>

                    <p><strong>Alıcı&#39;ların Hak ve Y&uuml;k&uuml;ml&uuml;l&uuml;kleri</strong></p>

                    <p>Alıcı, Site&#39;de sergilenen &uuml;r&uuml;n&uuml; satın almak &uuml;zere talepte bulunmak suretiyle, &uuml;r&uuml;n a&ccedil;ıklaması ile beraber satıcı tarafından belirlenmiş satış şartlarını ve usullerini de kabul etmiş sayılır.</p>

                    <p>Alıcı, Site&#39;de &Uuml;retici&rsquo;ler tarafından satışa arz edilen &uuml;r&uuml;nlerin defolu olup olmaması, Yasaklı &Uuml;r&uuml;nler&#39; den olup olmaması, ka&ccedil;ak olup olmaması, niteliği, orjinalliği, &uuml;r&uuml;n&uuml;n tanıtımında kullanılan yazılı ve/veya g&ouml;r&uuml;nt&uuml;l&uuml; a&ccedil;ıklamaların doğruluğu ve tamlığı da dahil ancak bunlarla sınırlı olmamak &uuml;zere, &uuml;r&uuml;n&uuml;n aslı ile ilgili hi&ccedil;bir konu hakkında satıcının bilgi sahibi olmadığı ve olması gerekmediğini ve bunları taahh&uuml;t ve garanti etmek y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml; bulunmadığını kabul ve beyan eder.</p>

                    <p>Alıcı, Site &uuml;zerinde ger&ccedil;ekleşmekte olan alım-satım s&uuml;recinin herhangi bir aşamasında ve her ne sebeple olursa olsun, &Uuml;retici&rsquo;nin &uuml;r&uuml;n&uuml; satmaktan ve teslim etmekten vazge&ccedil;mesi halinde, bu durumdan &ouml;t&uuml;r&uuml; satıcının hi&ccedil;bir sorumluluğu ve y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml; olmayacağını; satıcıdan, satın almaya &ccedil;alıştığı &uuml;r&uuml;n&uuml;n veya muadilinin, her ne suretle olursa olsun tedarikini veya buradan hareketle herhangi bir tazminat &ouml;denmesini talep edemeyeceğini kabul, beyan ve taahh&uuml;t eder.</p>

                    <p>Alıcı 6205 sayılı T&uuml;keticinin Korunması Hakkındaki Kanun ve Mesafeli S&ouml;zleşmeler Y&ouml;netmeliği gereğince satıcının &uuml;r&uuml;nlere ilişkin &ouml;n bilgilendirme formu ve mesafeli satış s&ouml;zleşmesini G&uuml;venli alışveriş sistemi Hizmeti i&ccedil;erisinde oluşturacağını kabul, beyan ve taahh&uuml;t eder.</p>

                    <p>Alıcı, Site&#39;de 6205 sayılı T&uuml;keticinin Korunması Hakkındaki Kanun ve Mesafeli S&ouml;zleşmeler Y&ouml;netmeliği &ccedil;er&ccedil;evesinde satıcının kendisine ait &uuml;yelik bilgilerini ve &Uuml;retici&rsquo;nin satışa konu &uuml;r&uuml;ne ilişkin verdiği bilgileri esas alarak d&uuml;zenlenen &ouml;n bilgilendirme formu ve mesafeli satış s&ouml;zleşmesini onaylayacağını kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><strong>Satıcının Hak ve Y&uuml;k&uuml;ml&uuml;l&uuml;kleri</strong></p>

                    <p>Satıcı Site&#39;de sunulan Hizmet&#39;leri ve i&ccedil;erikleri her zaman değiştirebilme; &Uuml;ye&#39;lerin sisteme y&uuml;kledikleri bilgileri ve i&ccedil;erikleri, &Uuml;ye&rsquo;ler de dahil olmak &uuml;zere, &uuml;&ccedil;&uuml;nc&uuml; kişilerin erişimine kapatabilme ve silme hakkını saklı tutmaktadır. Satıcı, bu hakkını, hi&ccedil;bir bildirimde bulunmadan ve &ouml;nel vermeden kullanabilir. &Uuml;ye&rsquo;ler, satıcının talep ettiği değişiklik ve/veya d&uuml;zeltmeleri ivedi olarak yerine getirmek zorundadırlar. Satıcı tarafından talep edilen değişiklik ve/veya d&uuml;zeltme istekleri, gerekli g&ouml;r&uuml;ld&uuml;ğ&uuml; takdirde, satıcı tarafından yapılabilir. Satıcı tarafından talep edilen değişiklik ve/veya d&uuml;zeltme taleplerinin &Uuml;ye&rsquo;ler tarafından zamanında yerine getirilmemesi sebebiyle doğan veya doğabilecek zararlar, hukuki ve cezai sorumluluklar tamamen &Uuml;ye&rsquo;lera aittir.</p>

                    <p>Satıcı Site&#39;de yer alan &uuml;yelik bilgilerini, Kullanıcı g&uuml;venliği, kendi y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;n&uuml; ifa ve www.petshopevinde.com ile ilgili pazarlama, tanıtım ve iletişim yapmak ve bazı istatistiki değerlendirmeler i&ccedil;in dilediği bi&ccedil;imde kullanabilir. Bunları bir veritabanı &uuml;zerinde tasnif edip muhafaza edebilir.</p>

                    <p>Satıcı, Site&#39;de sağlanan Hizmet&#39;ler kapsamında &Uuml;ye&rsquo;ler arasında ortaya &ccedil;ıkan uyuşmazlıklarda, arabulucu veya hakem sıfatlarıyla g&ouml;rev almaz.</p>

                    <p>Satıcı, Kullanıcılar arasında site &uuml;zerinden ger&ccedil;ekleşen ve Site&#39;nin işleyişine ve/veya &Uuml;yelik S&ouml;zleşmesine ve/veya Site&#39;nin genel kurallarına ve/veya genel ahlak kurallarına aykırı ve &ldquo;Satıcı&rdquo; veya &ldquo;X&rdquo; tarafından kabul edilmesi m&uuml;mk&uuml;n olmayan mesajların ve/veya i&ccedil;eriklerin tespit edilmesi amacıyla gerekli i&ccedil;erik ve/veya mesaj taraması yapabilir ve tespit ettiği mesaj ve/veya i&ccedil;erikleri istediği zaman ve şekilde erişimden kaldırabilir; Satıcı, bu mesaj ve/veya i&ccedil;eriği oluşturan &Uuml;ye&rsquo;yi yazılı uyarabilir ve/veya &Uuml;ye&rsquo;nin &uuml;yeliğine, herhangi bir ihbar yapmadan, ge&ccedil;ici veya kalıcı olarak, son verebilir.</p>

                    <p>&Uuml;ye&rsquo;ler ve satıcı hukuken bağımsız taraflardır. Aralarında ortaklık, temsilcilik veya iş&ccedil;i-işveren ilişkisi yoktur. &Uuml;yelik S&ouml;zleşmesi&#39;nin onaylanması ve uygulanması sonucunda, ortaklık, temsilcilik veya iş&ccedil;i-işveren ilişkisi doğmaz.</p>

                    <p>&Uuml;ye&rsquo;lerın, Site&#39;ye &uuml;ye olurken sisteme y&uuml;kledikleri &quot;kullanıcı isimleri&quot; işbu &Uuml;yelik S&ouml;zleşmesi i&ccedil;inde yer alan h&uuml;k&uuml;mlere tabi olup, &Uuml;ye&rsquo;lerin &quot;kullanıcı ismi&quot; belirlerken &uuml;&ccedil;&uuml;nc&uuml; şahısların telif hakkı, marka, ticari unvan gibi yasal haklarını ihlal etmemesi gerekmektedir. &Uuml;ye&rsquo;lerın işbu madde h&uuml;km&uuml;ne aykırı davranması durumunda, satıcı &Uuml;yelik S&ouml;zleşmesi&#39;ne aykırı bu durumun d&uuml;zeltilmesini &Uuml;ye&rsquo;den talep edebileceği gibi dilerse &Uuml;ye&rsquo;ye &ouml;nceden haber vermeksizin &Uuml;ye&rsquo;nin &uuml;yeliğini ge&ccedil;ici veya kalıcı olarak iptal edebilir.</p>

                    <p>Satıcı, Site &uuml;zerinden yapılan satışlara ilişkin &ouml;n bilgilendirme formu ve mesafeli satış s&ouml;zleşmelerini 3 yıl s&uuml;re ile saklayacağını ve bu s&uuml;re i&ccedil;erisinde işbu mesafeli satış s&ouml;zleşmesini ve &ouml;n bilgilendirme formunu Alıcı ve &Uuml;retici&rsquo;nin talebi ile paylaşacağını kabul, beyan ve taahh&uuml;t eder. satıcı m&uuml;şterilerine ait kredi kartı bilgilerini sistemde kayıtlı olarak tutmamaktadır.</p>

                    <p>Satıcı tarafından www.petshopevinde.com internet sitesinin iyileştirilmesi, geliştirilmesine y&ouml;nelik olarak ve/veya yasal mevzuat &ccedil;er&ccedil;evesinde siteye erişmek i&ccedil;in kullanılan İnternet servis sağlayıcısının adı ve Internet Protokol (IP) adresi, siteye erişilen tarih ve saat, sitede bulunulan sırada erişilen sayfalar ve siteye doğrudan bağlanılmasını sağlayan Web sitesinin Internet adresi gibi birtakım bilgiler toplanabilir.</p>

                    <p>Satıcı &uuml;yelerine daha iyi hizmet sunmak, &uuml;r&uuml;nlerini ve hizmetlerini iyileştirmek, sitenin kullanımını kolaylaştırmak i&ccedil;in kullanımını &Uuml;ye&rsquo;lerinin &ouml;zel tercihlerine ve ilgi alanlarına y&ouml;nelik &ccedil;alışmalarda &uuml;yelerin kişisel bilgilerini kullanabilir. Satıcı, &uuml;yenin www.petshopevinde.com internet sitesi &uuml;zerinde yaptığı hareketlerin kaydını bulundurma hakkını saklı tutar.</p>

                    <p>Satıcı, &uuml;ye, kullanıcı veya alıcının www.petshopevinde.com internet sitesi kurallarını yada iş bu s&ouml;zleşmede &ouml;ng&ouml;r&uuml;len herhangi bir y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml; veya herhangi kanun, y&ouml;netmelik ve sair mevzuata aykırı herhangi bir eylem ve işlem nedeniyle herhangi bir ihbar ve ihtarda bulunmaksızın derhal kullanım hakkını sınırlandırabilir, durdurabilir veya &uuml;yeliği iptal edebilir. Bu hallerde &uuml;ye, kullanıcı veya alıcı, satıcıdan herhangi bir menfi veya m&uuml;spet zarar ve ziyan, hak ve alacak talep ve tahsil edemez.</p>

                    <p><strong>Fikri M&uuml;lkiyet Hakları</strong></p>

                    <p>Site&#39;nin (tasarım, metin, imge, html kodu ve diğer kodlar da dahil ve fakat bunlarla sınırlı olmamak kaydıyla) t&uuml;m elemanları (Satıcı telif haklarına tabi &ccedil;alışmalar) satıcıya ait olarak ve/veya satıcı tarafından &uuml;&ccedil;&uuml;nc&uuml; bir kişiden alınan lisans hakkı altında kullanılmaktadır. &Uuml;ye&rsquo;ler, satıcı Hizmet&#39;lerini, satıcıbilgilerini ve satıcının telif haklarına tabi &ccedil;alışmalarını yeniden satamaz, paylaşamaz, dağıtamaz, sergileyemez, &ccedil;oğaltamaz, bunlardan t&uuml;remiş &ccedil;alışmalar yapamaz veya hazırlayamaz, veya başkasının satıcının Hizmet&#39;lerine erişmesine veya kullanmasına izin veremez; aksi takdirde, lisans verenler de dahil ancak bunlarla sınırlı olmaksızın, &uuml;&ccedil;&uuml;nc&uuml; kişilerin uğradıkları zararlardan dolayı satıcıdan talep edilen tazminat miktarını ve mahkeme masrafları ve avukatlık &uuml;creti de dahil ancak bununla sınırlı olmamak &uuml;zere diğer her t&uuml;rl&uuml; y&uuml;k&uuml;ml&uuml;l&uuml;kleri karşılamakla sorumlu olacaklardır.</p>

                    <p>Satıcının satıcı Hizmet&#39;leri, satıcı bilgileri, satıcı telif haklarına tabi &ccedil;alışmalar, satıcı ticari markaları, satıcı ticari g&ouml;r&uuml;n&uuml;m&uuml; veya Site vasıtasıyla sahip olduğu her t&uuml;r maddi ve fikri m&uuml;lkiyet hakları da dahil t&uuml;m malvarlığı, aynı ve şahsi hakları, ticari bilgi ve know-how&#39;a y&ouml;nelik t&uuml;m hakları saklıdır.</p>

                    <p><strong>S&ouml;zleşme Değişiklikleri</strong></p>

                    <p>İşbu s&ouml;zleşme &uuml;yenin &uuml;yeliğini iptal etmesi veya satıcı tarafından &uuml;yeliğinin iptal edilmesine kadar y&uuml;r&uuml;rl&uuml;kte kalacaktır. Satıcı &uuml;yenin &uuml;yelik s&ouml;zleşmesinin herhangi bir h&uuml;km&uuml;n&uuml; ihlal etmesi durumunda &uuml;yenin &uuml;yeliğini iptal ederek s&ouml;zleşmeyi tek taraflı olarak feshedebilecektir.</p>

                    <p><strong>Sorumluluğun Sınırlandırılması</strong></p>

                    <p>www.petshopevinde.com, Site&#39;ye erişilmesi, Site&#39;nin ya da Site&#39;deki bilgilerin ve diğer verilerin programların vs. kullanılması sebebiyle, s&ouml;zleşmenin ihlali, haksız fiil, ya da başkaca sebeplere binaen, doğabilecek doğrudan ya da dolaylı hi&ccedil;bir zarardan sorumlu değildir. www.petshopevinde.com s&ouml;zleşmenin ihlali, haksız fiil, ihmal veya diğer sebepler neticesinde; işlemin kesintiye uğraması, hata, ihmal, kesinti hususunda herhangi bir sorumluluk kabul etmez. Bu Site&rsquo;ye ya da link verilen diğer web sitelerine erişilmesi ya da Site&#39;nin kullanılması ile www.petshopevinde.com işletmesinin, kullanım/ziyaret sonucunda, doğabilecek her t&uuml;r sorumluluktan, mahkeme ve diğer masraflar da dahil olmak &uuml;zere her t&uuml;r zarar ve talep hakkından ayrı kılındığı kabul edilmektedir.</p>

                    <p><strong>Devir</strong></p>

                    <p>www.petshopevinde.com bu s&ouml;zleşmeyi bildirimsiz olarak istediği zaman kısmen veya b&uuml;t&uuml;n&uuml;yle devredebilir. Ancak Kullanıcı ve &Uuml;ye bu s&ouml;zleşmeyi veya herhangi bir kısmını başka bir tarafa devredemez. Bu t&uuml;rden bir devir girişimi ge&ccedil;ersizdir.</p>

                    <p><strong>M&uuml;cbir Sebepler</strong></p>

                    <p>Hukuken m&uuml;cbir sebep sayılan t&uuml;m durumlarda, işbu Kullanım Koşulları ve &Uuml;yelik S&ouml;zleşmesini ge&ccedil; ifa etmekten veya ifa etmemekten dolayı y&uuml;k&uuml;ml&uuml; değildir. Bu ve bunun gibi durumlar, www.petshopevinde.com a&ccedil;ısından, gecikme veya ifa etmeme veya temerr&uuml;t addedilmeyecek veya bu durumlar i&ccedil;in www.petshopevinde.com işletmesinin herhangi bir tazminat y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml; doğmayacaktır.</p>

                    <p><strong>Uygulanacak Hukuk ve Yetki</strong></p>

                    <p>İşbu s&ouml;zleşmenin uygulanmasında, Sanayi ve Ticaret Bakanlığınca ilan edilen değere kadar T&uuml;ketici Hakem Heyetleri ile satıcının yerleşim yerindeki T&Uuml;KETICI MAHKEMELERI yetkilidir. Siparişin elektronik ortamda onaylanması durumunda, Alıcı iş bu s&ouml;zleşmenin t&uuml;m h&uuml;k&uuml;mlerini kabul etmiş sayılır. Taraflar arasında ve/veya 3.şahısların şikayetleri ile ilgili herhangi bir uyuşmazlık olduğunda satıcının kayıt ve belgeleri, mail yazışmaları, internet sitesi &uuml;zerinde yer alan sistem kayıtları(bilgisayar-ses kayıtları gibi manyetik ortamdaki kayıtlar dahil) m&uuml;nhasıran HMK 193. madde anlamında kesin delil teşkil edecektir.</p>

                    <p><strong>Y&uuml;r&uuml;rl&uuml;k</strong></p>

                    <p>&Uuml;yenin, &uuml;yelik kaydı yapması &uuml;yenin &uuml;yelik s&ouml;zleşmesinde yer alan t&uuml;m maddeleri okuduğu ve &uuml;yelik s&ouml;zleşmesinde yer alan maddeleri kabul ettiği anlamına gelir. İşbu S&ouml;zleşme &uuml;yenin &uuml;ye olması anında akdedilmiş ve karşılıklı olarak y&uuml;r&uuml;rl&uuml;l&uuml;ğe girmiştir. Satıcı dilediği zaman işbu s&ouml;zleşme h&uuml;k&uuml;mlerinde değişikliğe gidebilir, değişiklikler, s&uuml;r&uuml;m numarası ve değişiklik tarihi belirtilerek site &uuml;zerinden yayınlanır ve aynı tarihte y&uuml;r&uuml;rl&uuml;ğe girer.</p>

                    <p>OKUDUM-KABUL ETTİM ve ONAYLADIM</p>

                    <p>&Uuml;ye</p>

				</div>

			</div>
		</div>
	</div>

	<!-- The UserCommunicationPermit Modal -->
	<div class="modal" id="communication-permit-modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Gizlilik Politikası</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">

					<h6><b>GİZLİLİK</b></h6>
					<p>Alıcı tarafından iş bu sözleşmede belirtilen bilgiler ile ödeme yapmak amacı ile satıcıya bildirdiği bilgiler satıcı tarafından 3. şahıslarla paylaşılmayacaktır.</p>
					<p>Satıcı bu bilgileri sadece idari/ yasal zorunluluğun mevcudiyeti çerçevesinde açıklayabilecektir. Araştırma ehliyeti belgelenmiş her türlü adli soruşturma dahilinde satıcı kendisinden istenen bilgiyi elinde bulunduruyorsa ilgili makama sağlayabilir.</p>
					<p>Kredi Kartı bilgileri kesinlikle saklanmaz, kredi kartı bilgileri sadece tahsilat işlemi sırasında ilgili bankalara güvenli bir şekilde iletilerek provizyon alınması için kullanılır ve provizyon sonrası sistemden silinir.</p>
					<p>Alıcıya ait e-posta adresi, posta adresi ve telefon gibi bilgiler yalnızca satıcı tarafından standart ürün teslim ve bilgilendirme prosedürleri için kullanılır. Bazı dönemlerde kampanya bilgileri, yeni ürünler hakkında bilgiler, promosyon bilgileri alıcıya onayı sonrasında gönderilebilir.</p>

					<b>Güvenlik</b><br />
					<b>Size ait bilgiler koruma altında, gönül rahatlığıyla alışverişin keyfini çıkartın...</b><br />
					<p><a href="<?=base_url()?>" target="_blank">www.petshopevinde.com</a>’dan alışveriş yapan müşterilerimizin kredi kartı, adres, telefon…vs  bilgileri SSL güvenlik sistemi tarafından korunmakta olup üçüncü kişilerle paylaşılmaz ve ele geçirmeleri olanaksızdır.</p>
					<b>Güvenli Alışveriş Nedir?</b><br />
					<p>Güvenli Alışveriş satıcıyı, malın teslimi karşılığında bedeli tahsil edememe riskine karşı; alıcıyı da bedelin ödenmesine karşılık, malı teslim alamama veya istenilen nitelik ve şartlarda teslim alamama riskine karşı korumaktadır.</p>
					<p>Güvenli Alışveriş, elektronik ortamda yapılan alışverişlerde hem alıcıyı hem de satıcıyı koruyan bir uygulamadır. Güvenli Alışveriş sayesinde alıcıya satın aldığı ürünü görerek onaylama imkânı, satıcıya ise ödeme güvencesi sunulmaktadır.</p>
					<b>SSL Nedir?</b><br />
					<p>SSL, Secure Sockets Layer'in baş harflerinden oluşan bir kısaltmadır. Kullandığınız tarayıcı ile bağlı olduğunuz sitenin sunucusu arasında güvenlikli bilgi alış-verişini sağlayan bir yöntemdir. Herhangi bir web sitesinde dolaştığınız ve bilgi girişi yaptığınız zaman, üçüncü şahıslar tarafından izlenme olasılığı vardır. Kişisel bilgilerinize, kredi kartı numaranıza, teknolojik açıdan son derece gelişkin olanaklara sahip diğer kullanıcılar tarafından ulaşılması ve bunların isteğiniz dışında kullanılması olanaksız değildir.</p>
					<p>SSL güvenlik sistemi bu tür istenmeyen durumları önlemek için geliştirilmiş bir şifreleme yöntemidir. SSL güvenlik alanında gönderdiğiniz bilgiler çok sıradan görünen, fakat çözülmesi gerçekte imkansız olan bir kodlama sistemiyle şifrelenir.</p>

				</div>

			</div>
		</div>
	</div>

	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

	<script type="text/javascript">
		
		$(document).ready(function(){
			
			$('#send-btn').click(function(){
				$('#preloader').fadeIn();
			});

			$('#country-id').change(function(){
	            var country_id = $(this).val();
	            city_list(country_id);
	        });

	        $('#city-id').change(function(){
	            var city_id = $(this).val();
	            district_list(city_id);
	        });

	        var country_status = '<?=set_value('AddressCountryID') == 65 ? 1 : 0?>';
	        if (country_status == 1)
	        {
	        	city_list(65);
	        }

		});

		function city_list(country_id, selected_id = '')
	    {
	        if (country_id == 0 || country_id == '') {
	        	country_id = 0;
	            $('.city-area, .district-area').css('display', 'block');
	            $('.alternative-city-area').css('display', 'none');
	            $('#alternative-city').val('');
	        }else{
	            $('.city-area, .district-area').css('display', 'none');
	            $('.alternative-city-area').css('display', 'block');
	            if (selected_id == '')
	            {
	                $('#alternative-city').val('');
	            }
	        }

	        $.ajax({
	            url : base_url + 'ajax/cities/' + country_id,
	            type: "GET",
	            dataType: "JSON",
	            success: function(data)
	            {
	                $('#city-id').html('<option value="">Seçiniz...</option>');
	                $('#district-id').html('<option value="">Seçiniz...</option>');

	                if (data != "" && typeof data.status === 'undefined') 
	                {
	                    $('.city-area, .district-area').css('display', 'block');
	                    $('.alternative-city-area').css('display', 'none');
	                    $('#alternative-city').val('');

	                    $.each(data, function(count, item) {
	                        let selected = item.CityID == selected_id ? 'selected' : '';
	                        $('#city-id').append('<option value="'+item.CityID+'" '+selected+'>'+item.CityName+'</option>');
	                    });
	                }
	            },
	            error: function (jqXHR, textStatus, errorThrown)
	            {
	                alert('Hata oluştu');
	            }
	        });
	    }

	    function district_list(city_id, selected_id = '')
	    {
	        $.ajax({
	            url : base_url + 'ajax/districts/' + city_id,
	            type: "GET",
	            dataType: "JSON",
	            success: function(data)
	            {   
	                $('#district-id').html('<option value="">Seçiniz...</option>');

	                if (data != "" && typeof data.status === 'undefined') 
	                {
	                    $.each(data, function(count, item) {
	                        let selected = item.DistrictID == selected_id ? 'selected' : '';
	                        $('#district-id').append('<option value="'+item.DistrictID+'" '+selected+'>'+item.DistrictName+'</option>');
	                    });
	                }
	            },
	            error: function (jqXHR, textStatus, errorThrown)
	            {
	                alert('Hata oluştu');
	            }
	        });
	    }

	</script>

</body>
	
</html>