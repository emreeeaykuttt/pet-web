<!DOCTYPE html>
<html lang="tr">
    
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Siparişiniz Gerçekleşmedi">
    <meta name="keywords" content="Siparişiniz Gerçekleşmedi">

    <title>Siparişiniz Gerçekleşmedi</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
    
    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page shopping-cart">

        <div class="cart-result">
            <img src="<?=base_url()?>assets/frontend/img/cart-icon-fail.svg">
            <div class="container message">
                <?php
                    if ($this->input->get('error') == 'address') 
                    {
                        $error_text = 'Siparişiniz Gerçekleşmedi, Lütfen Adres Ekleyiniz.';
                        $link = 'order/delivery';
                        $link_text = 'Adres Seçme Sayfasına Geri Dön';
                    }
                    elseif ($this->input->get('error') == 'payment') 
                    {
                        $error_text = 'Siparişiniz Gerçekleşmedi, Lütfen Ödeme Yöntemi Seçiniz.';
                        $link = 'order/delivery';
                        $link_text = 'Geri Dön';
                    }
                    elseif ($this->input->get('error') == 'contract') 
                    {
                        $error_text = 'Siparişiniz Gerçekleşmedi, Lütfen "Mesafeli Satış Sözleşmesi"ni Onaylayınız.';
                        $link = 'order/delivery';
                        $link_text = 'Geri Dön';
                    }
                    else
                    {
                        $error_text = 'Siparişiniz Gerçekleşmedi. Siparişlerim sayfasından siparişiniz hangi durumda kaldığını kontrol ediniz.';
                        $link = 'profile/orders';
                        $link_text = 'Siparişlerim';
                    }
                ?>
                <div class="info mb-5"><?=$error_text?></div>
                <a href="<?=base_url() . $link?>" class="btn btn-black p-3"><?=$link_text?></a>
            </div>
        </div>

    </div>
  
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>


</body>
    
</html>