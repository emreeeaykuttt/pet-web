<!DOCTYPE html>
<html lang="tr">
    
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="petshopevinde.com">
    <meta name="keywords" content="petshopevinde.com">

    <title>Sipariş Başarılı Bir Şekilde Gerçekleşti</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
    
    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page shopping-cart">

        <div class="cart-result">
            <img src="<?=base_url()?>assets/frontend/img/cart-icon-success.svg">
            <div class="container message">
                <div class="order-id">Sipariş Numarası: <?=$order['OrderTrackingNumber']?></div>

                <?php if ($this->input->get('payment') == 'transfer'){ ?>

                    <div class="info">
                        Teşekkür ederiz, siparişiniz başarıyla oluşturuldu.<br /> 
                        Lütfen aşağıdaki hesap numarasına ödeme yaparak siparişinizi tamamlayınız.<br /> 
                        Açıklama bölümüne SİPARİŞ NUMARANIZI yazmayı unutmayınız. 
                    </div>

                    <b>ÖDENECEK TUTAR: <?=$order['OrderLatestAmount'] . $order['OrderCurrencyCode']?></b><br />
                    Garanti Bankası<br />
                    Hesap: 287 - 6297377 GÖLCÜK<br />
                    IBAN: TR12 0006 2000 2870 0006 2973 77<br />
                    <small>(Eğer ödeme yaptıysanız bekleyiniz. Site yöneticisi siparişinizi onaylandığında siparişiniz işleme alınacaktır.)</small><br /><br />
                    <a href="<?=base_url()?>profile/orders" class="btn btn-black p-3">Siparişlerim</a>

                <?php } else{ ?>

                    <div class="info">
                        Teşekkür ederiz, siparişiniz başarıyla oluşturuldu.<br />
                        Ödeme yaparak siparişinizi tamamlayınız.
                    </div>

                    <?php print_r($iyzico->getCheckoutFormContent()); ?>
                    <div id="iyzipay-checkout-form" class="popup"></div>

                    <a href="<?=base_url()?>order/success/<?=$order['OrderID']?>?payment=card" id="payment-card" class="btn btn-success p-3 mt-2 mb-5 w-50">
                        ÖDEME İÇİN TIKLAYINIZ
                    </a>

                <?php } ?>

            </div>
        </div>

    </div>
  
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

</body>
    
</html>