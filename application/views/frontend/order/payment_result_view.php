<!DOCTYPE html>
<html lang="tr">
	
<head>
	<meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="petshopevinde.com">
    <meta name="keywords" content="petshopevinde.com">

    <title>Ödeme Başarılı Bir Şekilde Gerçekleşti</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
	
	<?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page shopping-cart">

        <div class="cart-result">
            <img src="<?=base_url()?>assets/frontend/img/cart-icon-<?=$tracking_number ? 'success' : 'fail'?>.svg">
            <div class="container message">

                <?php if ($tracking_number){ ?>
                    <div class="order-id">Sipariş Numarası: <?=$tracking_number?></div>
                    <div class="info">Ödemeniz başarılı bir şekilde gerçekleşti, Teşekkür Ederiz.</div>
                <?php }else{ ?>
                    <div class="info">
                        Ödeme yapılırken bir hata oluştu.<br />
                        Tekrar denemek için Siparişler sayfasında siparişinizi kontrol edebilir ve yeniden ödemeyi deneyebilirsiniz.
                    </div>
                <?php } ?>

                <a href="<?=base_url()?>profile/orders" class="btn btn-black p-3 mt-2">Siparişlerim</a>

            </div>
        </div>

    </div>
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

</body>
	
</html>