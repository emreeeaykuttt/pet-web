<!DOCTYPE html>
<html lang="tr">
	
<head>
	<meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="petshopevinde">
    <meta name="keywords" content="petshopevinde">

    <title>Sipariş Adres Seçimi</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
	
	<?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <?php  
        $guest_status = $this->session->flashdata('guest_status');
        $flashdata = null;
        if (isset($guest_status)) {
            $flashdata = $guest_status;
        }
    ?>

    <div class="sub-page shopping-cart">
        <div class="container">

            <?php if ($flashdata): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert special-alert <?=$flashdata['status'] ? 'alert-success' : 'alert-danger'?> mb-2">
                       <span><?=$flashdata['message']?></span>
                    </div>
                </div> 
            </div>
            <?php endif ?>

            <div class="cart-navigation">
                <div class="status cart ">
                    <span>Sepet</span>
                </div>
                <div class="status adress active">
                    <span>Adres</span>
                </div>
                <div class="status payment">
                    <span>Ödeme</span>
                </div>
            </div>

            <div class="cart-adress-detail">

                <form action="<?=base_url()?>order/payment" method="POST">
                
                    <div class="cart-adress-titles">
                        <div class="row">
                            <!-- d-none d-sm-block -->
                            <div class="col-md-2 col-2">Başlık</div>
                            <div class="col-md-5 col-5">Adres</div>
                            <div class="col-md-2 col-3 text-center">Teslimat Adresi</div>
                            <div class="col-md-2 col-2 text-center">Fatura Adresi</div>
                        </div>
                    </div>

                    <div class="cart-adresses">

                        <?php if ($address): ?>
                        <?php foreach ($address as $key => $value): ?>
                        <div class="adress-item">
                            <div class="row p-3">
                                <div class="col-md-2 col-2 d-flex">
                                    <span class="align-self-center"><?=$value['AddressTitle']?></span>
                                </div>
                                <div class="col-md-5 col-5 d-flex">
                                    <span class="align-self-center">
                                        <?php 
                                            $city_and_district = !empty($value['CityName']) ? $value['DistrictName'] . '/' . $value['CityName'] : $value['AddressAlternativeCity'];
                                        ?>
                                        <?=$value['AddressOpenAddress'] . ' ' . $city_and_district?>
                                    </span>
                                </div>
                                <div class="col-md-2 col-3 d-flex justify-content-center">
                                    <div class="align-self-center">
                                        <div class="custom-control custom-radio">
                                            <input 
                                                type="radio" 
                                                class="custom-control-input" 
                                                id="delivery<?=$value['AddressID']?>" 
                                                name="Delivery" 
                                                value="<?=$value['AddressID']?>"
                                                <?=$key == 0 ? 'checked' : ''?>
                                            />
                                            <label class="custom-control-label" for="delivery<?=$value['AddressID']?>"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-2 d-flex justify-content-center">
                                    <div class="align-self-center">
                                        <div class="custom-control custom-radio">
                                            <input 
                                                type="radio" 
                                                class="custom-control-input" 
                                                id="billing<?=$value['AddressID']?>" 
                                                name="Billing" 
                                                value="<?=$value['AddressID']?>"
                                                <?=$key == 0 ? 'checked' : ''?>
                                            />
                                            <label class="custom-control-label" for="billing<?=$value['AddressID']?>"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                        <?php endif ?>

                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <a href="javascript:void(0)" onclick="address_add()" class="btn btn-light w-100 p-3 mt-3">
                                Yeni Ekle <i class="las la-plus"></i>
                            </a>
                        </div>

                        <div class="col-md-6">

                            <?php
                                $currency = $this->session->userdata('UserCurrency'); 
                                $currency_icon = $this->session->userdata('UserCurrencyCode'); 
                                $basket_total_price = 0;

                                if ($products_in_basket) 
                                {
                                    foreach ($products_in_basket as $key => $value)
                                    {
                                        $basket_total_price += ($value['BasketProductQuantity'] *$value['ProductOption'.$currency.'LatestPrice']);
                                    }
                                }
                            ?>

                            <div class="container cart-summary">
                                <div class="row">
                                    <div class="col-md-8 col-8">Ürünler Toplamı (KDV Dahil)</div>
                                    <div class="col-md-4 col-4">
                                        <span id="basket-total-price">
                                            <?=number_format_render($basket_total_price)?>
                                        </span>
                                        <span class="currency"><?=$currency_icon?></span>
                                    </div>
                                </div>
                                
                                <?php if (!empty($coupon)): ?>

                                <input type="hidden" name="CouponCode" value="<?=$this->input->get('coupon')?>">

                                <div class="row">
                                    <div class="col-md-8 col-8">Kupon İndirimi</div>
                                    <div class="col-md-4 col-4">
                                    <?php
                                        if (!empty($coupon['CouponAmount'])) 
                                        {
                                            echo $coupon['CouponAmount'] . '<span class="currency">' . $currency_icon . '</span>';
                                            $basket_total_price = $basket_total_price - $coupon['CouponAmount'];
                                        }
                                        else
                                        {
                                            echo $coupon['CouponRate'] . ' <span class="currency">%</span>';
                                            $discount = ($basket_total_price / 100) * $coupon['CouponRate'];
                                            $basket_total_price = $basket_total_price - $discount;
                                        }
                                    ?>
                                    </div>
                                </div>
                                <?php endif ?>

                                <div class="row">
                                    <div class="col-md-8 col-8">Kargo</div>
                                    <div class="col-md-4 col-4">
                                        <?=$basket_total_price > 0 && $basket_total_price < 140 ? '9.89' : '0.00'?> 
                                        <span class="currency"><?=$currency_icon?></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img src="<?=base_url()?>assets/frontend/img/address-icon.svg" width="30" class="m-2" />
                                        Tahmini Teslimat Tarihi: <?=date('d.m.Y', strtotime('+ 7 days'))?>
                                    </div>
                                </div>
                                <div class="total">
                                    <div class="row">
                                        <div class="col-md-8 col-6">Toplam</div>
                                        <div class="col-md-4 col-6">
                                            <?php 
                                                if ($basket_total_price > 0 && $basket_total_price < 140) {
                                                    $cargo_price = 9.89;
                                                } else {
                                                    $cargo_price = 0;
                                                }
                                            ?>
                                            <?=number_format_render($basket_total_price + $cargo_price)?>
                                            <span class="currency"><?=$currency_icon?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-1 total">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <select name="PaymentType" class="form-control">
                                                <option value="0">Ödeme Yöntemi seçiniz</option>
                                                <option value="card">Kredi Kartı / Banka Kartı ile ödeme</option>
                                                <option value="transfer">EFT/havale ile ödeme</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-1 delivery-contract">
                                    <div class="col-md-12">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="DistanceSalesContract" value="1" /> 
                                            <a href="javascript:void(0)" onclick="distance_sales_contract_modal()">
                                                Mesafeli Satış Sözleşmesini kabul ediyorum
                                            </a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-3">
                                <div class="row">
                                    <div class="col-md-8"></div>
                                    <div class="col-md-4">
                                        <button class="btn btn-black p-3 w-100 d-none" id="payment-btn">Ödemeye Geç</button>
                                        <a href="javascript:void(0)" onclick="address_view()" class="btn btn-black p-3 w-100">Ödemeye Geç</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </form>

            </div>

        </div>
    </div>

    <!-- The UserDistanceSalesContract Modal -->
    <div class="modal" id="distance-sales-contract-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Mesafeli Satış Sözleşmesi</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    
                    <p><b>1-</b> Bu s&ouml;zleşme ile yazılı, g&ouml;rsel ve elektronik ortamda veya diğer iletişim ara&ccedil;ları kullanılarak ve t&uuml;keticilerle karşı karşıya gelinmeksizin yapılan, malın veye hizmetin t&uuml;keticiye anında veya sonradan teslimi veya ifası şeklinde yapılan &quot;T&uuml;ketici&quot; (Bundan sonra M&uuml;şteri olarak anılacaktır.) ve &quot;Satıcı&quot; arasındaki alışverişe ilişkin koşullar d&uuml;zenlenir. &nbsp;</p>

                    <p><b>2-</b> <b>SATICI</b>,&nbsp; Doa Satış Pazarlama ve Ltd. Şti. &#39; dir (Bundan sonra &quot;<b>PETSHOPEVİNDE</b>&quot; olarak anılacaktır. .</p>

                    <p>Mersis Numarası: 030251828100011</p>

                    <p>Adres:&nbsp; Yenidoğan Mah. Halit Cansever Sok. No:32/1 İzmit Kocaeli</p>

                    <p>E posta adresi: <a href="mailto:info@petshopevinde.com">info@petshopevinde.com</a></p>

                    <p>Tel no: 0262 331 73 34</p>

                    <p><b>3- ALICI</b> , <span class="delivery-fullname"></span> &#39;dir. (Bundan sonra&nbsp; &quot;<b>ALICI</b>&quot; olarak anılacaktır.)</p>

                    <p>TC No : <span class="delivery-identity-number"></span></p>

                    <p>Adres: <span class="delivery-address"></span></p>

                    <p>E posta adresi : <span class="delivery-email"></span></p>

                    <p>Teslim edilecek kişi: <span class="delivery-fullname"></span></p>

                    <p>Teslimat Adresi: <span class="delivery-address"></span><br />
                    Telefon: <span class="delivery-phone"></span><br />
                    Eposta/kullanıcı adı: <span class="delivery-email"></span></p>

                    <p><b>4- SİPARİŞ VEREN KİŞİ BİLGİLERİ</b></p>

                    <p>Ad/Soyad/Unvan: <span class="delivery-fullname"></span></p>

                    <p>Adres: <span class="delivery-address"></span><br />
                    Telefon: <span class="delivery-phone"></span><br />
                    Eposta/kullanıcı adı: <span class="delivery-email"></span></p>

                    <p><b>5- S&Ouml;ZLEŞME KONUSU &Uuml;R&Uuml;N/&Uuml;R&Uuml;NLER BİLGİLERİ</b></p>

                    <p><b>5.1. </b>Malın /&Uuml;r&uuml;n/&Uuml;r&uuml;nlerin/ Hizmetin temel &ouml;zelliklerini (t&uuml;r&uuml;, miktarı, marka/modeli, rengi, adedi) PETSHOPEVİNDE&rsquo;ye ait internet sitesinde yayınlanmaktadır. PETSHOPEVİNDE tarafından kampanya d&uuml;zenlenmiş ise ilgili &uuml;r&uuml;n&uuml;n temel &ouml;zelliklerini kampanya s&uuml;resince inceleyebilirsiniz. Kampanyaya dahil edilmiş &uuml;r&uuml;nler kampanya tarihi dışında kampanyayla aynı koşullarda satışa sunulamamaktadır.</p>

                    <p><b>5.2. </b>Listelenen ve sitede ilan edilen fiyatlar satış fiyatıdır. İlan edilen fiyatlar ve vaatler g&uuml;ncelleme yapılana ve değiştirilene kadar ge&ccedil;erlidir. S&uuml;reli olarak ilan edilen fiyatlar ise belirtilen s&uuml;re sonuna kadar ge&ccedil;erlidir.</p>

                    <p><b>5.3. </b>S&ouml;zleşme konusu mal ya da hizmetin t&uuml;m vergiler d&acirc;hil satış fiyatı aşağıda g&ouml;sterilmiştir.</p>

                    <?php
                        $currency = $this->session->userdata('UserCurrency'); 
                        $currency_icon = $this->session->userdata('UserCurrencyCode'); 
                        $basket_total_price = 0;
                    ?>
                    <?php if ($products_in_basket): ?>
                    <table class="table table-bordered">
                        <tr>
                            <td><b>Ürün Açıklaması</b></td>
                            <td><b>Adet</b></td>
                            <td><b>Birim Fiyatı</b></td>
                            <td><b>Ara Toplam (KDV Dahil)</b></td>
                        </tr>
                        <?php foreach ($products_in_basket as $key => $value): ?>
                        <?php $basket_total_price += ($value['BasketProductQuantity'] *$value['ProductOption'.$currency.'LatestPrice']); ?>
                        <tr>
                            <td><?=$value['ProductName']?></td>
                            <td><?=$value['BasketProductQuantity']?></td>
                            <td><?=number_format_render($value['ProductOption'.$currency.'Price'])?><?=$currency_icon?></td>
                            <td><?=number_format_render(($value['BasketProductQuantity'] * $value['ProductOption'.$currency.'LatestPrice']))?><?=$currency_icon?></td>
                        </tr>
                        <?php endforeach ?>

                        <?php
                            $coupon_content = '';
                            if (!empty($coupon)){
                                if (!empty($coupon['CouponAmount'])) 
                                {
                                    $coupon_content = $coupon['CouponAmount'] . '<span class="currency">' . $currency_icon . '</span>';
                                    $basket_total_price = $basket_total_price - $coupon['CouponAmount'];
                                }
                                else
                                {
                                    $coupon_content = $coupon['CouponRate'] . ' <span class="currency">%</span>';
                                    $discount = ($basket_total_price / 100) * $coupon['CouponRate'];
                                    $basket_total_price = $basket_total_price - $discount;
                                }
                            }
                        ?>

                        <?php if (!empty($coupon)): ?>
                        <tr>
                            <td colspan="3"><b>Kupon İndirimi:</b></td>
                            <td colspan="1">
                                <?=$coupon_content?>
                            </td>
                        </tr>    
                        <?php endif ?>
                        
                        <tr>
                            <td colspan="3"><b>Kargo Tutarı:</b></td>
                            <td colspan="1">
                                <?=$basket_total_price > 0 && $basket_total_price < 140 ? '9.89' : '0.00'?><?=$currency_icon?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"><b>Toplam:</b></td>
                            <td colspan="1">
                                <?=$basket_total_price > 0 && $basket_total_price < 140 ? number_format_render($basket_total_price + 9.89) : number_format_render($basket_total_price)?><?=$currency_icon?>
                            </td>
                        </tr>
                    </table>
                    <?php endif ?>

                    <p>&Ouml;deme Şekli ve Planı: <span class="payment-type"></span></p>

                    <p><b>5.4. </b>&Uuml;r&uuml;n sevkiyat masrafı olan kargo &uuml;creti ALICI tarafından &ouml;denecektir.</p>

                    <p><b>6-&nbsp; FATURA BİLGİLERİ</b></p>

                    <p>Ad/Soyad/Unvan: <span class="billing-fullname"></span></p>

                    <p>Adres: <span class="billing-address"></span><br />
                    Telefon: <span class="billing-phone"></span><br />
                    Eposta/kullanıcı adı: <span class="billing-email"></span><br />
                    Fatura teslim: Fatura sipariş teslimatı sırasında fatura adresine sipariş ile birlikte<br />
                    teslim edilecektir.</p>

                    <p><b>7- GENEL H&Uuml;K&Uuml;MLER</b></p>

                    <p><b>7.1.</b>ALICI, PETSHOPEVİNDE&lsquo;ye ait internet sitesinde s&ouml;zleşme konusu &uuml;r&uuml;n&uuml;n temel nitelikleri, satış fiyatı ve &ouml;deme şekli ile teslimata ilişkin &ouml;n bilgileri okuyup, bilgi sahibi olduğunu, elektronik ortamda gerekli teyidi verdiğini kabul, beyan ve taahh&uuml;t eder. ALICI&rsquo;nın; &Ouml;n Bilgilendirmeyi elektronik ortamda teyit etmesi, mesafeli satış s&ouml;zleşmesinin kurulmasından evvel, PETSHOPEVİNDE tarafından ALICI&#39; ya verilmesi gereken adresi, siparişi verilen &uuml;r&uuml;nlere ait temel &ouml;zellikleri, &uuml;r&uuml;nlerin vergiler dahil fiyatını, &ouml;deme ve teslimat bilgilerini de doğru ve eksiksiz olarak edindiğini kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.2.</b>S&ouml;zleşme konusu her bir &uuml;r&uuml;n, 30 g&uuml;nl&uuml;k yasal s&uuml;reyi aşmamak kaydı ile ALICI&#39; nın yerleşim yeri uzaklığına bağlı olarak internet sitesindeki &ouml;n bilgiler kısmında belirtilen s&uuml;re zarfında ALICI veya ALICI&rsquo;nın g&ouml;sterdiği adresteki kişi ve/veya kuruluşa teslim edilir. Bu s&uuml;re i&ccedil;inde &uuml;r&uuml;n&uuml;n ALICI&rsquo;ya teslim edilememesi durumunda, ALICI&rsquo;nın s&ouml;zleşmeyi feshetme hakkı saklıdır.</p>

                    <p><b>7.3.</b>PETSHOPEVİNDE, S&ouml;zleşme konusu &uuml;r&uuml;n&uuml; eksiksiz, siparişte belirtilen niteliklere uygun ve varsa garanti belgeleri, kullanım kılavuzları işin gereği olan bilgi ve belgeler ile teslim etmeyi, her t&uuml;rl&uuml; ayıptan ari olarak yasal mevzuat gereklerine g&ouml;re sağlam, standartlara uygun bir şekilde işi doğruluk ve d&uuml;r&uuml;stl&uuml;k esasları dahilinde ifa etmeyi, hizmet kalitesini koruyup y&uuml;kseltmeyi, işin ifası sırasında gerekli dikkat ve &ouml;zeni g&ouml;stermeyi, ihtiyat ve &ouml;ng&ouml;r&uuml; ile hareket etmeyi kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.4.</b>PETSHOPEVİNDE, s&ouml;zleşmeden doğan ifa y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;n&uuml;n s&uuml;resi dolmadan ALICI&rsquo;yı bilgilendirmek ve a&ccedil;ık&ccedil;a onayını almak suretiyle eşit kalite ve fiyatta farklı bir &uuml;r&uuml;n tedarik edebilir.</p>

                    <p><b>7.5.</b>PETSHOPEVİNDE, sipariş konusu &uuml;r&uuml;n veya hizmetin yerine getirilmesinin imkansızlaşması halinde s&ouml;zleşme konusu y&uuml;k&uuml;ml&uuml;l&uuml;klerini yerine getiremezse, bu durumu, &ouml;ğrendiği tarihten itibaren 3 g&uuml;n i&ccedil;inde yazılı olarak t&uuml;keticiye bildireceğini, 14 g&uuml;nl&uuml;k s&uuml;re i&ccedil;inde toplam bedeli ALICI&rsquo;ya iade edeceğini kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.6.</b>ALICI, S&ouml;zleşme konusu &uuml;r&uuml;n&uuml;n teslimatı i&ccedil;in işbu S&ouml;zleşme&rsquo;yi elektronik ortamda teyit edeceğini, herhangi bir nedenle s&ouml;zleşme konusu &uuml;r&uuml;n bedelinin &ouml;denmemesi ve/veya banka kayıtlarında iptal edilmesi halinde, PETSHOPEVİNDE&lsquo; s&ouml;zleşme konusu &uuml;r&uuml;n&uuml; teslim y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;n&uuml;n sona ereceğini kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.7.</b>SATICI, tarafların iradesi dışında gelişen, &ouml;nceden &ouml;ng&ouml;r&uuml;lemeyen ve tarafların bor&ccedil;larını yerine getirmesini engelleyici ve/veya geciktirici hallerin oluşması gibi m&uuml;cbir sebepler nedeni ile s&ouml;zleşme konusu &uuml;r&uuml;n&uuml; s&uuml;resi i&ccedil;inde teslim edemez ise, durumu ALICI&#39;ya bildireceğini kabul, beyan ve taahh&uuml;t eder. ALICI da siparişin iptal edilmesini, s&ouml;zleşme konusu &uuml;r&uuml;n&uuml;n varsa emsali ile değiştirilmesini ve/veya teslimat s&uuml;resinin engelleyici durumun ortadan kalkmasına kadar ertelenmesini SATICI&rsquo;dan talep etme hakkını haizdir. ALICI tarafından siparişin iptal edilmesi halinde ALICI&rsquo;nın nakit ile yaptığı &ouml;demelerde, &uuml;r&uuml;n tutarı 14 g&uuml;n i&ccedil;inde kendisine nakden ve defaten &ouml;denir. ALICI&rsquo;nın kredi kartı ile yaptığı &ouml;demelerde ise, &uuml;r&uuml;n tutarı, siparişin ALICI tarafından iptal edilmesinden sonra 14 g&uuml;n i&ccedil;erisinde ilgili bankaya iade edilir. ALICI, SATICI tarafından kredi kartına iade edilen tutarın banka tarafından ALICI hesabına yansıtılmasına ilişkin ortalama s&uuml;recin 2 ile 3 haftayı bulabileceğini, bu tutarın bankaya iadesinden sonra ALICI&rsquo;nın hesaplarına yansıması halinin tamamen banka işlem s&uuml;reci ile ilgili olduğundan, ALICI, olası gecikmeler i&ccedil;in SATICI&rsquo;yı sorumlu tutamayacağını kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.8.</b>SATICININ, ALICI tarafından siteye kayıt (&uuml;yelik) formunda belirtilen veya daha sonra kendisi tarafından g&uuml;ncellenen adresi, e-posta adresi, sabit ve mobil telefon hatları ve diğer iletişim bilgileri &uuml;zerinden mektup, e-posta, SMS, telefon g&ouml;r&uuml;şmesi ve diğer yollarla iletişim, pazarlama, bildirim ve diğer ama&ccedil;larla ALICI&rsquo;ya ulaşma hakkı bulunmaktadır. ALICI, işbu s&ouml;zleşmeyi kabul etmekle SATICI&rsquo;nın kendisine y&ouml;nelik yukarıda belirtilen iletişim faaliyetlerinde bulunabileceğini kabul ve beyan etmektedir.</p>

                    <p><b>7.9.</b>ALICI, s&ouml;zleşme konusu mal/hizmeti teslim almadan &ouml;nce muayene edecek; ezik, kırık, ambalajı yırtılmış vb. hasarlı ve ayıplı mal/hizmeti kargo şirketinden teslim almayacaktır. ALICI tarafından teslim alınan mal/hizmetin hasarsız ve sağlam olduğu kabul edilecektir. Teslimden sonra mal/hizmetin &ouml;zenle korunması borcu, ALICI&rsquo;ya aittir. Cayma hakkı kullanılacaksa mal/hizmet kullanılmamalıdır. Fatura iade edilmelidir.</p>

                    <p><b>7.10.</b>ALICI ile sipariş esnasında kullanılan kredi kartı hamilinin aynı kişi olmaması veya &uuml;r&uuml;n&uuml;n ALICI&rsquo;ya tesliminden evvel, siparişte kullanılan kredi kartına ilişkin g&uuml;venlik a&ccedil;ığı tespit edilmesi halinde, SATICI, kredi kartı hamiline ilişkin kimlik ve iletişim bilgilerini, siparişte kullanılan kredi kartının bir &ouml;nceki aya ait ekstresini yahut kart hamilinin bankasından kredi kartının kendisine ait olduğuna ilişkin yazıyı ibraz etmesini ALICI&rsquo;dan talep edebilir. ALICI&rsquo;nın talebe konu bilgi/belgeleri temin etmesine kadar ge&ccedil;ecek s&uuml;rede sipariş dondurulacak olup, mezkur taleplerin 24 saat i&ccedil;erisinde karşılanmaması halinde ise SATICI, siparişi iptal etme hakkını haizdir.</p>

                    <p><b>7.11.</b>ALICI, SATICI&rsquo;ya ait internet sitesine &uuml;ye olurken verdiği kişisel ve diğer sair bilgilerin ger&ccedil;eğe uygun olduğunu, SATICI&rsquo;nın bu bilgilerin ger&ccedil;eğe aykırılığı nedeniyle uğrayacağı t&uuml;m zararları, SATICI&rsquo;nın ilk bildirimi &uuml;zerine derhal, nakden ve defaten tazmin edeceğini beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.12.</b>ALICI, SATICI&rsquo;ya ait internet sitesini kullanırken yasal mevzuat h&uuml;k&uuml;mlerine riayet etmeyi ve bunları ihlal etmemeyi baştan kabul ve taahh&uuml;t eder. Aksi takdirde, doğacak t&uuml;m hukuki ve cezai y&uuml;k&uuml;ml&uuml;l&uuml;kler tamamen ve m&uuml;nhasıran ALICI&rsquo;yı bağlayacaktır.</p>

                    <p><b>7.13.</b>ALICI, SATICI&rsquo;ya ait internet sitesini hi&ccedil;bir şekilde kamu d&uuml;zenini bozucu, genel ahlaka aykırı, başkalarını rahatsız ve taciz edici şekilde, yasalara aykırı bir ama&ccedil; i&ccedil;in, başkalarının maddi ve manevi haklarına tecav&uuml;z edecek şekilde kullanamaz. Ayrıca, &uuml;ye başkalarının hizmetleri kullanmasını &ouml;nleyici veya zorlaştırıcı faaliyet (spam, virus, truva atı, vb.) işlemlerde bulunamaz.</p>

                    <p><b>7.14.</b>SATICI&rsquo;ya ait internet sitesinin &uuml;zerinden, SATICI&rsquo;nın kendi kontrol&uuml;nde olmayan ve/veya başkaca &uuml;&ccedil;&uuml;nc&uuml; kişilerin sahip olduğu ve/veya işlettiği başka web sitelerine ve/veya başka i&ccedil;eriklere link verilmiş olabilir. Bu linkler ALICI&rsquo;ya y&ouml;nlenme kolaylığı sağlamak amacıyla konmuş olup herhangi bir web sitesini veya o siteyi işleten kişiyi desteklememekte ve Link verilen web sitesinin i&ccedil;erdiği bilgilere y&ouml;nelik herhangi bir garanti niteliği taşımamaktadır.</p>

                    <p><b>7.15.</b>İşbu s&ouml;zleşme i&ccedil;erisinde sayılan maddelerden bir ya da birka&ccedil;ını ihlal eden &uuml;ye işbu ihlal nedeniyle cezai ve hukuki olarak şahsen sorumlu olup, SATICI&rsquo;yı bu ihlallerin hukuki ve cezai sonu&ccedil;larından ari tutacaktır. Ayrıca; işbu ihlal nedeniyle, olayın hukuk alanına intikal ettirilmesi halinde, SATICI&rsquo;nın &uuml;yeye karşı &uuml;yelik s&ouml;zleşmesine uyulmamasından dolayı tazminat talebinde bulunma hakkı saklıdır.</p>

                    <p><b>8. CAYMA HAKKI</b></p>

                    <p><b>8.1.</b>ALICI; mesafeli s&ouml;zleşmenin mal satışına ilişkin olması durumunda, &uuml;r&uuml;n&uuml;n kendisine veya g&ouml;sterdiği adresteki kişi/kuruluşa teslim tarihinden itibaren 14 (ond&ouml;rt) g&uuml;n i&ccedil;erisinde, SATICI&rsquo;ya bildirmek şartıyla hi&ccedil;bir hukuki ve cezai sorumluluk &uuml;stlenmeksizin ve hi&ccedil;bir gerek&ccedil;e g&ouml;stermeksizin malı reddederek s&ouml;zleşmeden cayma hakkını kullanabilir. Hizmet sunumuna ilişkin mesafeli s&ouml;zleşmelerde ise, bu s&uuml;re s&ouml;zleşmenin imzalandığı tarihten itibaren başlar. Cayma hakkı s&uuml;resi sona ermeden &ouml;nce, t&uuml;keticinin onayı ile hizmetin ifasına başlanan hizmet s&ouml;zleşmelerinde cayma hakkı kullanılamaz. Cayma hakkının kullanımından kaynaklanan masraflar SATICI&rsquo; ya aittir. ALICI, iş bu s&ouml;zleşmeyi kabul etmekle, cayma hakkı konusunda bilgilendirildiğini kabul eder.</p>

                    <p><b>8.2.</b>Cayma hakkının kullanılması i&ccedil;in 14 (ond&ouml;rt) g&uuml;nl&uuml;k s&uuml;re i&ccedil;inde SATICI&#39; ya iadeli taahh&uuml;tl&uuml; posta, faks veya eposta ile yazılı bildirimde bulunulması ve &uuml;r&uuml;n&uuml;n işbu s&ouml;zleşmede d&uuml;zenlenen &quot;Cayma Hakkı Kullanılamayacak &Uuml;r&uuml;nler&quot; h&uuml;k&uuml;mleri &ccedil;er&ccedil;evesinde kullanılmamış olması şarttır. Bu hakkın kullanılması halinde,</p>

                    <p><b>a)</b>3. kişiye veya ALICI&rsquo; ya teslim edilen &uuml;r&uuml;n&uuml;n faturası, (İade edilmek istenen &uuml;r&uuml;n&uuml;n faturası kurumsal ise, iade ederken kurumun d&uuml;zenlemiş olduğu iade faturası ile birlikte g&ouml;nderilmesi gerekmektedir. Faturası kurumlar adına d&uuml;zenlenen sipariş iadeleri İADE FATURASI kesilmediği takdirde tamamlanamayacaktır.)</p>

                    <p><b>b)</b>İade formu,</p>

                    <p><b>c)</b>İade edilecek &uuml;r&uuml;nlerin kutusu, ambalajı, varsa standart aksesuarları ile birlikte eksiksiz ve hasarsız olarak teslim edilmesi gerekmektedir.</p>

                    <p><b>d)</b>SATICI, cayma bildiriminin kendisine ulaşmasından itibaren en ge&ccedil; 10 g&uuml;nl&uuml;k s&uuml;re i&ccedil;erisinde toplam bedeli ve ALICI&rsquo;yı bor&ccedil; altına sokan belgeleri ALICI&rsquo; ya iade etmekle ve ALICI 20 g&uuml;nl&uuml;k s&uuml;re i&ccedil;erisinde malı satıcıya teslim etmekle y&uuml;k&uuml;ml&uuml;d&uuml;r.</p>

                    <p><b>e)</b>ALICI&rsquo; nın kusurundan kaynaklanan bir nedenle malın değerinde bir azalma olursa veya iade imkansızlaşırsa ALICI kusuru oranında SATICI&rsquo; nın zararlarını tazmin etmekle y&uuml;k&uuml;ml&uuml;d&uuml;r. Ancak cayma hakkı s&uuml;resi i&ccedil;inde malın veya &uuml;r&uuml;n&uuml;n usul&uuml;ne uygun kullanılması sebebiyle meydana gelen değişiklik ve bozulmalardan ALICI sorumlu değildir.</p>

                    <p><b>f)</b>Cayma hakkının kullanılması nedeniyle SATICI tarafından d&uuml;zenlenen kampanya limit tutarının altına d&uuml;ş&uuml;lmesi halinde kampanya kapsamında faydalanılan indirim miktarı iptal edilir.</p>

                    <p><b>9. CAYMA HAKKI KULLANILAMAYACAK &Uuml;R&Uuml;NLER</b></p>

                    <p>ALICI&rsquo;nın isteği veya a&ccedil;ık&ccedil;a kişisel ihtiya&ccedil;ları doğrultusunda hazırlanan ve geri g&ouml;nderilmeye m&uuml;sait olmayan &uuml;r&uuml;n ve/veya malzemeleri, tek kullanımlık &uuml;r&uuml;nler, &ccedil;abuk bozulma tehlikesi olan veya son kullanma tarihi ge&ccedil;me ihtimali olan mallar, ALICI&rsquo;ya teslim edilmesinin ardından ALICI tarafından ambalajı a&ccedil;ıldığı takdirde iade edilmesi sağlık ve hijyen a&ccedil;ısından uygun olmayan &uuml;r&uuml;nler, teslim edildikten sonra başka &uuml;r&uuml;nlerle karışan ve doğası gereği ayrıştırılması m&uuml;mk&uuml;n olmayan &uuml;r&uuml;nler, elektronik ortamda anında ifa edilen hizmetler veya t&uuml;keticiye anında teslim edilen gayrimaddi mallar ile ses veya g&ouml;r&uuml;nt&uuml; kayıtlarının, kitap, dijital i&ccedil;erik, yazılım programlarının, veri kaydedebilme ve veri depolama cihazlarının ambalajının ALICI tarafından a&ccedil;ılmış olması halinde iadesi Y&ouml;netmelik gereği m&uuml;mk&uuml;n değildir.</p>

                    <p>a) Hijyenik &uuml;r&uuml;nlerin hi&ccedil;bir sebeple iadesi yoktur.(ambalajı a&ccedil;ılmış kedi yada k&ouml;peğinizin bedenine temas etmiş olması)<br />
                    b) Kedi tuvalet &uuml;r&uuml;nleri.<br />
                    c) Tırnak makası, fır&ccedil;a, tarak gibi kolay kontaminasyona uygun &uuml;r&uuml;nler( Ambalajı a&ccedil;ılmasa dahi!)<br />
                    d) Tek kullanımlık &uuml;r&uuml;nler.<br />
                    e) G&ouml;z, kulak damlası gibi ağızı a&ccedil;ıldıktan sonra kullanım s&uuml;resi olan &uuml;r&uuml;nler.<br />
                    f) K&uuml;lotlar ve pedler.</p>

                    <p>Kozmetik ve pet bakım &uuml;r&uuml;nleri, giyim &uuml;r&uuml;nleri, kitap, kopyalanabilir yazılım ve programlar, DVD, VCD, CD ve kasetlerin iade edilebilmesi i&ccedil;in ambalajlarının a&ccedil;ılmamış, denenmemiş, bozulmamış ve kullanılmamış olmaları gerekir.</p>

                    <p><b>10. TEMERR&Uuml;T HALİ VE HUKUKİ SONU&Ccedil;LARI</b></p>

                    <p>ALICI, &ouml;deme işlemlerini kredi kartı ile yaptığı durumda temerr&uuml;de d&uuml;şt&uuml;ğ&uuml; takdirde, kart sahibi banka ile arasındaki kredi kartı s&ouml;zleşmesi &ccedil;er&ccedil;evesinde faiz &ouml;deyeceğini ve bankaya karşı sorumlu olacağını kabul, beyan ve taahh&uuml;t eder. Bu durumda ilgili banka hukuki yollara başvurabilir; doğacak masrafları ve vekalet &uuml;cretini ALICI&rsquo;dan talep edebilir ve her koşulda ALICI&rsquo;nın borcundan dolayı temerr&uuml;de d&uuml;şmesi halinde, ALICI, borcun gecikmeli ifasından dolayı SATICI&rsquo;nın uğradığı zarar ve ziyanını &ouml;deyeceğini kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>11. YETKİLİ MAHKEME</b></p>

                    <p><b>İşbu s&ouml;zleşmeden doğan uyuşmazlıklarda şikayet ve itirazlar </b>ilgili yasalarda belirtilen parasal sınırlar dahilinde t&uuml;ketici sorunları hakem heyetine veya t&uuml;ketici mahkemesine yapılabilir.<br />
                    İşbu S&ouml;zleşme ticari ama&ccedil;larla yapılmaktadır. İşbu s&ouml;zleşme &ccedil;er&ccedil;evesinde meydana gelebilecek uyuşmazlıklarda kanunlarda &ouml;zel bir d&uuml;zenleme yoksa Kocaeli Mahkemeleri ve İcra Daireleri yetkilidir.</p>

                    <p><b>12. Y&Uuml;R&Uuml;RL&Uuml;K</b></p>

                    <p>ALICI, Site &uuml;zerinden verdiği siparişe ait &ouml;demeyi ger&ccedil;ekleştirdiğinde işbu s&ouml;zleşmenin t&uuml;m şartlarını kabul etmiş sayılır.&nbsp;</p>

                </div>

            </div>
        </div>
    </div>

    <!-- The AddressView Modal -->
    <div class="modal" id="address-view-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Teslimat Adres - Fatura Adresi</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="delivery-area mb-2">
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="billing-area mb-2">
                                
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h6 class="d-flex justify-content-center mt-4 text-danger">Teslimat ve Fatura Adresini Kontrol Ettiyseniz</h6>
                            <a href="javascript:void(0)" onclick="payment_go()" id="payment-send" class="btn btn-black p-3 w-100">Ödemeye Geçiniz</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/address_transactions.php'); ?>

    <script type="text/javascript">

        function distance_sales_contract_modal()
        {
            let delivery = $('[name="Delivery"]:checked').val();
            let billing = $('[name="Billing"]:checked').val();
            let payment_type = $('[name="PaymentType"]').val();

            if (payment_type == 'card') {
                $('.payment-type').text('Kredi Kartı / Banka Kartı ile ödeme');
            } else if (payment_type == 'transfer') {
                $('.payment-type').text('EFT/havale ile ödeme');
            } else {
                $('.payment-type').text('');
            }

            $.ajax({
                url : base_url + 'ajax/view_address/' + delivery,
                type: 'GET',
                dataType: 'JSON',
                success: function(data)
                {
                    $('.delivery-fullname').text(data.AddressFirstName + ' ' + data.AddressLastName);
                    $('.delivery-identity-number').text(data.AddressIdentityNumber);
                    $('.delivery-address').text(data.AddressOpenAddress + ' ' + data.DistrictName + '/' + data.CityName);
                    $('.delivery-email').text(data.AddressEmail);
                    $('.delivery-phone').text(data.AddressPhone);
                }
            });

            $.ajax({
                url : base_url + 'ajax/view_address/' + billing,
                type: 'GET',
                dataType: 'JSON',
                success: function(data)
                {
                    $('.billing-fullname').text(data.AddressFirstName + ' ' + data.AddressLastName);
                    $('.billing-identity-number').text(data.AddressIdentityNumber);
                    $('.billing-address').text(data.AddressOpenAddress + ' ' + data.DistrictName + '/' + data.CityName);
                    $('.billing-email').text(data.AddressEmail);
                    $('.billing-phone').text(data.AddressPhone);

                   $('#distance-sales-contract-modal').modal('show');
                }
            });
        }

        function address_view()
        {
            let delivery = $('[name="Delivery"]:checked').val();
            let billing = $('[name="Billing"]:checked').val();

            $('.delivery-area, .billing-area').html('');

            $.ajax({
                url : base_url + 'ajax/view_address/' + delivery,
                type: 'GET',
                dataType: 'JSON',
                success: function(data)
                {
                    $('.delivery-area').append(
                        '<div class="view-item text-danger"><b>Teslimat Adresi</b></div>' +
                        '<div class="view-item"><b>Ad Soyad: </b>' + data.AddressFirstName + ' ' + data.AddressLastName + '</div>' +
                        '<div class="view-item"><b>TC Kimlik No: </b>' + data.AddressIdentityNumber + '</div>' +
                        '<div class="view-item"><b>Posta Kodu: </b>' + data.AddressZipCode + '</div>' +
                        '<div class="view-item"><b>Email: </b>' + data.AddressEmail + '</div>' +
                        '<div class="view-item"><b>Telefon: </b>' + data.AddressPhone + '</div>' +
                        '<div class="view-item"><b>Ülke: </b>' + data.CountryName + '</div>' +
                        '<div class="view-item"><b>Şehir: </b>' + data.CityName + '</div>' +
                        '<div class="view-item"><b>İlçe: </b>' + data.DistrictName + '</div>' +
                        '<div class="view-item"><b>Açık Adres: </b>' + data.AddressOpenAddress + '</div>'
                    );
                }
            });

            $.ajax({
                url : base_url + 'ajax/view_address/' + billing,
                type: 'GET',
                dataType: 'JSON',
                success: function(data)
                {
                    let billing_area = '';

                    if (data.AddressBillType == 'individual')
                    {
                        billing_area = '<div class="view-item"><b>Fatura Tipi: </b>Bireysel</div>';
                    }
                    else if (data.AddressBillType == 'corporate')
                    {
                        billing_area = 
                            '<div class="view-item"><b>Fatura Tipi: </b>Kurumsal</div>' +
                            '<div class="view-item"><b>Firma Adı: </b>' + data.AddressCompanyName + '</div>' +
                            '<div class="view-item"><b>Vergi Numarası: </b>' + data.AddressCompanyTaxNumber + '</div>' +
                            '<div class="view-item"><b>Vergi Dairesi: </b>' + data.AddressCompanyTaxAdministration + '</div>';
                    }

                    $('.billing-area').append(
                        '<div class="view-item text-danger"><b>Fatura Adresi</b></div>' +
                        '<div class="view-item"><b>Ad Soyad: </b>' + data.AddressFirstName + ' ' + data.AddressLastName + '</div>' +
                        '<div class="view-item"><b>TC Kimlik No: </b>' + data.AddressIdentityNumber + '</div>' +
                        '<div class="view-item"><b>Posta Kodu: </b>' + data.AddressZipCode + '</div>' +
                        '<div class="view-item"><b>Email: </b>' + data.AddressEmail + '</div>' +
                        '<div class="view-item"><b>Telefon: </b>' + data.AddressPhone + '</div>' +
                        '<div class="view-item"><b>Ülke: </b>' + data.CountryName + '</div>' +
                        '<div class="view-item"><b>Şehir: </b>' + data.CityName + '</div>' +
                        '<div class="view-item"><b>İlçe: </b>' + data.DistrictName + '</div>' +
                        '<div class="view-item"><b>Açık Adres: </b>' + data.AddressOpenAddress + '</div>' +
                        billing_area
                    );

                    $('#address-view-modal').modal('show');
                }
            });

        }

        function payment_go()
        {
            $('#payment-send').text('Bekleyiniz...');
            $('#payment-send').attr('disabled',true);
            $('#payment-btn').click();
        }

    </script>

</body>
	
</html>