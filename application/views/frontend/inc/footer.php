<!-- Social media Instagram start -->
<div class="container">
    <div class="instagram">
        <div class="title"><span>BİZİ TAKİP EDİN FIRSATLARI KAÇIRMAYIN</span></div>
        <div id="instagram-feed">
        </div>
    </div>
</div>
<!-- Social media Instagram end -->


<!-- Footer start -->

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="footer-title">KURUMSAL</div>
                <div class="footer-menu">
                    <ul>
                        <li><a href="<?= base_url() ?>hakkimizda">Hakkımızda</a></li>
                        <li><a href="<?= base_url() ?>gizlilik-politikasi">Gizlilik Politikası</a></li>
                        <li><a href="<?= base_url() ?>kullanici-sozlesmesi">Kullanıcı Sözleşmesi</a></li>
                        <li><a href="<?= base_url() ?>mesafeli-satis-sozlesmesi">Mesafeli Satış Sözleşmesi</a></li>
                        <li><a href="<?= base_url() ?>iptal-ve-iade-sartlari">İptal ve İade Şartları</a></li>
                        <li><a href="<?= base_url() ?>kargo-ve-teslimat-bilgileri">Kargo ve Teslimat Bilgileri</a></li>
                        <!-- <li><a href="<?= base_url() ?>iletisim">İletişim</a> -->
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-title">MARKALAR</div>
                <div class="footer-menu">
                    <ul>
                        <li><a href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"49"}]'>Royal Canin</a></li>
                        <li><a href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"44"}]'>Pro Plan</a></li>
                        <li><a href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"32"}]'>Hills</a></li>
                        <li><a href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"119"}]'>Doggie</a></li>
                        <li><a href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"132"}]'>Ever Clean</a></li>
                        <li><a href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"125"}]'>Purule</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 text-md-right">
                <div class="footer-title">İLETİŞİM</div>
                <div class="footer-menu">
                    <ul>
                        <li><i class="la la-phone"></i> 0262 331 73 34</li>
                        <li>
                            <div><i class="la la-envelope-open"></i> <a href="#">info@petshopevinde.com</a></div>
                        </li>
                        <li></li>
                        <li>
                            <div class="social-media">
                                <a href="https://www.instagram.com/petshopevinde/" rel="noopener" target="_blank"><i class="lab la-instagram"></i></a>
                            </div>
                        </li>
                        <li>Yenidoğan Mah. Halit Cansever Sok. No: 32  <br>
                        İzmit / Kocaeli </li>
                        <li>
                            <div class="iyzico mt-2 mb-2">
                                <img width="150" height="20" alt="İyzico"src="<?= base_url() ?>assets/frontend/img/visa-iyzico.png" width="150" />
                            </div>
                        </li>
                        <li class="mb-2">
                            <?php include(dirname(__DIR__) . '/inc/etbis.php'); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="cookie-fixed">
    <div class="cookie-text">
        Sitemizden en iyi şekilde faydalanabilmeniz için çerezler kullanılmaktadır. Bu siteye giriş yaparak çerez kullanımını kabul etmiş sayılıyorsunuz. <a href="<?= base_url() ?>cerez-politikasi">Daha fazla bilgi</a>
    </div>
    <div class="cookie-close"><i class="la la-times"></i></div>
</div>

<div class="whatsapp-area animated fadeInUp delay-1s">

    <?php
    $iphone = stripos($_SERVER['HTTP_USER_AGENT'], "iphone");
    $android = stripos($_SERVER['HTTP_USER_AGENT'], "android");
    if ($iphone) {
        $link = 'whatsapp://send?text=Merhaba, Hemen bizimle iletişime geçebilirsiniz.&phone=902623317334';
    } elseif ($android) {
        $link = 'https://api.whatsapp.com/send?phone=902623317334&text=Merhaba, Hemen bizimle iletişime geçebilirsiniz.';
    } else {
        $link = 'https://web.whatsapp.com/send?phone=902623317334&text=Merhaba, Hemen bizimle iletişime geçebilirsiniz.';
    }
    ?>
    <a href="<?= $link ?>" target="_blank" rel=noopener class="w-icon">
        <i class="lab la-whatsapp"></i>
    </a>

</div>

<div class="overlay">

</div>