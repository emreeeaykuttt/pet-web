<!-- modal start -->
<div class="modal" id="form-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">Adres Formu</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body form-area">
                <form action="#" id="form" class="row">

                    <input type="hidden" value="" name="AddressID"/> 

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="input-label">Adres Tipi</label>
                            <select class="form-control" id="address-type" name="AddressType">
                                <option value="0">Seçiniz...</option>
                                <option value="delivery">Teslimat Adresi</option>
                                <option value="billing">Fatura Adresi</option>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="input-label">Adres Adı</label>
                            <input name="AddressTitle" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="input-label">Adınız</label>
                            <input name="AddressFirstName" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="input-label">Soyadınız</label>
                            <input name="AddressLastName" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="input-label">Ülke</label>
                            <select class="form-control" id="country-id" name="AddressCountryID">
                                <option value="0">Seçiniz...</option>
                                <?php foreach ($countries as $key => $country): ?>
                                    <option value="<?=$country['CountryContentID']?>"><?=$country['CountryName']?></option>
                                <?php endforeach ?>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4 city-area">
                        <div class="form-group">
                            <label class="input-label">Şehir</label>
                            <select class="form-control" id="city-id" name="AddressCityID">
                                <option value="0">Seçiniz...</option>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4 district-area">
                        <div class="form-group">
                            <label class="input-label">İlçe</label>
                            <select class="form-control" id="district-id" name="AddressDistrictID">
                                <option value="0">Seçiniz...</option>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-8 alternative-city-area">
                        <div class="form-group">
                            <label class="input-label">Şehir</label>
                            <input name="AddressAlternativeCity" id="alternative-city" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="input-label">Açık Adres</label>
                            <textarea name="AddressOpenAddress" placeholder="Mahalle, sokak, cadde ve diğer bilgilerinizi giriniz" cols="3" class="form-control"></textarea>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="input-label">Email</label>
                            <input name="AddressEmail" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="input-label">TC Kimlik No</label>
                            <input name="AddressIdentityNumber" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="input-label">Telefon</label>
                            <input name="AddressPhone" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="input-label">Posta Kodu</label>
                            <input name="AddressZipCode" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-12 bill-type">
                        <div class="form-group">
                            <label class="input-label">Fatura Tipi</label>
                            <select class="form-control" id="bill-type" name="AddressBillType">
                                <option value="0">Seçiniz...</option>
                                <option value="individual">Bireysel</option>
                                <option value="corporate">Kurumsal</option>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4 company-detail">
                        <div class="form-group">
                            <label class="input-label">Firma Adı</label>
                            <input name="AddressCompanyName" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4 company-detail">
                        <div class="form-group">
                            <label class="input-label">Vergi Numarası</label>
                            <input name="AddressCompanyTaxNumber" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4 company-detail">
                        <div class="form-group">
                            <label class="input-label">Vergi Dairesi</label>
                            <input name="AddressCompanyTaxAdministration" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>

                </form>
            </div>

            <div class="modal-footer">
                <button type="button" id="btn-save" onclick="address_save()" class="btn btn-primary">Kaydet</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">İptal</button>
            </div>

        </div>
    </div>
</div>
<!-- End modal -->

<script type="text/javascript">

    var save_method;

    $(document).ready(function(){
        var pageActiveUrl = window.location.pathname;
        pageActiveUrl = pageActiveUrl.replace(/\//g,'-');

        $('#menu' + pageActiveUrl).addClass('active');

        $('#country-id').change(function(){
            var country_id = $(this).val();
            city_list(country_id);
        });

        $('#city-id').change(function(){
            var city_id = $(this).val();
            district_list(city_id);
        });

        $('#address-type').change(function(){
            var address_type = $(this).val();
            type_choose(address_type);
        });

        $('#bill-type').change(function(){
            var bill_type = $(this).val();
            bill_type_choose(bill_type);
        });

        $('.form-area input').change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        
        $('.form-area textarea').change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $('.form-area select').change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });

    function city_list(country_id, selected_id = '')
    {
        if (country_id == 0) {
            $('.city-area, .district-area').css('display', 'block');
            $('.alternative-city-area').css('display', 'none');
            $('#alternative-city').val('0');
        }else{
            $('.city-area, .district-area').css('display', 'none');
            $('.alternative-city-area').css('display', 'block');
            if (selected_id == '')
            {
                $('#alternative-city').val('');
            }
        }

        $.ajax({
            url : base_url + 'ajax/cities/' + country_id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('#city-id').html('<option value="0">Seçiniz...</option>');
                $('#district-id').html('<option value="0">Seçiniz...</option>');

                if (data != "" && typeof data.status === 'undefined') 
                {
                    $('.city-area, .district-area').css('display', 'block');
                    $('.alternative-city-area').css('display', 'none');
                    $('#alternative-city').val('0');

                    $.each(data, function(count, item) {
                        let selected = item.CityID == selected_id ? 'selected' : '';
                        $('#city-id').append('<option value="'+item.CityID+'" '+selected+'>'+item.CityName+'</option>');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Hata oluştu');
            }
        });
    }

    function district_list(city_id, selected_id = '')
    {
        $.ajax({
            url : base_url + 'ajax/districts/' + city_id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {   
                $('#district-id').html('<option value="0">Seçiniz...</option>');

                if (data != "" && typeof data.status === 'undefined') 
                {
                    $.each(data, function(count, item) {
                        let selected = item.DistrictID == selected_id ? 'selected' : '';
                        $('#district-id').append('<option value="'+item.DistrictID+'" '+selected+'>'+item.DistrictName+'</option>');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Hata oluştu');
            }
        });
    }

    function type_choose(val)
    {
        if (val == 'delivery')
        {
            $('.bill-type, .company-detail').css('display', 'none');
            $('[name="AddressBillType"]').val(0);
            $('[name="AddressCompanyName"], [name="AddressCompanyTaxNumber"], [name="AddressCompanyTaxAdministration"]').val('');
        }
        else if (val == 'billing')
        {
            $('.bill-type').css('display', 'block');
        }
    }

    function bill_type_choose(val)
    {
        if (val == 'individual')
        {
            $('.company-detail').css('display', 'none');
        }
        else if (val == 'corporate')
        {
            $('.company-detail').css('display', 'block');
        }
    }

    function address_add()
    {
        save_method = 'add';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty(); 
        $('.modal-title').text('Adres Ekle');
        $('#form-modal').modal('show');
        $('#error-message').text('');

        $('.alternative-city-area, .bill-type, .company-detail').css('display', 'none');
        $('#alternative-city').val('0');
        $('[name="AddressCountryID"]').val(65);
        $('#city-id').html('<option value="0">Seçiniz...</option>');
        $('#district-id').html('<option value="0">Seçiniz...</option>');
        city_list(65);
    }

    function address_edit(id)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#error-message').text('');
        $('.alternative-city-area, .bill-type, .company-detail').css('display', 'none');

        $.ajax({
            url : base_url + 'ajax/view_address/' + id,
            type: 'GET',
            dataType: 'JSON',
            success: function(data)
            {
                if (typeof(data.status) != "undefined" && data.status !== null)
                {
                    if(data.status == 'login')
                    {
                        window.location.href = base_url + 'auth/login';
                    }
                }

                type_choose(data.AddressType);
                bill_type_choose(data.AddressBillType);

                $('[name="AddressID"]').val(data.AddressID);
                $('[name="AddressTitle"]').val(data.AddressTitle);
                $('[name="AddressType"]').val(data.AddressType);
                $('[name="AddressFirstName"]').val(data.AddressFirstName);
                $('[name="AddressLastName"]').val(data.AddressLastName);
                $('[name="AddressCountryID"]').val(data.AddressCountryID);
                $('[name="AddressAlternativeCity"]').val(data.AddressAlternativeCity);
                $('[name="AddressOpenAddress"]').val(data.AddressOpenAddress);
                $('[name="AddressIdentityNumber"]').val(data.AddressIdentityNumber);
                $('[name="AddressZipCode"]').val(data.AddressZipCode);
                $('[name="AddressPhone"]').val(data.AddressPhone);
                $('[name="AddressEmail"]').val(data.AddressEmail);
                $('[name="AddressBillType"]').val(data.AddressBillType ? data.AddressBillType : 0);
                $('[name="AddressCompanyName"]').val(data.AddressCompanyName);
                $('[name="AddressCompanyTaxNumber"]').val(data.AddressCompanyTaxNumber);
                $('[name="AddressCompanyTaxAdministration"]').val(data.AddressCompanyTaxAdministration);

                city_list(data.AddressCountryID, data.AddressCityID);

                setTimeout(function(){
                    district_list(data.AddressCityID, data.AddressDistrictID);
                }, 500);

                $('#form-modal').modal('show'); 
                $('.modal-title').text('Adresi Düzenle');
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function address_save()
    {
        let url;
        let notify_type;
        let formData = new FormData($('#form')[0]);

        $('#btn-save').text('Kaydediyor...');
        $('#btn-save').attr('disabled',true);

        if(save_method == 'add') 
        {
            url = base_url + 'ajax/add_address';
            notify_message = 'Kayıt başarılı bir şekilde eklendi.';
        } 
        else
        {
            url = base_url + 'ajax/update_address';
            notify_message = 'Kayıt başarılı bir şekilde düzenlendi.';
        }

        $.ajax({
            url : url,
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'JSON',
            success: function(data)
            {
                if(data.status == 'login')
                {
                    window.location.href = base_url + 'auth/login';
                }

                if(data.status) 
                {
                    $('#form-modal').modal('hide');

                    Swal.fire({
                        title: notify_message,
                        icon: 'success',
                    }).then((result) => {
                        if (result.value) 
                        {
                            window.location.href = window.location.href;
                        }
                    });
                }
                else
                {   
                    for (var i = 0; i < data.error.inputerror.length; i++) 
                    {
                        $('[name="'+data.error.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                        $('[name="'+data.error.inputerror[i]+'"]').next().text(data.error.error_string[i]); 
                    }
                    
                    $('#error-message').text('*Gerekli alanları doldurmadan işleme devam edemezsiniz.');
                    $('#form-modal').stop().animate({
                        scrollTop:0
                    });
                }
               
                $('#btn-save').text('Kaydet'); 
                $('#btn-save').attr('disabled',false);
                
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btn-save').text('Kaydet'); 
                $('#btn-save').attr('disabled',false);
            }
        });
    }

    function address_destroy(address_id)
    {
        Swal.fire({
            title: 'Emin misiniz?',
            text: "Sil derseniz adresiniz silinecektir.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonText: 'Vazgeç',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sil'
        }).then((result) => {
            if (result.value) 
            {
                $('#preloader').fadeIn();

                let notify_type = '';
                let message = '';
                let icon = '';

                $.ajax({
                    url : base_url + 'ajax/destroy_address/' + address_id,
                    type: 'DELETE',
                    success: function(data)
                    {
                        if(data.status == 'login')
                        {
                            window.location.href = base_url + 'auth/login';
                        }
                        
                        $('#preloader').fadeOut();

                        data = JSON.parse(data);
                        message = data.message;

                        if(data.status)
                        {
                            notify_type = 'success';
                            icon = 'la la-check';
                        }
                        else
                        {
                            notify_type = 'danger';
                            icon = 'la la-close';
                        }

                        $.notify(
                            {
                                icon: icon,
                                message: message
                            },
                            {
                                type: notify_type,
                                animate: {
                                    enter: 'animated fadeInRight',
                                    exit: 'animated fadeOutRight'
                                }
                            }
                        );
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Adres silerken bir sorun oluştu.');
                    }
                });

                $('#address-item-' + address_id).slideUp(300, function() { $(this).remove(); });
            }
        });
    }

</script>