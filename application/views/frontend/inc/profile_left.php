<div class="left-menu">
    <ul>
        <li><a href="<?=base_url()?>profil" class="menu-profil">Temel Bilgiler</a></li>
        <li><a href="<?=base_url()?>profil/adreslerim" class="menu-profil-adreslerim">Adreslerim</a></li>
        <li><a href="<?=base_url()?>profil/siparislerim" class="menu-profil-siparislerim">Siparişlerim</a></li>
        <li><a href="<?=base_url()?>profil/kuponlarim" class="menu-profil-kuponlarim">Kuponlarım</a></li>
        <li><a href="<?=base_url()?>profil/favorilerim" class="menu-profil-favorilerim">Favorilerim</a></li>
        <li>
        	<a href="<?=base_url()?>uyelik/cikis-yap" class="logout">
        		<i class="las la-sign-out-alt"></i>
        		Güvenli Çıkış
        	</a>
        </li>
    </ul>
</div>