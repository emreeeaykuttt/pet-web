<div id="preloader">
    <div class="inner">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>

<!-- Header start -->
<div class="header">
    <div class="container d-lg-flex justify-content-between">
        <a href="<?= base_url() ?>" class="logo">
            <img width="250" height="32" alt="Petshopevinde.com" src="<?= base_url() ?>assets/frontend/img/petshopevinde_logo.svg" />
        </a>
        <div class="search-area d-none d-lg-flex">
            <div class="search">
                <input type="search" placeholder="Ürün, kategori veya marka ara" class="form-control search-key">
            </div>
            <button href="#" class="btn" onclick="search()">Ara</button>
        </div>
        <div class="top-menu">
            <ul>
                <li>
                    <a href="#" class="btn">
                        <?php if (!$this->session->userdata('UserID')): ?>
                        <i class="la la-user-alt"></i> Giriş Yap / Üye Ol <i class="la la-angle-down"></i> 
                        <?php endif ?>

                        <?php if ($this->session->userdata('UserID')): ?>
                        <i class="la la-user-alt"></i> <?=$this->session->userdata('UserFullname')?> <i class="la la-angle-down"></i> 
                        <?php endif ?>
                    </a>
                    <div class="sub-menu">
                        <ul>

                            <?php if (!$this->session->userdata('UserID')): ?>
                                <li><a class="login" href="<?=base_url()?>uyelik/giris-yap"><i class="la la-lock-open"></i> Giriş Yap</a></li>
                                <li><a class="register" href="<?=base_url()?>uyelik/uye-ol"><i class="la la-user-alt"></i> Üye Ol</a></li>
                            <?php endif ?>

                            <?php if ($this->session->userdata('UserID')): ?>
                                <li><a href="<?=base_url()?>profil" class="menu-profil">Temel Bilgiler</a></li>
                                <li><a href="<?=base_url()?>profil/adreslerim" class="menu-profil-adreslerim">Adreslerim</a></li>
                                <li><a href="<?=base_url()?>profil/siparislerim" class="menu-profil-siparislerim">Siparişlerim</a></li>
                                <li><a href="<?=base_url()?>profil/kuponlarim" class="menu-profil-kuponlarim">Kuponlarım</a></li>
                                <li><a href="<?=base_url()?>profil/favorilerim" class="menu-profil-favorilerim">Favorilerim</a></li>
                                <hr>
                                <li><a class="register" href="<?=base_url()?>uyelik/cikis-yap"><i class="las la-sign-out-alt"></i> Çıkış Yap</a></li>
                            <?php endif ?>
                            
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="<?=base_url('sepet')?>" class="btn shopping-cart-top">
                        <i class="la la-shopping-cart"></i> <span class="d-none d-md-inline-block d-lg-inline-block">Sepetim</span> <span class="badge badge-dark" id="basket-count"><?= !empty($this->session->userdata('UserBasketTotalProduct')) ? $this->session->userdata('UserBasketTotalProduct') : 0 ?></span>
                    </a>
                </li>
            </ul>
            <div class="hamburger bars"><i class="la la-bars"></i></div>
        </div>
    </div>

    <div class="menu text-center">
    <div class="search-area d-md-none d-flex">
            <div class="search">
                <input type="search" placeholder="Ürün, kategori veya marka ara" class="form-control search-key" >
            </div>
            <button href="#" class="btn" onclick="search()">Ara</button>
        </div>

        <?php
            $ci = &get_instance();
            $cat_menu = $ci->categories(unserialize(CATEGORIES), 2, 1);
            $dog_menu = $ci->categories(unserialize(CATEGORIES), 3, 1);
            $bird_menu = $ci->categories(unserialize(CATEGORIES), 4, 1);
            $fish_menu = $ci->categories(unserialize(CATEGORIES), 5, 1);
            $reptile_menu = $ci->categories(unserialize(CATEGORIES), 6, 1);
            $ginepig_menu = $ci->categories(unserialize(CATEGORIES), 57, 1);
            $rabbit_menu = $ci->categories(unserialize(CATEGORIES), 58, 1);
            $hamster_menu = $ci->categories(unserialize(CATEGORIES), 59, 1);
            $other_menu = $ci->categories(unserialize(CATEGORIES), 84, 1);
        ?>

        <ul>
            <li>
                <a href="#"><img alt="" src="/assets/frontend/img/kedi.svg" width="30" height="30" /> Kedi</a>
                <div class="sub-menu kedi container" data-menu="kedi">
                    <h2>KEDİ ÜRÜNLERİ</h2>
                    <?= $cat_menu ?>
                </div>
            </li>
            <li>
                <a href="#"><img alt="" src="/assets/frontend/img/kopek.svg" width="30" height="30" /> Köpek</a>
                <div class="sub-menu kopek container" data-menu="kopek">
                    <h2>KÖPEK ÜRÜNLERİ</h2>
                    <?= $dog_menu ?>
                </div>
            </li>
            <li><a href="#"><img alt="" src="/assets/frontend/img/kus.svg" width="30" height="30" /> Kuş</a>
                <div class="sub-menu kus container" data-menu="kus">
                    <h2>KUŞ ÜRÜNLERİ</h2>
                    <?= $bird_menu ?>
                </div>
            </li>
            <li><a href="#"><img alt="" src="/assets/frontend/img/balik.svg" width="30" height="30" />Balık</a>
                <div class="sub-menu balik container" data-menu="balik">
                    <h2>BALIK ÜRÜNLERİ</h2>
                    <?= $fish_menu ?>
                </div>
            </li>
            <li><a href="#"><img alt="" src="/assets/frontend/img/surungen.svg" width="30" height="30" />Sürüngen</a>
                <div class="sub-menu surungen container" data-menu="surungen">
                    <h2>SÜRÜNGEN ÜRÜNLERİ</h2>
                    <?= $reptile_menu ?>
                </div>
            </li>
            <li><a href="#"><img alt="" src="/assets/frontend/img/kemirgen.svg" width="30" height="30" />Kemirgen</a>
                <div class="sub-menu kemirgen container" data-menu="kemirgen">
                    <h2>KEMİRGEN ÜRÜNLERİ</h2>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="menu-subtitle">GİNEPİG</div>
                            <?= $ginepig_menu ?>
                        </div>
                        <div class="col-md-4">
                            <div class="menu-subtitle">TAVŞAN</div>
                            <?= $rabbit_menu ?>
                        </div>
                        <div class="col-md-4">
                            <div class="menu-subtitle">HAMSTER</div>
                            <?= $hamster_menu ?>
                        </div>
                    </div>
                </div>
            </li>
            
                <li><a href="#"><img alt="" src="/assets/frontend/img/diger.svg" width="30" height="30" />Diğer</a>
                <div class="sub-menu diger container" data-menu="diger">
                    <h2>DİĞER ÜRÜNLER</h2>
                    <?= $other_menu ?>
                </div>
            </li> 
           
        </ul>
    </div>
</div>

<!-- Header end -->