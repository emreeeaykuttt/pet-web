<script src="<?= base_url() ?>dist/bundle.js"></script>
<script type="text/javascript">
	var base_url = '<?= base_url() ?>';
	var server_url = '<?= SERVER_URL ?>';
	var currency = '<?= $this->session->userdata("UserCurrency") ?>';
	var currency_icon = '<?= $this->session->userdata("UserCurrencyCode") ?>'
</script>
<script src="<?= base_url() ?>assets/frontend/js/scripts.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
<script>
  WebFont.load({
	custom: {
    families: ['Sofia Pro', 'Line Awesome Free'],
    urls: ['/assets/frontend/fonts/stylesheet.css', '/assets/plugins/line-awesome/css/line-awesome.min.css']
  }
  });
</script>