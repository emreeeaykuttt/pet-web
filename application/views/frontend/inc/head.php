<meta name="google-site-verification" content="6wff61AGH-yz78TAP4IL7AwDczetjLZysoKnJSPj9L4" />
<meta name="application-name" content="Petshopevinde" />

<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?=base_url()?>assets/frontend/img/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="<?=base_url()?>assets/frontend/img/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?=base_url()?>assets/frontend/img/favicon-16x16.png" sizes="16x16" />
<meta name="application-name" content="Petshopevinde.com"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="<?=base_url()?>assets/frontend/img/mstile-144x144.png" />

<link rel="dns-prefetch" href="https://instagram.com">
<link rel="stylesheet preload prefetch" as="style" href="<?=base_url()?>dist/bundle.css">
<link rel="stylesheet preload prefetch" as="style" href="<?=base_url()?>assets/frontend/css/style.css">

<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-4QKYH5DX0W"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-4QKYH5DX0W');
</script>