<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Kedi Maması, Köpek Maması, Kuş, Balık ve Diğer Evcil Besinleri ile Malzemeleri, Türkiyenin en Büyük Online Petshop Mağazası petshopevinde.com'da">
    <meta name="keywords" content="Petshopevinde, petshop, Kedi Maması, Köpek Maması , Kuş, Balık, Akvaryum, Hamster, Kuş Yemi, Balık Yemi, Konserve, Kedi Kumu, Proplan, Hills, Royal Canin, Online Petshop, Ucuz Mama, Kaliteli Mama, Kampanyalı Mama">

    <title>Petshopevinde.com</title>

    <?php include('inc/head.php'); ?>

</head>

<body>

    <?php
    $currency = $this->session->userdata('UserCurrency');
    $currency_icon = $this->session->userdata('UserCurrencyCode');
    ?>

    <?php include('inc/nav_top.php'); ?>

    <h1 class="d-none">Petshopevinde</h1>

    <!-- Slider start -->

    <div class="container">

        <?php if ($sliders) : ?>
        <div class="owl-carousel owl-theme main-carousel">
            <?php foreach ($sliders as $key => $value) : ?>

            <div class="item">

                <?php if ($value['SliderLink']): ?>
                <a aria-label="Slider Link" href='<?=$value['SliderLink']?>'>
                <?php endif ?>

                <img width="1110" height="400" alt="" src="<?= SERVER_URL . 'resize/lg/' . $value['SliderPhoto'] ?>" class="w-100" loading="lazy" />
                <!-- <div class="slider-title">
                    <div><?=$value['SliderTitle']?></div>
                    <div><?=$value['SliderDescription']?></div>
                </div> -->

                <?php if ($value['SliderLink']): ?>
                </a>
                <?php endif ?>

            </div>

            <?php endforeach ?>
        </div>
        <?php endif ?>

        <hr>
        <div class="brands">
            <a aria-label="Marka" class="brand-item" href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"49"}]'>
                <img width="150" height="150" alt="ROYAL CANIN" src="/assets/frontend/img/royalcanin-logo.png" loading="lazy">
                <div class="name">ROYAL CANIN</div>
            </a>
            <a aria-label="Marka" class="brand-item" href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"44"}]'>
                <img width="150" height="150" alt="PRO PLAN" src="/assets/frontend/img/proplan-logo.png" loading="lazy">
                <div class="name">PRO PLAN</div>
            </a>
            <a aria-label="Marka" class="brand-item" href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"143"}]'>
                <img width="150" height="150"  alt="HILLS" src="/assets/frontend/img/hills-logo.png" loading="lazy">
                <div alt="" class="name">HILLS</div>
            </a>
            <a aria-label="Marka" class="brand-item" href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"119"}]'>
                <img width="150" height="150"  alt="DOGGIE" src="/assets/frontend/img/doggie-logo.png" loading="lazy">
                <div class="name">DOGGIE</div>
            </a>
            <a aria-label="Marka" class="brand-item" href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"132"}]'>
                <img width="150" height="150" alt="EVER CLEAN" src="/assets/frontend/img/ever-clean-logo.png" loading="lazy">
                <div class="name">EVER CLEAN</div>
            </a>
            <a aria-label="Marka"  class="brand-item" href='<?= base_url() ?>kategori/tumu?filter=[{"name":"Marka","value":"125"}]'>
                <img width="150" height="150" alt="PURELE" src="/assets/frontend/img/purele-logo.png" loading="lazy">
                <div class="name">PURELE</div>
            </a>
        </div>
    </div>

    <div class="container">
        <?php if ($products_by_tag1) { ?>
            <div class="products">
                <div class="title"><span>Günün Fırsatları</span></div>
                <div class="owl-carousel owl-theme product-carousel">

                    <?php foreach ($products_by_tag1 as $key => $product) : ?>
                        <a aria-label="<?= $product['ProductName'] ?>"  class="item" href="<?= base_url() . 'urun/' . $product['ProductSlug'] ?>">
                            <div class="name"><?= $product['ProductName'] ?></div>
                            <div class="image">

                                <?php
                                if (!empty($product['ProductPhoto'])) {
                                    $image = SERVER_URL . 'resize/xs/' . $product['ProductPhoto'];
                                } elseif (!empty($product['PhotoName'])) {
                                    $image = SERVER_URL . 'resize/xs/' . $product['PhotoName'];
                                } else {
                                    $image = SERVER_URL . 'upload/product/null-photo.png';
                                }
                                ?>
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="discount">-%<?= discount_render($product['ProductOption' . $currency . 'DiscountRate']) ?></span>
                                <?php endif ?>

                            </div>
                            <img width="267" height="150" src="<?= $image ?>" alt="<?= $product['PhotoTitle'] ? $product['PhotoTitle'] : $product['ProductPhotoAlt'] ?>" loading="lazy">

                            <div class="add-to-cart">
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="amount" id="product-discount-price">
                                        <?= number_format_render($product['ProductOption' . $currency . 'VatInclusivePrice']) ?>
                                    </span> 
                                <?php endif ?>
                                <?php if ($product['ProductOptionQuantity'] > 0 || $product['ProductOptionUnlimited'] == 1) { ?>
                                <div class="price"> <?= number_format_render($product['ProductOption' . $currency . 'LatestPrice']) ?>
                                    <span><?= $currency_icon ?></span>
                                </div>
                                <?php } else { ?>
                                <div class="price">STOKTA YOK</div>
                                <?php } ?>

                                <div class="go-to-product">ÜRÜNÜ İNCELE <i class="las la-long-arrow-alt-right"></i></div>
                            </div>
                        </a>
                    <?php endforeach ?>

                </div>
            </div>
        <?php } ?>

        <?php if ($best_selling_products) { ?>
            <div class="products">
                <div class="title"><span>Çok Satanlar</span></div>
                <div class="owl-carousel owl-theme product-carousel">

                    <?php foreach ($best_selling_products as $key => $product) : ?>
                        <a aria-label="<?= $product['ProductName'] ?>" class="item" href="<?= base_url() . 'urun/' . $product['ProductSlug'] ?>">
                            <div class="name"><?= $product['ProductName'] ?></div>
                            <div class="image">

                                <?php
                                if (!empty($product['ProductPhoto'])) {
                                    $image = SERVER_URL . 'resize/xs/' . $product['ProductPhoto'];
                                } elseif (!empty($product['PhotoName'])) {
                                    $image = SERVER_URL . 'resize/xs/' . $product['PhotoName'];
                                } else {
                                    $image = SERVER_URL . 'upload/product/null-photo.png';
                                }
                                ?>
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="discount">-%<?= discount_render($product['ProductOption' . $currency . 'DiscountRate']) ?></span>
                                <?php endif ?>

                            </div>
                            <img width="267" height="150" src="<?= $image ?>" alt="<?= $product['PhotoTitle'] ? $product['PhotoTitle'] : $product['ProductPhotoAlt'] ?>" loading="lazy">

                            <div class="add-to-cart">
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="amount" id="product-discount-price">
                                        <?= number_format_render($product['ProductOption' . $currency . 'VatInclusivePrice']) ?>
                                    </span> 
                                <?php endif ?>
                                <?php if ($product['ProductOptionQuantity'] > 0 || $product['ProductOptionUnlimited'] == 1) { ?>
                                <div class="price"> <?= number_format_render($product['ProductOption' . $currency . 'LatestPrice']) ?>
                                    <span><?= $currency_icon ?></span>
                                </div>
                                <?php } else { ?>
                                <div class="price">STOKTA YOK</div>
                                <?php } ?>
                                
                                <div class="go-to-product">ÜRÜNÜ İNCELE <i class="las la-long-arrow-alt-right"></i></div>
                            </div>
                        </a>
                    <?php endforeach ?>

                </div>
            </div>
        <?php } ?>

        <?php if ($products_by_tag2) { ?>
            <div class="products">
                <div class="title"><span>Kampanyalar</span></div>
                <div class="owl-carousel owl-theme product-carousel">

                    <?php foreach ($products_by_tag2 as $key => $product) : ?>
                        <a aria-label="?= $product['ProductName'] ?>" class="item" href="<?= base_url() . 'urun/' . $product['ProductSlug'] ?>">
                            <div class="name"><?= $product['ProductName'] ?></div>
                            <div class="image">

                                <?php
                                if (!empty($product['ProductPhoto'])) {
                                    $image = SERVER_URL . 'resize/xs/' . $product['ProductPhoto'];
                                } elseif (!empty($product['PhotoName'])) {
                                    $image = SERVER_URL . 'resize/xs/' . $product['PhotoName'];
                                } else {
                                    $image = SERVER_URL . 'upload/product/null-photo.png';
                                }
                                ?>
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="discount">-%<?= discount_render($product['ProductOption' . $currency . 'DiscountRate']) ?></span>
                                <?php endif ?>

                            </div>
                            <img  width="267" height="150" src="<?= $image ?>" alt="<?= $product['PhotoTitle'] ? $product['PhotoTitle'] : $product['ProductPhotoAlt'] ?>">

                            <div class="add-to-cart">
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="amount" id="product-discount-price">
                                        <?= number_format_render($product['ProductOption' . $currency . 'VatInclusivePrice']) ?>
                                    </span> 
                                <?php endif ?>
                                <?php if ($product['ProductOptionQuantity'] > 0 || $product['ProductOptionUnlimited'] == 1) { ?>
                                <div class="price"> <?= number_format_render($product['ProductOption' . $currency . 'LatestPrice']) ?>
                                    <span><?= $currency_icon ?></span>
                                </div>
                                <?php } else { ?>
                                <div class="price">STOKTA YOK</div>
                                <?php } ?>

                                <div class="go-to-product">ÜRÜNÜ İNCELE <i class="las la-long-arrow-alt-right"></i></div>
                            </div>
                        </a>
                    <?php endforeach ?>

                </div>
            </div>
        <?php } ?>
        
        <?php if ($products_by_tag3) { ?>
            <div class="products">
                <div class="title"><span>Kedi Kumları</span></div>
                <div class="owl-carousel owl-theme product-carousel">

                    <?php foreach ($products_by_tag3 as $key => $product) : ?>
                        <a aria-label="<?= $product['ProductName'] ?>" class="item" href="<?= base_url() . 'urun/' . $product['ProductSlug'] ?>">
                            <div class="name"><?= $product['ProductName'] ?></div>
                            <div class="image">

                                <?php
                                if (!empty($product['ProductPhoto'])) {
                                    $image = SERVER_URL . 'resize/xs/' . $product['ProductPhoto'];
                                } elseif (!empty($product['PhotoName'])) {
                                    $image = SERVER_URL . 'resize/xs/' . $product['PhotoName'];
                                } else {
                                    $image = SERVER_URL . 'upload/product/null-photo.png';
                                }
                                ?>
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="discount">-%<?= discount_render($product['ProductOption' . $currency . 'DiscountRate']) ?></span>
                                <?php endif ?>

                            </div>
                            <img width="267" height="150" src="<?= $image ?>" alt="<?= $product['PhotoTitle'] ? $product['PhotoTitle'] : $product['ProductPhotoAlt'] ?>" loading="lazy">

                            <div class="add-to-cart">
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="amount" id="product-discount-price">
                                        <?= number_format_render($product['ProductOption' . $currency . 'VatInclusivePrice']) ?>
                                    </span> 
                                <?php endif ?>
                                <?php if ($product['ProductOptionQuantity'] > 0 || $product['ProductOptionUnlimited'] == 1) { ?>
                                <div class="price"> <?= number_format_render($product['ProductOption' . $currency . 'LatestPrice']) ?>
                                    <span><?= $currency_icon ?></span>
                                </div>
                                <?php } else { ?>
                                <div class="price">STOKTA YOK</div>
                                <?php } ?>

                                <div class="go-to-product">ÜRÜNÜ İNCELE<i class="las la-long-arrow-alt-right"></i></div>
                            </div>
                        </a>
                    <?php endforeach ?>

                </div>
            </div>
        <?php } ?>

        <?php if ($products_by_tag4) { ?>
            <div class="products">
                <div class="title"><span>Avantajlı Paketler</span></div>
                <div class="owl-carousel owl-theme product-carousel">

                    <?php foreach ($products_by_tag4 as $key => $product) : ?>
                        <a arial-label="<?= $product['ProductName'] ?>" class="item" href="<?= base_url() . 'urun/' . $product['ProductSlug'] ?>">
                            <div class="name"><?= $product['ProductName'] ?></div>
                            <div class="image">

                                <?php
                                if (!empty($product['ProductPhoto'])) {
                                    $image = SERVER_URL . 'resize/xs/' . $product['ProductPhoto'];
                                } elseif (!empty($product['PhotoName'])) {
                                    $image = SERVER_URL . 'resize/xs/' . $product['PhotoName'];
                                } else {
                                    $image = SERVER_URL . 'upload/product/null-photo.png';
                                }
                                ?>
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="discount">-%<?= discount_render($product['ProductOption' . $currency . 'DiscountRate']) ?></span>
                                <?php endif ?>
                                
                            </div>
                            <img width="267" height="150" src="<?= $image ?>" alt="<?= $product['PhotoTitle'] ? $product['PhotoTitle'] : $product['ProductPhotoAlt'] ?>" loading="lazy">

                            <div class="add-to-cart">
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="amount" id="product-discount-price">
                                        <?= number_format_render($product['ProductOption' . $currency . 'VatInclusivePrice']) ?>
                                    </span> 
                                <?php endif ?>
                                <?php if ($product['ProductOptionQuantity'] > 0 || $product['ProductOptionUnlimited'] == 1) { ?>
                                <div class="price"> <?= number_format_render($product['ProductOption' . $currency . 'LatestPrice']) ?>
                                    <span><?= $currency_icon ?></span>
                                </div>
                                <?php } else { ?>
                                <div class="price">STOKTA YOK</div>
                                <?php } ?>

                                <div class="go-to-product">ÜRÜNÜ İNCELE<i class="las la-long-arrow-alt-right"></i></div>
                            </div>
                        </a>
                    <?php endforeach ?>

                </div>
            </div>
        <?php } ?>

        <div class="container p-0 d-flex justify-content-between mb-4 banner-area">
            <a aria-label href="<?= base_url('kategori/kedi-mamalari-8') ?>">
                <img width="500" height="150"  alt="KEDİ MAMALARI" src="/assets/frontend/img/kedi-mamalari-banner.jpg">
            </a>
            <a aria-label href="<?= base_url('kategori/kopek-mamalari-24') ?>">
                <img width="500" height="150" alt="KÖPEK MAMALARI" src="/assets/frontend/img/kopek-mamalari-banner.jpg">
            </a>
        </div>

        <div class="info-bar">
            <div class="d-flex h-100">
                <div class="item align-self-center">
                    <img width="70" height="70" alt="GÜVENLİ ALIŞVERİŞ" src="/assets/frontend/img/guvenli-alisveris.svg" />
                    GÜVENLİ ALIŞVERİŞ
                </div>
                <div class="item align-self-center">
                    <img width="70" height="70" alt="HIZLI TESLİMAT" src="/assets/frontend/img/hizli-teslimat.svg" />
                    HIZLI TESLİMAT
                </div>
                <div class="item align-self-center">
                    <img width="70" height="70" alt="ORJİNAL ÜRÜN" src="/assets/frontend/img/orijinal-urun.svg" />
                    ORJİNAL ÜRÜN
                </div>
            </div>
            <div class="free-shipping">
              140 TL VE ÜZERİNE ÜCRETSİZ KARGO
            </div>
        </div>

        <?php if ($products_by_tag5) { ?>
            <div class="products">
                <div class="title"><span>Editörün Seçimi</span></div>
                <div class="owl-carousel owl-theme product-carousel">

                    <?php foreach ($products_by_tag5 as $key => $product) : ?>
                        <a  aria-label class="item" href="<?= base_url() . 'urun/' . $product['ProductSlug'] ?>">
                            <div class="name"><?= $product['ProductName'] ?></div>
                            <div class="image">

                                <?php
                                if (!empty($product['ProductPhoto'])) {
                                    $image = SERVER_URL . 'resize/xs/' .  $product['ProductPhoto'];
                                } elseif (!empty($product['PhotoName'])) {
                                    $image = SERVER_URL . 'resize/xs/' .  $product['PhotoName'];
                                } else {
                                    $image = SERVER_URL . 'upload/product/null-photo.png';
                                }
                                ?>
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="discount">-%<?= discount_render($product['ProductOption' . $currency . 'DiscountRate']) ?></span>
                                <?php endif ?>

                            </div>
                            <img width="267" height="150" src="<?= $image ?>" alt="<?= $product['PhotoTitle'] ? $product['PhotoTitle'] : $product['ProductPhotoAlt'] ?>" loading="lazy">

                            <div class="add-to-cart">
                                <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                    <span class="amount" id="product-discount-price">
                                        <?= number_format_render($product['ProductOption' . $currency . 'VatInclusivePrice']) ?>
                                    </span> 
                                <?php endif ?>
                                <?php if ($product['ProductOptionQuantity'] > 0 || $product['ProductOptionUnlimited'] == 1) { ?>
                                <div class="price"> <?= number_format_render($product['ProductOption' . $currency . 'LatestPrice']) ?>
                                    <span><?= $currency_icon ?></span>
                                </div>
                                <?php } else { ?>
                                <div class="price">STOKTA YOK</div>
                                <?php } ?>
                                
                                <div class="go-to-product">ÜRÜNÜ İNCELE <i class="las la-long-arrow-alt-right"></i></div>
                            </div>
                        </a>
                    <?php endforeach ?>

                </div>
            </div>
    </div>

    <?php } ?>

<?php include('inc/footer.php'); ?>

<?php include('inc/script.php'); ?>

<script type="text/javascript">
    $(document).ready(function() {

        $('.main-carousel').owlCarousel({
            loop: true,
            margin: 0,
            autoplay: true,
            autoplayTimeout: 7000,
            autoplayHoverPause: true,
            nav: true,
            dots: false,
            animateOut: "fadeOut",
            animateIn: "fadeIn",
            navText: ["<i class='la la-angle-left'></i>", "<i class='la la-angle-right'></i>"],

            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });

        $('.product-carousel').owlCarousel({
            loop: true,
            margin: 20,
            autoplay: false,
            autoplayTimeout: 7000,
            autoplayHoverPause: true,
            nav: true,
            dots: false,
            animateOut: "fadeOut",
            animateIn: "fadeIn",
            navText: ["<i class='la la-angle-left'></i>", "<i class='la la-angle-right'></i>"],

            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 5
                }
            }
        });

    });
</script>
</body>

</html>