<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="0549 474 19 25 - bilgi@petshopevinde.com - Aşık Veysel Mah. Hadımköy-Bahçeşehir Yolu Cad. Parkcity Evleri D Blok No:2/14-17 Esenyurt/İSTANBUL">
    <meta name="keywords" content="İletişim">

    <title>İletişim | petshopevinde.com</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page contact-page">

        <div class="container">
            <h1 class="title mb-4 pt-4">iLETİŞİM</h1>
            <div class="content">
                <div class="map">
                    <!-- map -->
                </div>
                <div class="info row mt-2">
                    <div class="col-md-12">
                        <div class="address mb-4 mt-3">
                        <i class="las la-map-marker-alt"></i>
                        <p>
                            Yenidoğan Mah. Halit Cansever Sok. No:32 İzmit/Kocaeli
                        </p>
                       </div>
                    </div>
                    <div class="col-md-4">
                        <div class="email d-flex align-items-center justify-content-center">
                            <div class="email-inner">
                            <i class="las la-envelope"></i>bilgi@petshopevinde.com
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="phone d-flex align-items-center justify-content-center ">
                            <div class="phone-inner">
                                <i class="las la-phone"></i>
                                0262<br><strong>331 73 34</strong>
                            </div>
           
                        </div>
                    </div>
                </div>
              
            </div>
        </div>
    </div>
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.contact-nav').addClass('active');
        });
    </script>

</body>

</html>