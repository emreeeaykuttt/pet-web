<!DOCTYPE html>
<html lang="tr">
    
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Mesafeli Satış Sözleşmesi">
    <meta name="keywords" content="Mesafeli Satış Sözleşmesi">

    <title>Mesafeli Satış Sözleşmesi | petshopevinde.com</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
    
    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page" >
        <div class="container">
            <div class="col-md-12">
                <h1 class="title mb-4 pt-4">Mesafeli Satış Sözleşmesi</h1>
                <div class="content">

                    <p><b>1-</b> Bu s&ouml;zleşme ile yazılı, g&ouml;rsel ve elektronik ortamda veya diğer iletişim ara&ccedil;ları kullanılarak ve t&uuml;keticilerle karşı karşıya gelinmeksizin yapılan, malın veye hizmetin t&uuml;keticiye anında veya sonradan teslimi veya ifası şeklinde yapılan &quot;T&uuml;ketici&quot; (Bundan sonra M&uuml;şteri olarak anılacaktır.) ve &quot;Satıcı&quot; arasındaki alışverişe ilişkin koşullar d&uuml;zenlenir. &nbsp;</p>

                    <p><b>2-</b> <b>SATICI</b>,&nbsp; Doa Satış Pazarlama ve Ltd. Şti. &#39; dir (Bundan sonra &quot;<b>PETSHOPEVİNDE</b>&quot; olarak anılacaktır. .</p>

                    <p>Mersis Numarası: 030251828100011</p>

                    <p>Adres:&nbsp; Yenidoğan Mah. Halit Cansever Sok. No:32/1 İzmit Kocaeli</p>

                    <p>E posta adresi: <a href="mailto:info@petshopevinde.com">info@petshopevinde.com</a></p>

                    <p>Tel no: 0262 331 73 34</p>

                    <p><b>3- ALICI</b> , ........................ &#39;dir. (Bundan sonra&nbsp; &quot;<b>ALICI</b>&quot; olarak anılacaktır.)</p>

                    <p>TC No : ...........................</p>

                    <p>Adres: ....................................................</p>

                    <p>E posta adresi :..........................................</p>

                    <p>Teslim edilecek kişi &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>

                    <p>Teslimat Adresi &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..<br />
                    Telefon &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.<br />
                    Faks &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..<br />
                    Eposta/kullanıcı adı &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>

                    <p><b>4- SİPARİŞ VEREN KİŞİ BİLGİLERİ</b></p>

                    <p>Ad/Soyad/Unvan &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>

                    <p>Adres &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.<br />
                    Telefon &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;<br />
                    Faks &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.<br />
                    Eposta/kullanıcı adı &hellip;&hellip;&hellip;&hellip;&hellip;...................</p>

                    <p><b>5- S&Ouml;ZLEŞME KONUSU &Uuml;R&Uuml;N/&Uuml;R&Uuml;NLER BİLGİLERİ</b></p>

                    <p><b>5.1. </b>Malın /&Uuml;r&uuml;n/&Uuml;r&uuml;nlerin/ Hizmetin temel &ouml;zelliklerini (t&uuml;r&uuml;, miktarı, marka/modeli, rengi, adedi) PETSHOPEVİNDE&rsquo;ye ait internet sitesinde yayınlanmaktadır. PETSHOPEVİNDE tarafından kampanya d&uuml;zenlenmiş ise ilgili &uuml;r&uuml;n&uuml;n temel &ouml;zelliklerini kampanya s&uuml;resince inceleyebilirsiniz. Kampanyaya dahil edilmiş &uuml;r&uuml;nler kampanya tarihi dışında kampanyayla aynı koşullarda satışa sunulamamaktadır.</p>

                    <p><b>5.2. </b>Listelenen ve sitede ilan edilen fiyatlar satış fiyatıdır. İlan edilen fiyatlar ve vaatler g&uuml;ncelleme yapılana ve değiştirilene kadar ge&ccedil;erlidir. S&uuml;reli olarak ilan edilen fiyatlar ise belirtilen s&uuml;re sonuna kadar ge&ccedil;erlidir.</p>

                    <p><b>5.3. </b>S&ouml;zleşme konusu mal ya da hizmetin t&uuml;m vergiler d&acirc;hil satış fiyatı aşağıda g&ouml;sterilmiştir.</p>

                    <table class="table table-bordered">
                        <tr>
                            <td><b>Ürün Açıklaması</b></td>
                            <td><b>Adet</b></td>
                            <td><b>Birim Fiyatı</b></td>
                            <td><b>Ara Toplam (KDV Dahil)</b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><b>Kargo Tutarı:</b></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2"><b>Toplam:</b></td>
                            <td colspan="2"></td>
                        </tr>
                    </table>

                    <p>&nbsp;</p>

                    <p>&Ouml;deme Şekli ve Planı</p>

                    <p><b>5.4. </b>&Uuml;r&uuml;n sevkiyat masrafı olan kargo &uuml;creti ALICI tarafından &ouml;denecektir.</p>

                    <p><b>6-&nbsp; FATURA BİLGİLERİ</b></p>

                    <p>Ad/Soyad/Unvan &hellip;&hellip;&hellip;&hellip;&hellip;.</p>

                    <p>Adres &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.<br />
                    Telefon&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.<br />
                    Faks&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;<br />
                    Eposta/kullanıcı adı &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..<br />
                    Fatura teslim :Fatura sipariş teslimatı sırasında fatura adresine sipariş ile birlikte<br />
                    teslim edilecektir.</p>

                    <p><b>7- GENEL H&Uuml;K&Uuml;MLER</b></p>

                    <p><b>7.1.</b>ALICI, PETSHOPEVİNDE&lsquo;ye ait internet sitesinde s&ouml;zleşme konusu &uuml;r&uuml;n&uuml;n temel nitelikleri, satış fiyatı ve &ouml;deme şekli ile teslimata ilişkin &ouml;n bilgileri okuyup, bilgi sahibi olduğunu, elektronik ortamda gerekli teyidi verdiğini kabul, beyan ve taahh&uuml;t eder. ALICI&rsquo;nın; &Ouml;n Bilgilendirmeyi elektronik ortamda teyit etmesi, mesafeli satış s&ouml;zleşmesinin kurulmasından evvel, PETSHOPEVİNDE tarafından ALICI&#39; ya verilmesi gereken adresi, siparişi verilen &uuml;r&uuml;nlere ait temel &ouml;zellikleri, &uuml;r&uuml;nlerin vergiler dahil fiyatını, &ouml;deme ve teslimat bilgilerini de doğru ve eksiksiz olarak edindiğini kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.2.</b>S&ouml;zleşme konusu her bir &uuml;r&uuml;n, 30 g&uuml;nl&uuml;k yasal s&uuml;reyi aşmamak kaydı ile ALICI&#39; nın yerleşim yeri uzaklığına bağlı olarak internet sitesindeki &ouml;n bilgiler kısmında belirtilen s&uuml;re zarfında ALICI veya ALICI&rsquo;nın g&ouml;sterdiği adresteki kişi ve/veya kuruluşa teslim edilir. Bu s&uuml;re i&ccedil;inde &uuml;r&uuml;n&uuml;n ALICI&rsquo;ya teslim edilememesi durumunda, ALICI&rsquo;nın s&ouml;zleşmeyi feshetme hakkı saklıdır.</p>

                    <p><b>7.3.</b>PETSHOPEVİNDE, S&ouml;zleşme konusu &uuml;r&uuml;n&uuml; eksiksiz, siparişte belirtilen niteliklere uygun ve varsa garanti belgeleri, kullanım kılavuzları işin gereği olan bilgi ve belgeler ile teslim etmeyi, her t&uuml;rl&uuml; ayıptan ari olarak yasal mevzuat gereklerine g&ouml;re sağlam, standartlara uygun bir şekilde işi doğruluk ve d&uuml;r&uuml;stl&uuml;k esasları dahilinde ifa etmeyi, hizmet kalitesini koruyup y&uuml;kseltmeyi, işin ifası sırasında gerekli dikkat ve &ouml;zeni g&ouml;stermeyi, ihtiyat ve &ouml;ng&ouml;r&uuml; ile hareket etmeyi kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.4.</b>PETSHOPEVİNDE, s&ouml;zleşmeden doğan ifa y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;n&uuml;n s&uuml;resi dolmadan ALICI&rsquo;yı bilgilendirmek ve a&ccedil;ık&ccedil;a onayını almak suretiyle eşit kalite ve fiyatta farklı bir &uuml;r&uuml;n tedarik edebilir.</p>

                    <p><b>7.5.</b>PETSHOPEVİNDE, sipariş konusu &uuml;r&uuml;n veya hizmetin yerine getirilmesinin imkansızlaşması halinde s&ouml;zleşme konusu y&uuml;k&uuml;ml&uuml;l&uuml;klerini yerine getiremezse, bu durumu, &ouml;ğrendiği tarihten itibaren 3 g&uuml;n i&ccedil;inde yazılı olarak t&uuml;keticiye bildireceğini, 14 g&uuml;nl&uuml;k s&uuml;re i&ccedil;inde toplam bedeli ALICI&rsquo;ya iade edeceğini kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.6.</b>ALICI, S&ouml;zleşme konusu &uuml;r&uuml;n&uuml;n teslimatı i&ccedil;in işbu S&ouml;zleşme&rsquo;yi elektronik ortamda teyit edeceğini, herhangi bir nedenle s&ouml;zleşme konusu &uuml;r&uuml;n bedelinin &ouml;denmemesi ve/veya banka kayıtlarında iptal edilmesi halinde, PETSHOPEVİNDE&lsquo; s&ouml;zleşme konusu &uuml;r&uuml;n&uuml; teslim y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;n&uuml;n sona ereceğini kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.7.</b>SATICI, tarafların iradesi dışında gelişen, &ouml;nceden &ouml;ng&ouml;r&uuml;lemeyen ve tarafların bor&ccedil;larını yerine getirmesini engelleyici ve/veya geciktirici hallerin oluşması gibi m&uuml;cbir sebepler nedeni ile s&ouml;zleşme konusu &uuml;r&uuml;n&uuml; s&uuml;resi i&ccedil;inde teslim edemez ise, durumu ALICI&#39;ya bildireceğini kabul, beyan ve taahh&uuml;t eder. ALICI da siparişin iptal edilmesini, s&ouml;zleşme konusu &uuml;r&uuml;n&uuml;n varsa emsali ile değiştirilmesini ve/veya teslimat s&uuml;resinin engelleyici durumun ortadan kalkmasına kadar ertelenmesini SATICI&rsquo;dan talep etme hakkını haizdir. ALICI tarafından siparişin iptal edilmesi halinde ALICI&rsquo;nın nakit ile yaptığı &ouml;demelerde, &uuml;r&uuml;n tutarı 14 g&uuml;n i&ccedil;inde kendisine nakden ve defaten &ouml;denir. ALICI&rsquo;nın kredi kartı ile yaptığı &ouml;demelerde ise, &uuml;r&uuml;n tutarı, siparişin ALICI tarafından iptal edilmesinden sonra 14 g&uuml;n i&ccedil;erisinde ilgili bankaya iade edilir. ALICI, SATICI tarafından kredi kartına iade edilen tutarın banka tarafından ALICI hesabına yansıtılmasına ilişkin ortalama s&uuml;recin 2 ile 3 haftayı bulabileceğini, bu tutarın bankaya iadesinden sonra ALICI&rsquo;nın hesaplarına yansıması halinin tamamen banka işlem s&uuml;reci ile ilgili olduğundan, ALICI, olası gecikmeler i&ccedil;in SATICI&rsquo;yı sorumlu tutamayacağını kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.8.</b>SATICININ, ALICI tarafından siteye kayıt (&uuml;yelik) formunda belirtilen veya daha sonra kendisi tarafından g&uuml;ncellenen adresi, e-posta adresi, sabit ve mobil telefon hatları ve diğer iletişim bilgileri &uuml;zerinden mektup, e-posta, SMS, telefon g&ouml;r&uuml;şmesi ve diğer yollarla iletişim, pazarlama, bildirim ve diğer ama&ccedil;larla ALICI&rsquo;ya ulaşma hakkı bulunmaktadır. ALICI, işbu s&ouml;zleşmeyi kabul etmekle SATICI&rsquo;nın kendisine y&ouml;nelik yukarıda belirtilen iletişim faaliyetlerinde bulunabileceğini kabul ve beyan etmektedir.</p>

                    <p><b>7.9.</b>ALICI, s&ouml;zleşme konusu mal/hizmeti teslim almadan &ouml;nce muayene edecek; ezik, kırık, ambalajı yırtılmış vb. hasarlı ve ayıplı mal/hizmeti kargo şirketinden teslim almayacaktır. ALICI tarafından teslim alınan mal/hizmetin hasarsız ve sağlam olduğu kabul edilecektir. Teslimden sonra mal/hizmetin &ouml;zenle korunması borcu, ALICI&rsquo;ya aittir. Cayma hakkı kullanılacaksa mal/hizmet kullanılmamalıdır. Fatura iade edilmelidir.</p>

                    <p><b>7.10.</b>ALICI ile sipariş esnasında kullanılan kredi kartı hamilinin aynı kişi olmaması veya &uuml;r&uuml;n&uuml;n ALICI&rsquo;ya tesliminden evvel, siparişte kullanılan kredi kartına ilişkin g&uuml;venlik a&ccedil;ığı tespit edilmesi halinde, SATICI, kredi kartı hamiline ilişkin kimlik ve iletişim bilgilerini, siparişte kullanılan kredi kartının bir &ouml;nceki aya ait ekstresini yahut kart hamilinin bankasından kredi kartının kendisine ait olduğuna ilişkin yazıyı ibraz etmesini ALICI&rsquo;dan talep edebilir. ALICI&rsquo;nın talebe konu bilgi/belgeleri temin etmesine kadar ge&ccedil;ecek s&uuml;rede sipariş dondurulacak olup, mezkur taleplerin 24 saat i&ccedil;erisinde karşılanmaması halinde ise SATICI, siparişi iptal etme hakkını haizdir.</p>

                    <p><b>7.11.</b>ALICI, SATICI&rsquo;ya ait internet sitesine &uuml;ye olurken verdiği kişisel ve diğer sair bilgilerin ger&ccedil;eğe uygun olduğunu, SATICI&rsquo;nın bu bilgilerin ger&ccedil;eğe aykırılığı nedeniyle uğrayacağı t&uuml;m zararları, SATICI&rsquo;nın ilk bildirimi &uuml;zerine derhal, nakden ve defaten tazmin edeceğini beyan ve taahh&uuml;t eder.</p>

                    <p><b>7.12.</b>ALICI, SATICI&rsquo;ya ait internet sitesini kullanırken yasal mevzuat h&uuml;k&uuml;mlerine riayet etmeyi ve bunları ihlal etmemeyi baştan kabul ve taahh&uuml;t eder. Aksi takdirde, doğacak t&uuml;m hukuki ve cezai y&uuml;k&uuml;ml&uuml;l&uuml;kler tamamen ve m&uuml;nhasıran ALICI&rsquo;yı bağlayacaktır.</p>

                    <p><b>7.13.</b>ALICI, SATICI&rsquo;ya ait internet sitesini hi&ccedil;bir şekilde kamu d&uuml;zenini bozucu, genel ahlaka aykırı, başkalarını rahatsız ve taciz edici şekilde, yasalara aykırı bir ama&ccedil; i&ccedil;in, başkalarının maddi ve manevi haklarına tecav&uuml;z edecek şekilde kullanamaz. Ayrıca, &uuml;ye başkalarının hizmetleri kullanmasını &ouml;nleyici veya zorlaştırıcı faaliyet (spam, virus, truva atı, vb.) işlemlerde bulunamaz.</p>

                    <p><b>7.14.</b>SATICI&rsquo;ya ait internet sitesinin &uuml;zerinden, SATICI&rsquo;nın kendi kontrol&uuml;nde olmayan ve/veya başkaca &uuml;&ccedil;&uuml;nc&uuml; kişilerin sahip olduğu ve/veya işlettiği başka web sitelerine ve/veya başka i&ccedil;eriklere link verilmiş olabilir. Bu linkler ALICI&rsquo;ya y&ouml;nlenme kolaylığı sağlamak amacıyla konmuş olup herhangi bir web sitesini veya o siteyi işleten kişiyi desteklememekte ve Link verilen web sitesinin i&ccedil;erdiği bilgilere y&ouml;nelik herhangi bir garanti niteliği taşımamaktadır.</p>

                    <p><b>7.15.</b>İşbu s&ouml;zleşme i&ccedil;erisinde sayılan maddelerden bir ya da birka&ccedil;ını ihlal eden &uuml;ye işbu ihlal nedeniyle cezai ve hukuki olarak şahsen sorumlu olup, SATICI&rsquo;yı bu ihlallerin hukuki ve cezai sonu&ccedil;larından ari tutacaktır. Ayrıca; işbu ihlal nedeniyle, olayın hukuk alanına intikal ettirilmesi halinde, SATICI&rsquo;nın &uuml;yeye karşı &uuml;yelik s&ouml;zleşmesine uyulmamasından dolayı tazminat talebinde bulunma hakkı saklıdır.</p>

                    <p><b>8. CAYMA HAKKI</b></p>

                    <p><b>8.1.</b>ALICI; mesafeli s&ouml;zleşmenin mal satışına ilişkin olması durumunda, &uuml;r&uuml;n&uuml;n kendisine veya g&ouml;sterdiği adresteki kişi/kuruluşa teslim tarihinden itibaren 14 (ond&ouml;rt) g&uuml;n i&ccedil;erisinde, SATICI&rsquo;ya bildirmek şartıyla hi&ccedil;bir hukuki ve cezai sorumluluk &uuml;stlenmeksizin ve hi&ccedil;bir gerek&ccedil;e g&ouml;stermeksizin malı reddederek s&ouml;zleşmeden cayma hakkını kullanabilir. Hizmet sunumuna ilişkin mesafeli s&ouml;zleşmelerde ise, bu s&uuml;re s&ouml;zleşmenin imzalandığı tarihten itibaren başlar. Cayma hakkı s&uuml;resi sona ermeden &ouml;nce, t&uuml;keticinin onayı ile hizmetin ifasına başlanan hizmet s&ouml;zleşmelerinde cayma hakkı kullanılamaz. Cayma hakkının kullanımından kaynaklanan masraflar SATICI&rsquo; ya aittir. ALICI, iş bu s&ouml;zleşmeyi kabul etmekle, cayma hakkı konusunda bilgilendirildiğini kabul eder.</p>

                    <p><b>8.2.</b>Cayma hakkının kullanılması i&ccedil;in 14 (ond&ouml;rt) g&uuml;nl&uuml;k s&uuml;re i&ccedil;inde SATICI&#39; ya iadeli taahh&uuml;tl&uuml; posta, faks veya eposta ile yazılı bildirimde bulunulması ve &uuml;r&uuml;n&uuml;n işbu s&ouml;zleşmede d&uuml;zenlenen &quot;Cayma Hakkı Kullanılamayacak &Uuml;r&uuml;nler&quot; h&uuml;k&uuml;mleri &ccedil;er&ccedil;evesinde kullanılmamış olması şarttır. Bu hakkın kullanılması halinde,</p>

                    <p><b>a)</b>3. kişiye veya ALICI&rsquo; ya teslim edilen &uuml;r&uuml;n&uuml;n faturası, (İade edilmek istenen &uuml;r&uuml;n&uuml;n faturası kurumsal ise, iade ederken kurumun d&uuml;zenlemiş olduğu iade faturası ile birlikte g&ouml;nderilmesi gerekmektedir. Faturası kurumlar adına d&uuml;zenlenen sipariş iadeleri İADE FATURASI kesilmediği takdirde tamamlanamayacaktır.)</p>

                    <p><b>b)</b>İade formu,</p>

                    <p><b>c)</b>İade edilecek &uuml;r&uuml;nlerin kutusu, ambalajı, varsa standart aksesuarları ile birlikte eksiksiz ve hasarsız olarak teslim edilmesi gerekmektedir.</p>

                    <p><b>d)</b>SATICI, cayma bildiriminin kendisine ulaşmasından itibaren en ge&ccedil; 10 g&uuml;nl&uuml;k s&uuml;re i&ccedil;erisinde toplam bedeli ve ALICI&rsquo;yı bor&ccedil; altına sokan belgeleri ALICI&rsquo; ya iade etmekle ve ALICI 20 g&uuml;nl&uuml;k s&uuml;re i&ccedil;erisinde malı satıcıya teslim etmekle y&uuml;k&uuml;ml&uuml;d&uuml;r.</p>

                    <p><b>e)</b>ALICI&rsquo; nın kusurundan kaynaklanan bir nedenle malın değerinde bir azalma olursa veya iade imkansızlaşırsa ALICI kusuru oranında SATICI&rsquo; nın zararlarını tazmin etmekle y&uuml;k&uuml;ml&uuml;d&uuml;r. Ancak cayma hakkı s&uuml;resi i&ccedil;inde malın veya &uuml;r&uuml;n&uuml;n usul&uuml;ne uygun kullanılması sebebiyle meydana gelen değişiklik ve bozulmalardan ALICI sorumlu değildir.</p>

                    <p><b>f)</b>Cayma hakkının kullanılması nedeniyle SATICI tarafından d&uuml;zenlenen kampanya limit tutarının altına d&uuml;ş&uuml;lmesi halinde kampanya kapsamında faydalanılan indirim miktarı iptal edilir.</p>

                    <p><b>9. CAYMA HAKKI KULLANILAMAYACAK &Uuml;R&Uuml;NLER</b></p>

                    <p>ALICI&rsquo;nın isteği veya a&ccedil;ık&ccedil;a kişisel ihtiya&ccedil;ları doğrultusunda hazırlanan ve geri g&ouml;nderilmeye m&uuml;sait olmayan &uuml;r&uuml;n ve/veya malzemeleri, tek kullanımlık &uuml;r&uuml;nler, &ccedil;abuk bozulma tehlikesi olan veya son kullanma tarihi ge&ccedil;me ihtimali olan mallar, ALICI&rsquo;ya teslim edilmesinin ardından ALICI tarafından ambalajı a&ccedil;ıldığı takdirde iade edilmesi sağlık ve hijyen a&ccedil;ısından uygun olmayan &uuml;r&uuml;nler, teslim edildikten sonra başka &uuml;r&uuml;nlerle karışan ve doğası gereği ayrıştırılması m&uuml;mk&uuml;n olmayan &uuml;r&uuml;nler, elektronik ortamda anında ifa edilen hizmetler veya t&uuml;keticiye anında teslim edilen gayrimaddi mallar ile ses veya g&ouml;r&uuml;nt&uuml; kayıtlarının, kitap, dijital i&ccedil;erik, yazılım programlarının, veri kaydedebilme ve veri depolama cihazlarının ambalajının ALICI tarafından a&ccedil;ılmış olması halinde iadesi Y&ouml;netmelik gereği m&uuml;mk&uuml;n değildir.</p>

                    <p>a) Hijyenik &uuml;r&uuml;nlerin hi&ccedil;bir sebeple iadesi yoktur.(ambalajı a&ccedil;ılmış kedi yada k&ouml;peğinizin bedenine temas etmiş olması)<br />
                    b) Kedi tuvalet &uuml;r&uuml;nleri.<br />
                    c) Tırnak makası, fır&ccedil;a, tarak gibi kolay kontaminasyona uygun &uuml;r&uuml;nler( Ambalajı a&ccedil;ılmasa dahi!)<br />
                    d) Tek kullanımlık &uuml;r&uuml;nler.<br />
                    e) G&ouml;z, kulak damlası gibi ağızı a&ccedil;ıldıktan sonra kullanım s&uuml;resi olan &uuml;r&uuml;nler.<br />
                    f) K&uuml;lotlar ve pedler.</p>

                    <p>Kozmetik ve pet bakım &uuml;r&uuml;nleri, giyim &uuml;r&uuml;nleri, kitap, kopyalanabilir yazılım ve programlar, DVD, VCD, CD ve kasetlerin iade edilebilmesi i&ccedil;in ambalajlarının a&ccedil;ılmamış, denenmemiş, bozulmamış ve kullanılmamış olmaları gerekir.</p>

                    <p><b>10. TEMERR&Uuml;T HALİ VE HUKUKİ SONU&Ccedil;LARI</b></p>

                    <p>ALICI, &ouml;deme işlemlerini kredi kartı ile yaptığı durumda temerr&uuml;de d&uuml;şt&uuml;ğ&uuml; takdirde, kart sahibi banka ile arasındaki kredi kartı s&ouml;zleşmesi &ccedil;er&ccedil;evesinde faiz &ouml;deyeceğini ve bankaya karşı sorumlu olacağını kabul, beyan ve taahh&uuml;t eder. Bu durumda ilgili banka hukuki yollara başvurabilir; doğacak masrafları ve vekalet &uuml;cretini ALICI&rsquo;dan talep edebilir ve her koşulda ALICI&rsquo;nın borcundan dolayı temerr&uuml;de d&uuml;şmesi halinde, ALICI, borcun gecikmeli ifasından dolayı SATICI&rsquo;nın uğradığı zarar ve ziyanını &ouml;deyeceğini kabul, beyan ve taahh&uuml;t eder.</p>

                    <p><b>11. YETKİLİ MAHKEME</b></p>

                    <p><b>İşbu s&ouml;zleşmeden doğan uyuşmazlıklarda şikayet ve itirazlar </b>ilgili yasalarda belirtilen parasal sınırlar dahilinde t&uuml;ketici sorunları hakem heyetine veya t&uuml;ketici mahkemesine yapılabilir.<br />
                    İşbu S&ouml;zleşme ticari ama&ccedil;larla yapılmaktadır. İşbu s&ouml;zleşme &ccedil;er&ccedil;evesinde meydana gelebilecek uyuşmazlıklarda kanunlarda &ouml;zel bir d&uuml;zenleme yoksa Kocaeli Mahkemeleri ve İcra Daireleri yetkilidir.</p>

                    <p><b>12. Y&Uuml;R&Uuml;RL&Uuml;K</b></p>

                    <p>ALICI, Site &uuml;zerinden verdiği siparişe ait &ouml;demeyi ger&ccedil;ekleştirdiğinde işbu s&ouml;zleşmenin t&uuml;m şartlarını kabul etmiş sayılır.&nbsp;</p>

                </div>
            </div>
        </div>
    </div>
  
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

</body>
    
</html>