<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Gizlilik Politikası">
    <meta name="keywords" content="Gizlilik Politikası">

    <title>Gizlilik Politikası | petshopevinde.com</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page">
        <div class="container">
            <div class="col-md-12">
                <h1 class="title mb-4 pt-4">Gizlilik Politikası</h1>
                <div class="content">
                    
                    <p><strong>GİZLİLİK POLİTİKAMIZ</strong></p>

                    <p>Sitemizde 6698 s. KVKK , 6563 s. Elektronik Ticaretin D&uuml;zenlenmesi Hakkında Kanun kişisel verilerin korunması hakkında h&uuml;k&uuml;mler &ccedil;er&ccedil;evesinde kişisel veri toplanmaktadır.&nbsp;</p>

                    <p>Alıcı tarafından iş bu s&ouml;zleşmede belirtilen bilgiler ile &ouml;deme yapmak amacı ile satıcıya bildirdiği bilgiler satıcı tarafından 3. şahıslarla paylaşılmayacaktır.</p>

                    <p>Satıcı bu bilgileri sadece idari/ yasal zorunluluğun mevcudiyeti &ccedil;er&ccedil;evesinde a&ccedil;ıklayabilecektir. Araştırma ehliyeti belgelenmiş her t&uuml;rl&uuml; adli soruşturma dahilinde satıcı kendisinden istenen bilgiyi elinde bulunduruyorsa ilgili makama sağlayabilir.</p>

                    <p>Kredi Kartı bilgileri kesinlikle saklanmaz, kredi kartı bilgileri sadece tahsilat işlemi sırasında ilgili bankalara g&uuml;venli bir şekilde iletilerek provizyon alınması i&ccedil;in kullanılır ve provizyon sonrası sistemden silinir.</p>

                    <p>Alıcıya ait e-posta adresi, posta adresi ve telefon gibi bilgiler yalnızca satıcı tarafından standart &uuml;r&uuml;n teslim ve bilgilendirme prosed&uuml;rleri i&ccedil;in kullanılır. Bazı d&ouml;nemlerde kampanya bilgileri, yeni &uuml;r&uuml;nler hakkında bilgiler, promosyon bilgileri alıcıya onayı sonrasında g&ouml;nderilebilir.</p>

                    <p><b>G&uuml;venlik</b><br />
                    <b>Size ait bilgiler koruma altında, g&ouml;n&uuml;l rahatlığıyla alışverişin keyfini &ccedil;ıkartın...</b></p>

                    <p><a href="<?=base_url()?>">www.petshopevinde.com</a>&rsquo;dan alışveriş yapan m&uuml;şterilerimizin kredi kartı, adres, telefon&hellip;vs bilgileri SSL g&uuml;venlik sistemi tarafından korunmakta olup &uuml;&ccedil;&uuml;nc&uuml; kişilerle paylaşılmaz ve ele ge&ccedil;irmeleri olanaksızdır.</p>

                    <p><b>G&uuml;venli Alışveriş Nedir?</b></p>

                    <p>G&uuml;venli Alışveriş satıcıyı, malın teslimi karşılığında bedeli tahsil edememe riskine karşı; alıcıyı da bedelin &ouml;denmesine karşılık, malı teslim alamama veya istenilen nitelik ve şartlarda teslim alamama riskine karşı korumaktadır.</p>

                    <p>G&uuml;venli Alışveriş, elektronik ortamda yapılan alışverişlerde hem alıcıyı hem de satıcıyı koruyan bir uygulamadır. G&uuml;venli Alışveriş sayesinde alıcıya satın aldığı &uuml;r&uuml;n&uuml; g&ouml;rerek onaylama imk&acirc;nı, satıcıya ise &ouml;deme g&uuml;vencesi sunulmaktadır.</p>

                    <p><b>SSL Nedir?</b></p>

                    <p>SSL, Secure Sockets Layer&#39;in baş harflerinden oluşan bir kısaltmadır. Kullandığınız tarayıcı ile bağlı olduğunuz sitenin sunucusu arasında g&uuml;venlikli bilgi alış-verişini sağlayan bir y&ouml;ntemdir. Herhangi bir web sitesinde dolaştığınız ve bilgi girişi yaptığınız zaman, &uuml;&ccedil;&uuml;nc&uuml; şahıslar tarafından izlenme olasılığı vardır. Kişisel bilgilerinize, kredi kartı numaranıza, teknolojik a&ccedil;ıdan son derece gelişkin olanaklara sahip diğer kullanıcılar tarafından ulaşılması ve bunların isteğiniz dışında kullanılması olanaksız değildir.</p>

                    <p>SSL g&uuml;venlik sistemi bu t&uuml;r istenmeyen durumları &ouml;nlemek i&ccedil;in geliştirilmiş bir şifreleme y&ouml;ntemidir. SSL g&uuml;venlik alanında g&ouml;nderdiğiniz bilgiler &ccedil;ok sıradan g&ouml;r&uuml;nen, fakat &ccedil;&ouml;z&uuml;lmesi ger&ccedil;ekte imkansız olan bir kodlama sistemiyle şifrelenir.</p>
                    
                </div>
            </div>
        </div>
    </div>

    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

</body>

</html>