<!DOCTYPE html>
<html lang="tr">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Tablosan, yaşam alanınıza renk katacak tasarımlar ve görsel ürünler üretir. Bütün üretim tamamen el işçiliğiyle olup kanvas tablolarda, Türk üretimi pamuklu bez ile en iyi ve güncel baskı teknikleri kullanılmaktadır.">
    <meta name="keywords" content="Hakkımızda">

    <title>Hakkımızda | petshopevinde.com</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
	
	<?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>
    <div class="sub-page" >
        <div class="container">
            <div class="col-md-12">
                <h1 class="title mb-4 pt-4">Hakkımızda</h1>
                <div class="content">
                   
                   <p><b>BİZ KİMİZ&nbsp;</b></p>

                    <p>PETSHOPEVİNDE olarak zooteknist olan kurucumuz ve biz herşeyden &ouml;nce hayvanların beslenmesine odaklı bir firmayız. Hayvan sevgisi ve bilimsel yaklaşım bizim itici g&uuml;c&uuml;m&uuml;zd&uuml;r. Hayvanları tutkuyla sevmemiz bizi onlar i&ccedil;in faydalı birşeyler yapmaya sevk ediyor.&nbsp;</p>

                    <p>Firmamızın hayvan besleme ile ilgili ilk hareketi kendi mamamızı &uuml;retmek oldu. Zootekni bilimi hareketimizin dayanağı oldu. <b>Pratele</b> (&Ccedil;ek dilinde &quot;dost&quot; anlamındadır.) ve <b>Local Hero</b> (Yerel Kahraman) aile işletmemizde kendi &uuml;rettiğimiz evcil hayvan mamalarıdır. Bu mamaların form&uuml;lasyonunu hazırlarken hayvanlarımızın &ouml;m&uuml;r boyu sağlıklı ve dengeli beslenmelerini hedefledik. Her yeni g&uuml;n bu mamalarımızı geliştirme ve evcil hayvanlarımızın yaşam kalitelerini arttırma isteği ile &ccedil;alışmaktayız. Pratele ve Local Heronun kendi laboratuvarlarımızda test edilen kendi &uuml;retimimiz olması bu mamalarımızın her zaman taze olarak piyasaya sunulmasını sağlıyor ve bu da bize işimizi hakkıyla yapmanın g&ouml;n&uuml;l rahatlığını veriyor.&nbsp;</p>

                    <p>Hayvan sevgimiz ve ilgimiz dikkatimizi evcil hayvan malzemelerine de y&ouml;nelttiğinde bu işe de eğilmemiz gerektiğine karar verdik. Mutlu bir şekilde PETSHOPEVİNDE&#39; yi kurduk ve en az bizim kadar evcil hayvanlarını ve hatta t&uuml;m hayvanları &ouml;nemsediğini bildiğimiz m&uuml;şterilerimizi desteklemeyi temel motivasyon edindik.</p>


                </div>
            </div>
        </div>
    </div>
   
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.about-nav').addClass('active');
        });
    </script>

</body>
	
</html>