<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Çerez Politikası">
    <meta name="keywords" content="Çerez Politikası">

    <title>Çerez Politikası | petshopevinde.com</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page contact-page">
        <div class="container">
            <div class="col-md-12">
                <h1 class="title mb-4 pt-4">Çerez Politikası</h1>
                <div class="content">
                    
                    <p><strong>&Ccedil;EREZ POLİTİKAMIZ</strong></p>

                    <p>Petshopevinde olarak internet sitemizin d&uuml;zg&uuml;n &ccedil;alışmasını sağlamak, m&uuml;şterilerimize gerekli hizmet ve &ouml;zellikleri sağlamak, m&uuml;şterilerimizin ilgi alanları hakkında bilgi sahibi olmak i&ccedil;in &ccedil;erez teknolojisi kullanmaktayız. &Ccedil;erezler, internet sitemizi ziyaret ettiğinizde bilgisayarınıza ya da mobil cihazınıza kaydedilen k&uuml;&ccedil;&uuml;k metin dosyalarıdır. Bu metin dosyaları ile birlikte web işaret&ccedil;ileri, pikseller veya etiketler gibi diğer takip&ccedil;iler de işbu &Ccedil;erez Politikası kapsamında &ccedil;erez olarak kabul edilecektir.</p>

                    <p>&Ccedil;erezlerle;&nbsp;</p>

                    <ul>
                        <li>Site g&uuml;venliğimizi sağlamakta,</li>
                        <li>Site performansı arttırılmakta ve site daha kullanıcı dostu haline getirilmekte,</li>
                        <li>M&uuml;şterimizin alışveriş tercihlerini tanımakta,&nbsp;</li>
                        <li>M&uuml;şterilerimizin sipariş takibi yapılmakta,&nbsp;</li>
                        <li>&Uuml;r&uuml;n satış, m&uuml;şteri memnuniyeti ve site performansı raporlanması gibi konularda raporlama yapılmakta,</li>
                        <li>&Ccedil;erez Politikası kullanıcıya sunulmakta,&nbsp;</li>
                        <li>&Ouml;zelleştirilmiş hizmetler sağlamaktayız.&nbsp;</li>
                    </ul>


                </div>
            </div>
        </div>
    </div>
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

</body>

</html>