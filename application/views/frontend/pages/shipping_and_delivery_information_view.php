<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Kargo ve Teslimat Bilgileri">
    <meta name="keywords" content="Kargo ve Teslimat Bilgileri">

    <title>Kargo ve Teslimat Bilgileri | petshopevinde.com</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page">
        <div class="container">
            <div class="col-md-12">
                <h1 class="title mb-4 pt-4">Kargo ve Teslimat Bilgileri</h1>
                <div class="content">
                    
                    <p>Siparişleriniz, banka onayı alındıktan sonra en ge&ccedil; 2 iş g&uuml;n&uuml; (Pazartesi-Cuma) i&ccedil;erisinde (Saat 16.00&#39;a kadar) kargoya teslim edilir.&nbsp; Haftasonu kargolama ile ilgili son kargo &ccedil;ıkışı saati Cumartesi i&ccedil;in 11.00&#39; dır. İşletmemiz Pazar g&uuml;n&uuml; &ccedil;alışmadığı i&ccedil;in pazar g&uuml;nleri işlem yapılmamaktadır.</p>

                    <p>&Ouml;zel &uuml;retim &uuml;r&uuml;nlerin teslim s&uuml;releri imalat zamanına g&ouml;re farklılık g&ouml;stermektedir.</p>

                    <p>Bu t&uuml;r &uuml;r&uuml;nlerin teslimat bilgileri ve s&uuml;releri &uuml;r&uuml;n sayfalarında belirtilmiştir.</p>

                    <p>Tarafımızdan kaynaklanan bir aksilik olması halinde ise size &uuml;yelik bilgilerinizden yola &ccedil;ıkılarak haber verilecektir. Bu y&uuml;zden &uuml;yelik bilgilerinizin eksiksiz ve doğru olması &ouml;nemlidir. Bayram ve tatil g&uuml;nlerinde teslimat yapılmamaktadır.</p>

                    <p>Se&ccedil;tiğiniz &uuml;r&uuml;nlerin tamamı anlaşmalı olduğumuz kargo şirketleri tarafından kargo garantisi ile size teslim edilecektir.</p>

                    <p>Satın aldığınız &uuml;r&uuml;nler bir teyit e-posta&#39;sı ile tarafınıza bildirilecektir. Se&ccedil;tiğiniz &uuml;r&uuml;nlerden herhangi birinin stokta mevcut olmaması durumunda konu ile ilgili bir e-posta size yollanacak ve &uuml;r&uuml;n&uuml;n ilk stoklara gireceği tarih tarafınıza bildirilecektir.</p>

                    <p>PetShopEvinde&nbsp;online alışveriş sitesidir. Aynı anda birden &ccedil;ok kullanıcıya alışveriş yapma imkanı tanır. Enderde olsa t&uuml;keticinin aynı &uuml;r&uuml;n&uuml; alması s&ouml;z konusudur ve &uuml;r&uuml;n stoklarda t&uuml;kenmektedir bu durumda ;</p>

                    <p>&Ouml;demesini internet &uuml;zerinden yaptınız &uuml;r&uuml;n eğer stoklarmızda kalmamış ise en az 1 (Bir) en fazla 3 (&Uuml;&ccedil;) g&uuml;n bekeleme s&uuml;resi vardır. &Uuml;r&uuml;n bu tarihleri arasında t&uuml;keticiye verilemez ise yaptığı &ouml;deme kendisine iade edilir.</p>

                </div>
            </div>
        </div>
    </div>
    
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

</body>

</html>