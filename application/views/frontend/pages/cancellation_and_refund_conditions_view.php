<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="İptal ve İade Şartları">
    <meta name="keywords" content="İptal ve İade Şartları">

    <title>İptal ve İade Şartları | petshopevinde.com</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page contact-page">
        <div class="container">
            <div class="col-md-12">
                <h1 class="title mb-4 pt-4">İptal ve İade Şartları</h1>
                <div class="content">
                    
                    <p><b>İptal ve İade Politikamız:</b></p>

                    <p>Aşağıda sayılan istisnalardan biri uygulanmıyorsa, herhangi bir neden g&ouml;stermeksizin, mesafeli satış&nbsp; s&ouml;zleşmesinin akdedildiği g&uuml;nden itibaren 14 g&uuml;n i&ccedil;erisinde siparişinizden cayma hakkına sahipsiniz.&nbsp;</p>

                    <p>Cayma talebinizi, iletişim kanallarımızdan veya cayma formunu tarafımıza g&ouml;ndererek bildirebilirsiniz.Cayma bildiriminin alındığına ilişkin teyidimiz tarafınıza iletilir.</p>

                    <p>Son cayma tarihini ge&ccedil;memek i&ccedil;in, cayma hakkını kullanmak istediğinize dair iletişimi cayma d&ouml;nemi sona ermeden g&ouml;ndermeniz ve &uuml;r&uuml;nleri aşağıda tanımlanan son tarih i&ccedil;erisinde, İade Destek Merkezimiz aracılığıyla iade etmeniz yeterli olacaktır.</p>

                    <p>Cayma hakkın kullanımıyla ilgili kapsam, i&ccedil;erik ve talimatlar ile ilgili ek bilgi i&ccedil;in, M&uuml;şteri Hizmetleriyle iletişim kurmanız sağlıklı olacaktır.</p>

                    <p>Siparişinizden caymanız durumunda, satın alınan &uuml;r&uuml;nler i&ccedil;in sizden aldığımız t&uuml;m &ouml;demeyi ve teslimat &uuml;cretlerini geri &ouml;deyeceğiz. Bu &ouml;deme siparişin iptali ile ilgili bildiriminizi aldığımız tarihten itibaren en ge&ccedil; 14 g&uuml;n i&ccedil;erisinde yapılacaktır.&nbsp;</p>

                    <p>&Uuml;r&uuml;nleri bize siparişinizden caydığınızı bize ilettiğiniz tarihten itibaren en ge&ccedil; 10 g&uuml;n i&ccedil;erisinde g&ouml;ndermeniz gerekmektedir. Son tarihi ge&ccedil;irmemek i&ccedil;in, &uuml;r&uuml;nleri 10 g&uuml;nl&uuml;k d&ouml;nem sona ermeden &ouml;nce g&ouml;ndermeniz yeterlidir. Teslim edilen &uuml;r&uuml;nlerin sipariş edilen &uuml;r&uuml;nler ile aynı olması ve bizim tarafımızdan belirlenen kargo şirketinin kullanılmış olması halinde &uuml;r&uuml;nlerin normal iade &uuml;cretini &uuml;stlenmeniz gerekmeyecektir.</p>

                    <p>&Uuml;r&uuml;nlerin niteliğinin, &ouml;zelliklerinin ve işlevinin ortaya koyulması i&ccedil;in gerekli olan durumlar hari&ccedil; olmak &uuml;zere, &uuml;r&uuml;nlerin niteliğinin, &ouml;zelliklerinin ve işlevinin saptanmasının gerekli olduğu durumların &ouml;tesine ge&ccedil;ecek şekilde &uuml;r&uuml;nlerin ele alınması sebebiyle &uuml;r&uuml;nlerin değerinin azalması durumunda, &uuml;r&uuml;nlerin yıpranmasına ilişkin bir tazminat &ouml;demeniz gerekebilecektir.</p>

                    <p><b>CAYMA HAKKININ İSTİSNALARI</b></p>

                    <p>Aşağıdakilere ilişkin s&ouml;zleşmelerde cayma hakkı kullanılamamaktadır veya zamanaşımına uğramaktadır:</p>

                    <ul>
                        <li>Tesliminden sonra koruyucu unsurları a&ccedil;ılmış olan mallardan; iadesi sağlık ve hijyen a&ccedil;ısından uygun olmayanların teslimine ilişkin s&ouml;zleşmeler veya tesliminden sonra başka &uuml;r&uuml;nlerle karışan ve doğası gereği ayrıştırılması m&uuml;mk&uuml;n olmayan mallara ilişkin s&ouml;zleşmeler;</li>
                        <li>T&uuml;keticinin istekleri veya kişisel ihtiya&ccedil;ları doğrultusunda hazırlanan mallara ilişkin s&ouml;zleşmeler;</li>
                        <li>&Ccedil;abuk bozulabilen veya son kullanma tarihi ge&ccedil;ebilecek malların teslimine ilişkin s&ouml;zleşmeler;</li>
                        <li>Fiyatı piyasalardaki dalgalanmalara bağlı olarak değişen ve kontrol&uuml;m&uuml;zde olmayan &uuml;r&uuml;nlere ilişkin s&ouml;zleşmeler.</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>

</body>

</html>