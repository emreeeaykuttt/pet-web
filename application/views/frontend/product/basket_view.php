<!DOCTYPE html>
<html lang="tr">
	
<head>
	<meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sepet">
    <meta name="keywords" content="Sepet">

    <title>Sepet</title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>
	
	<?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page shopping-cart">
        <div class="container">

            <div class="cart-navigation d-sm-flex justify-content-sm-between">

                <div class="status cart active">
                    <span>Sepet</span>
                </div>
                <div class="status adress">
                    <span>Adres</span>
                </div>
                <div class="status payment">
                    <span>Ödeme</span>
                </div>

            </div>
            <div class="cart-detail">
                <div class="cart-titles">
                    <div class="row">
                        <div class="col-md-4 col-4">Ürün</div>
                        <div class="col-md-2 col-2">Fiyat</div>
                        <div class="col-md-3 col-3 text-center">Adet</div>
                        <div class="col-md-3 col-3">Toplam</div>
                    </div>
                </div>
                <div class="cart-products">

                    <?php
                        $currency = $this->session->userdata('UserCurrency'); 
                        $currency_icon = $this->session->userdata('UserCurrencyCode'); 
                        $basket_total_price = 0;
                    ?>

                    <?php if ($products_in_basket): ?>
                    <?php foreach ($products_in_basket as $key => $value): ?>
                    <div class="product-item" id="product-item-<?=$value['BasketID']?>">

                        <form action="#" id="form-basket-<?=$value['BasketID']?>">

                            <?php $basket_total_price += ($value['BasketProductQuantity'] *$value['ProductOption'.$currency.'LatestPrice']); ?>
                            <input type="hidden" name="ProductID[<?=$value['BasketID']?>]" value="<?=$value['ProductID']?>">
                            <input type="hidden" name="ProductContentID[<?=$value['BasketID']?>]" value="<?=$value['ProductContentID']?>">
                            <input type="hidden" name="ProductOptionID[<?=$value['BasketID']?>]" value="<?=$value['ProductOptionID']?>">
                            <input type="hidden" name="ProductOptionContentID[<?=$value['BasketID']?>]" value="<?=$value['ProductOptionContentID']?>">
                            <input type="hidden" name="ProductOptionLatestPrice[<?=$value['BasketID']?>]" value="<?=$value['ProductOption'.$currency.'LatestPrice']?>">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-4 col-4">
                                            
                                            <?php 
                                                if (!empty($value['ProductPhoto'])) {
                                                    $image = SERVER_URL . 'resize/xs/' . $value['ProductPhoto'];
                                                }elseif (!empty($value['PhotoName'])) {
                                                    $image = SERVER_URL . 'resize/xs/' . $value['PhotoName'];
                                                }else{
                                                    $image = SERVER_URL . 'upload/product/null-photo.png';
                                                } 
                                            ?>

                                            <img src="<?=$image?>" alt="<?=$value['PhotoTitle'] ? $value['PhotoTitle'] : $value['ProductPhotoAlt']?>">
                                            
                                        </div>
                                        <div class="col-md-8 col-8 d-flex flex-column">
                                            <div class="my-auto">
                                                <b><?=$value['ProductName']?></b><br />
                                                <!-- <?=$value['ProductOptionDetails']?> -->
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2 d-flex">
                                    <span class="align-self-center">
                                        <?=number_format_render($value['ProductOption'.$currency.'LatestPrice'])?>
                                        <span class="currency"><?=$currency_icon?></span>
                                    </span>
                                </div>

                                <div class="col-md-3 d-flex justify-content-center ">
                                    <div class="d-flex align-self-center">
                                        <button type="button" class="btn btn-light btn-minus quantity-minus" data-id="<?=$value['BasketID']?>">  
                                            <i class="la la-minus m-0"></i>
                                        </button>
                                        <input 
                                            type="number" 
                                            min="1" 
                                            oninput="validity.valid||(value='');" 
                                            name="ProductOptionQuantity[<?=$value['BasketID']?>]" 
                                            value="<?=$value['BasketProductQuantity']?>" 
                                            class="count product-option-quantity" 
                                            data-id="<?=$value['BasketID']?>"
                                        />
                                        <div class="quantity-warning" id="quantity-warning-<?=$value['BasketID']?>">Güncelle</div>
                                        <button type="button" class="btn btn-light btn-plus quantity-plus" data-id="<?=$value['BasketID']?>"> 
                                            <i class="la la-plus m-0"></i>
                                        </button>
                                    </div>
                                </div>

                                <div class="col-md-3 d-flex">
                                    <div class="align-self-center w-100">
                                        <span class="total-price">
                                            <span id="product-total-price-<?=$value['BasketID']?>">
                                                <?=number_format_render(($value['BasketProductQuantity'] * $value['ProductOption'.$currency.'LatestPrice']))?>
                                            </span>
                                            <span class="currency"><?=$currency_icon?></span>
                                        </span>
                                        <a href="javascript:void(0)" onclick="destroy(<?=$value['BasketID']?>)" class="btn btn-remove btn-light float-right mr-2">
                                            <i class="las la-times m-0"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </form>

                    </div>
                    <?php endforeach ?>
                    <?php endif ?>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?php if ($this->session->userdata('UserID')): ?>
                        <div class="cart-discount mt-2">
                            <div class="input-group mb-3">
                                <input type="text" name="CouponCode" class="form-control" placeholder="Kupon Kodu">
                                <div class="input-group-append">
                                    <a href="javascript:void(0)" onclick="use_coupon_code()" class="btn btn-black" type="button">Uygula</a>
                                </div>
                            </div>
                        </div>
                        <?php endif ?>
                    </div>
                    <div class="col-md-6">
                        <div class="container cart-summary">

                            <div class="row">
                                <div class="col-md-8 col-8">Ürünler Toplamı (KDV Dahil)</div>
                                <div class="col-md-4 col-4">
                                    <input type="hidden" name="BasketTotalPrice" value="<?=$basket_total_price?>">
                                    <span id="basket-total-price">
                                        <?=number_format_render($basket_total_price)?>
                                    </span>
                                    <span class="currency"><?=$currency_icon?></span>
                                </div>
                            </div>
                            <div class="row coupon-area">
                                <div class="col-md-8 col-8">Kupon İndirimi</div>
                                <div class="col-md-4 col-4">
                                    <span id="coupon-amount"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-8">Kargo</div>
                                <div class="col-md-4 col-4">
                                    <span id="cargo-price">
                                        <?=$basket_total_price > 0 && $basket_total_price < 140 ? '9.89' : '0.00'?>
                                    </span>
                                    <span class="currency"><?=$currency_icon?></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <img src="<?=base_url()?>assets/frontend/img/address-icon.svg " width="30 " class="m-2 " />
                                    Tahmini Teslimat Tarihi: <?=date('d.m.Y', strtotime('+ 7 days'))?>
                                </div>
                            </div>
                            <div class="total">
                                <div class="row">
                                    <div class="col-md-8 col-6">Toplam</div>
                                    <div class="col-md-4 col-6">
                                        <span id="basket-cargo-inclusive-price">
                                            <?=$basket_total_price > 0 && $basket_total_price < 140 ? number_format_render($basket_total_price + 9.89) : number_format_render($basket_total_price)?>
                                        </span>
                                        <span class="currency"><?=$currency_icon?></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="mt-3">
                            <div class="row">
                                <div class="col-md-8"></div>
                                <div class="col-md-4">
                                    <a href="<?=base_url()?>order/delivery" id="delivery-btn" class="btn btn-black p-3 w-100">Devam Et</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
	<?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
	<?php include(dirname(__DIR__) . '/inc/script.php'); ?>

    <script type="text/javascript">
        var coupon_amount = 0;
        var coupon_rate = 0;

        $(document).ready(function(){

            $('.quantity-plus').click(function() {
                $('#preloader').fadeIn();

                let basket_id = $(this).attr('data-id');
                let quantity = $('[name="ProductOptionQuantity['+basket_id+']"]').val();
                $('[name="ProductOptionQuantity['+basket_id+']"]').val(parseInt(quantity) + 1);

                update(basket_id, 'plus');
            });

            $('.quantity-minus').click(function() {
                let basket_id = $(this).attr('data-id');
                let quantity = $('[name="ProductOptionQuantity['+basket_id+']"]').val();

                if (parseInt(quantity) > 1)
                {
                    $('#preloader').fadeIn();
                    $('[name="ProductOptionQuantity['+basket_id+']"]').val(parseInt(quantity) - 1);

                    update(basket_id, 'minus');
                }
                else {
                    destroy(basket_id);
                }
            });

            $('.product-option-quantity').on('focusin', function(){
                $(this).data('val', $(this).val());
            });

            $('.product-option-quantity').on('change', function(){
                let prev_val = $(this).data('val');
                let basket_id = $(this).attr('data-id');
                update(basket_id, '', prev_val);
            });

            $('.quantity-warning').click(function(){
                $(this).css('display', 'none');
            });

            $('.product-option-quantity').keydown(function(){
                let basket_id = $(this).attr('data-id');
                $('#quantity-warning-' + basket_id).css('display', 'block');
            });

        });

        function use_coupon_code()
        {
            let coupon_code = $('[name="CouponCode"]').val();

            if (coupon_code)
            {
                $.ajax({
                    url : base_url + 'ajax/view_coupon/' + coupon_code,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function(data)
                    {
                        if (typeof(data.status) != "undefined" && data.status !== null)
                        {
                            if(data.status == 'login')
                            {
                                window.location.href = base_url + 'auth/login';
                            }
                        }
                        
                        if (data)
                        {
                            let cargo_price = 9.89;
                            let basket_total_price = parseFloat($('[name="BasketTotalPrice"]').val());

                            if (basket_total_price >= data.CouponMinAmount)
                            {
                                if (data.CouponAmount)
                                {
                                    coupon_amount = parseFloat(data.CouponAmount);
                                }
                                else
                                {
                                    coupon_rate = parseFloat(data.CouponRate);
                                }

                                if (basket_total_price > 140) {
                                    cargo_price = 0;
                                    $('#cargo-price').text('0.00');
                                } else{
                                    $('#cargo-price').text('9.89');
                                }

                                if (coupon_amount > 0)
                                {
                                    $('#coupon-amount').html(number_format(coupon_amount.toFixed(2)) + ' <span class="currency">₺</span>');
                                    $('#basket-cargo-inclusive-price').text((basket_total_price - coupon_amount + cargo_price).toFixed(2));
                                }
                                else
                                {
                                    let discount_amount = (basket_total_price / 100) * coupon_rate;
                                    $('#coupon-amount').html(coupon_rate.toFixed(2) + ' <span class="currency">%</span>');
                                    $('#basket-cargo-inclusive-price').text(number_format((basket_total_price - discount_amount + cargo_price).toFixed(2)));
                                }

                                Swal.fire({
                                    title: 'Kupon indiriminiz sepete eklenmiştir.',
                                    icon: 'success',
                                });

                                $('.cart-discount').remove();
                                $('.coupon-area').css('display', 'flex');

                                $('#delivery-btn').attr('href', base_url + 'order/delivery?coupon=' + coupon_code);
                            }
                            else
                            {
                                Swal.fire({
                                    title: 'Ürünlerin minumum tutarı: ' + number_format(data.CouponMinAmount) + '₺ olmalı',
                                    icon: 'warning',
                                });
                            }
                        }
                        else
                        {
                            Swal.fire({
                                title: 'Böyle bir kupon bulunmamaktadır.',
                                icon: 'warning',
                            });
                        }

                        $('[name="CouponCode"]').val('');
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });
            }
        }

        function update(basket_id, type = '', prev_val = '')
        {
            let notify_type = '';
            let message = '';
            let icon = '';
            let formData = new FormData($('#form-basket-' + basket_id)[0]);

            $.ajax({
                type: "POST",
                url: base_url + 'ajax/update_basket/' + basket_id,
                data: formData,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function (data)
                {
                    $('#preloader').fadeOut();

                    let quantity = parseInt($('[name="ProductOptionQuantity['+basket_id+']"]').val());
                    message = data.message;

                    if(data.status) 
                    {
                        notify_type = 'success';
                        icon = 'la la-check';

                        let cargo_price = 9.89;
                        let new_basket_total_price = 0;
                        let latest_price = parseFloat($('[name="ProductOptionLatestPrice['+basket_id+']"]').val());
                        let product_total_price = quantity * latest_price;
                        let basket_total_price = parseFloat($('[name="BasketTotalPrice"]').val());
                        $('#product-total-price-' + basket_id).text(number_format(product_total_price.toFixed(2)));

                        if (type == 'plus')
                        {
                            new_basket_total_price = (basket_total_price + latest_price).toFixed(2);
                            
                        }
                        else if(type == 'minus')
                        {
                            new_basket_total_price = (basket_total_price - latest_price).toFixed(2);
                        }
                        else
                        {
                            old_basket_total_price = basket_total_price - (latest_price * prev_val);
                            new_basket_total_price = (old_basket_total_price + product_total_price).toFixed(2);
                            $('.quantity-warning').css('display', 'none');
                        }

                        $('#basket-total-price').text(number_format(new_basket_total_price));
                        $('[name="BasketTotalPrice"]').val(new_basket_total_price);

                        if (new_basket_total_price >= 140) {
                            cargo_price = 0;
                            $('#cargo-price').text('0.00');
                        } else{
                            $('#cargo-price').text('9.89');
                        }

                        if (coupon_amount > 0)
                        {
                            new_basket_total_price = parseFloat(new_basket_total_price) + cargo_price - coupon_amount;
                        }
                        else if(coupon_rate > 0)
                        {
                            let discount_amount = (new_basket_total_price / 100) * coupon_rate;
                            new_basket_total_price = parseFloat(new_basket_total_price) + cargo_price - discount_amount;
                        }
                        else
                        {
                            new_basket_total_price = parseFloat(new_basket_total_price) + cargo_price;   
                        }

                        $('#basket-cargo-inclusive-price').text(number_format(parseFloat(new_basket_total_price).toFixed(2)));
                    }
                    else
                    {
                        notify_type = 'danger';
                        icon = 'la la-close';

                        if (type == 'plus')
                        {
                            $('[name="ProductOptionQuantity['+basket_id+']"]').val(quantity - 1);
                        }
                        else if(type == 'minus')
                        {
                            $('[name="ProductOptionQuantity['+basket_id+']"]').val(quantity + 1);
                        }
                    }
                    
                    $.notify(
                        {
                            icon: icon,
                            message: message
                        },
                        {
                            type: notify_type,
                            animate: {
                                enter: 'animated fadeInRight',
                                exit: 'animated fadeOutRight'
                            }
                        }
                    );
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Sepetteki ürünü güncellerken bir sorun oluştu.');
                }
            });
        }

        function destroy(basket_id)
        {
            $('#preloader').fadeIn();

            let notify_type = '';
            let message = '';
            let icon = '';

            $.ajax({
                url : base_url + 'ajax/destroy_basket/' + basket_id,
                type: 'DELETE',
                success: function(data)
                {
                    $('#preloader').fadeOut();
                    
                    data = JSON.parse(data);
                    message = data.message;

                    if(data.status)
                    {
                        notify_type = 'success';
                        icon = 'la la-check';

                        let cargo_price = 9.89;
                        let new_basket_total_price = 0;
                        let basket_total_price = parseFloat($('[name="BasketTotalPrice"]').val());
                        let quantity = parseInt($('[name="ProductOptionQuantity['+basket_id+']"]').val());
                        let latest_price = parseFloat($('[name="ProductOptionLatestPrice['+basket_id+']"]').val());
                        let product_total_price = quantity * latest_price;
                        new_basket_total_price = parseFloat(basket_total_price - product_total_price);

                        if (new_basket_total_price >= 140) {
                            cargo_price = 0;
                            $('#cargo-price').text('0.00');
                        } else{
                            $('#cargo-price').text('9.89');
                        }

                        $('#basket-total-price').text(number_format(new_basket_total_price.toFixed(2)));
                        $('[name="BasketTotalPrice"]').val(new_basket_total_price);
                        new_basket_total_price = new_basket_total_price != 0 ? (parseFloat(new_basket_total_price) + cargo_price) : 0;
                        $('#basket-cargo-inclusive-price').text(number_format(new_basket_total_price.toFixed(2)));
                        $('#basket-count').text(data.basket_total_product);
                    }
                    else
                    {
                        notify_type = 'danger';
                        icon = 'la la-close';
                    }

                    $.notify(
                        {
                            icon: icon,
                            message: message
                        },
                        {
                            type: notify_type,
                            animate: {
                                enter: 'animated fadeInRight',
                                exit: 'animated fadeOutRight'
                            }
                        }
                    );
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Sepetteki ürünü silerken bir sorun oluştu.');
                }
            });
            $('#product-item-' + basket_id).slideUp(300, function() { $(this).remove(); });
        }

    </script>


</body>
	
</html>