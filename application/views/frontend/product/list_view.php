<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php
    $category = $category_name;
    if (empty($category_name)) {
        $category = 'Tüm Ürünler';
    }
    ?>

    <meta name="description" content="<?= $category ?>">
    <meta name="keywords" content="<?= $category ?>">
    <title><?= $category . ' | petshopevinde.com' ?></title>

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <?php
    $get_tags = $this->input->get('tag');
    $tag_arr = [];
    if (!empty($get_tags)) {
        $tag_arr = explode('-', $get_tags);
    }
    ?>

    <div class="sub-page">
        <div class="container">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>">Ana Sayfa</a></li>
                    <li class="breadcrumb-item"><?= $category ?></li>
                </ol>
            </nav>

            <div class="row">

                <div class="col-md-3">

                    <form action="" id="filter-form" class="filter">

                        <h2 class="d-none">Kategoriler:</h2>
                        <div class="filter-box brand-filter d-none">
                            <div id="side-menu-accordion">
                                <!-- menu -->
                            </div>
                        </div>
                        <h2>Ürünleri Filtrele:</h2>

                        <!-- product filter start -->
                        <?php if ($options_group) : ?>

                            <?php $option_group_id = 0; ?>
                            <?php foreach ($options_group as $key => $option_group) : ?>

                                <?php if ($option_group_id != $option_group['OptionGroupContentID']) : ?>
                                    <?php if ($option_group_id != 0) : ?>
                                        </div>
                                    <?php endif ?>
                                    <?php $option_group_id = $option_group['OptionGroupContentID']; ?>
                                    <div class="filter-box brand-filter <?= $option_group['GroupSlug'] ?>">
                                        <h4><?= $option_group['GroupName'] ?>:</h4>
                                    <?php endif ?>

                                    <div class="custom-control custom-radio">
                                        <input type="checkbox" class="custom-control-input checkbox-filter" id="option-<?= $option_group['OptionContentID'] ?>" name="<?= $option_group['GroupSlug'] ?>" value="<?= $option_group['OptionContentID'] ?>">
                                        <label class="custom-control-label" for="option-<?= $option_group['OptionContentID'] ?>">
                                            <?= $option_group['OptionName'] ?>
                                        </label>
                                    </div>

                            <?php endforeach ?>

                                    </div>

                        <?php endif ?>
                        <!-- product filter end -->

                        <div class="filter-box d-block">
                            <h4>Fiyat Aralığı:</h4>
                            <div id="nonlinear"></div>
                            <div class="d-flex w-100 justify-content-between">
                                <span class="example-val" id="lower-value"></span>
                                <span class="example-val" id="upper-value"></span>
                            </div>
                        </div>
                        <a href="javascript:void(0)" onclick="filter_assign()" class="btn btn-black btn-block">UYGULA</a>

                    </form>

                    <input type="hidden" name="price_min" value="0">
                    <input type="hidden" name="price_max" value="0">

                </div>

                <div class="col-md-9">

                    <div class="tag-list">

                        <?php if ($this->input->get('search')) : ?>
                            <div class="item">
                                <span>"<?= $this->input->get('search') ?>" için bulunan sonuçlar</span>
                                <a href="javascript:void(0)" onclick="search_remove('<?= url_param_render($this->input->get('search')) ?>')" class="tag-remove">
                                    <i class="las la-times"></i>
                                </a>
                            </div>
                        <?php endif ?>

                        <?php if (!empty($get_tags)) : ?>
                            <?php foreach ($tag_arr as $key => $value) : ?>
                                <div class="item">
                                    <span><?= $value ?></span>
                                    <a href="javascript:void(0)" onclick="tag_remove('<?= url_param_render($value) ?>')" class="tag-remove">
                                        <i class="las la-times"></i>
                                    </a>
                                </div>
                            <?php endforeach ?>
                        <?php endif ?>

                        <?php if (!empty($this->input->get('filter'))) : ?>
                            <?php foreach (json_decode($this->input->get('filter'), true) as $key => $value) : ?>
                                <div class="item">
                                    <span><?= option_name($value['value']) ?></span>
                                    <a href="javascript:void(0)" onclick="filter_remove('<?= $value['value'] ?>','<?= $value['value'] ?>')" class="tag-remove">
                                        <i class="las la-times"></i>
                                    </a>
                                </div>
                            <?php endforeach ?>
                        <?php endif ?>

                    </div>

                    <div class="sort-filter">
                        <div class="form-group">
                            <select class="form-control" id="sort-filter" onchange="sort_filter(this.value)">
                                <option value="">Sıralama Seçiniz</option>
                                <option value="fiyata-gore-artan">Fiyata Göre (Artan)</option>
                                <option value="fiyata-gore-azalan">Fiyata Göre (Azalan)</option>
                                <option value="urun-adina-gore-a-z">Ürün Adına Göre (A › Z)</option>
                                <option value="urun-adina-gore-z-a">Ürün Adına Göre (Z ‹ A)</option>
                            </select>
                        </div>
                    </div>

                    <div class="product-list">
                        <div class="row products">

                            <?php if ($products_by_category) {
                                $currency = $this->session->userdata('UserCurrency');
                                $currency_icon = $this->session->userdata('UserCurrencyCode');
                            ?>

                                <?php foreach ($products_by_category as $key => $product) : ?>
                                    <div class="col-md-4">
                                        <div class="item <?=$product['ProductOptionStockStatus'] == 0 ? 'out-of-stock' : '' ?>">
                                            <a href="<?= base_url() . 'urun/' . $product['ProductSlug'] ?>" class="product-list-item">
                                                <div class="name"><?= $product['ProductName'] ?></div>
                                                <div class="image">

                                                    <?php
                                                    if (!empty($product['ProductPhoto'])) {
                                                        $image = SERVER_URL . 'resize/xs/' . $product['ProductPhoto'];
                                                    } elseif (!empty($product['PhotoName'])) {
                                                        $image = SERVER_URL . 'resize/xs/' . $product['PhotoName'];
                                                    } else {
                                                        $image = SERVER_URL . 'upload/product/null-photo.png';
                                                    }
                                                    ?>

                                                    <img src="<?= $image ?>" alt="<?= $product['PhotoTitle'] ? $product['PhotoTitle'] : $product['ProductPhotoAlt'] ?>">

                                                    <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                                        <span class="discount">-%<?= discount_render($product['ProductOption' . $currency . 'DiscountRate']) ?></span>
                                                    <?php endif ?>
                                                </div>
                                                <div class="add-to-cart">
                                                    <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                                    <span class="amount" id="product-discount-price">
                                                        <?= number_format_render($product['ProductOption' . $currency . 'VatInclusivePrice']) ?>
                                                    </span> 
                                                    <?php endif ?>

                                                    <?php if ($product['ProductOptionQuantity'] > 0 || $product['ProductOptionUnlimited'] == 1) { ?>
                                                    <div class="price"> <?= number_format_render($product['ProductOption' . $currency . 'LatestPrice']) ?>
                                                        <span><?= $currency_icon ?></span>
                                                    </div>
                                                    <?php } else { ?>
                                                    <div class="price">STOKTA YOK</div>
                                                    <?php } ?>
                                                    
                                                    <div class="go-to-product">ÜRÜNÜ İNCELE<i class="las la-long-arrow-alt-right"></i></div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                <?php endforeach ?>

                            <?php } else { ?>

                                <div class="col-md-12">
                                    <h6 class="p-3 text-center">Ürün bulunamadı</h6>
                                </div>

                            <?php } ?>

                        </div>

                        <!-- Show pagination links (start) -->
                        <?php
                        if (@$links) {
                            foreach ($links as $link) {
                                echo $link;
                            }
                        }
                        ?>
                        <!-- Show pagination links (end) -->

                    </div>

                </div>
        </div>
    </div>
    </div>

    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/14.6.0/nouislider.min.js"></script>

    <script type="text/javascript">
        var categories = <?= $categories ?>;
        var parent_category_slug = '<?= $parent_category_slug ?>';
        var tree_html = '';
        var path_arr = window.location.pathname.split('/');
        var filter_parameter = '<?= $this->input->get("filter") ?>';
        var sort_parameter = '<?= $this->input->get("sort") ?>';
        var get_parameters = '';

        if (filter_parameter != '') {
            $.each(JSON.parse(filter_parameter), function(key, val) {
                $('#option-' + val.value).attr('checked', 'checked');
            });
        }

        if (sort_parameter != '' || sort_parameter != '0') {
            $('#sort-filter').val(sort_parameter);
        }

        treeRadio(categories, 1);

        function treeRadio(dataArray, parentID) {
            let slug = '';
            let parent_id = '';

            if (dataArray[parentID]) {
                tree_html += '<ul>';

                $.each(dataArray[parentID], function(index, value) {
                    parent_id = index;
                    slug = base_url + 'kategori/' + value['CategorySlug'];

                    if (parentID == 1) {
                        tree_html += '<li class="' + value['CategorySlug'] + '">' + '<h3><a href="javascript:void(0)">' + value['CategoryName'] + '</a></h3>';

                        treeRadio(dataArray, parent_id);

                        tree_html += '</li>';
                    } else {
                        tree_html += '<li class="' + value['CategorySlug'] + '">' + '<a href="' + slug + get_parameters + '">' + value['CategoryName'] + '</a>';

                        treeRadio(dataArray, parent_id);

                        tree_html += '</li>';
                    }

                });

                tree_html += '</ul>';
            }

            $('#side-menu-accordion').html(tree_html);

            $("#side-menu-accordion a").click(function() {
                var link = $(this);
                var closest_ul = link.closest("ul");
                var parallel_active_links = closest_ul.find(".active")
                var closest_li = link.closest("li");
                var link_status = closest_li.hasClass("active");
                var count = 0;

                closest_ul.find("ul").slideUp(function() {
                    if (++count == closest_ul.find("ul").length)
                        parallel_active_links.removeClass("active");
                });

                if (!link_status) {
                    closest_li.children("ul").slideDown();
                    closest_li.addClass("active");
                }
            })

            if (parent_category_slug) {
                $('.' + parent_category_slug).addClass('active');
            }

            if (path_arr[2]) {
                $('.' + path_arr[2]).addClass('active');
            }
        }

        function filter_assign() {
            let path = window.location.pathname;
            let filter_param = JSON.stringify($("#filter-form").serializeArray());
            let price_min = $('[name="price_min"]').val();
            let price_max = $('[name="price_max"]').val();
            let tag_param = getUrlParameter('tag');
            let search_param = getUrlParameter('search');
            let sort_param = getUrlParameter('sort');
            let new_filter = '';
            let new_price = '';
            let new_tag = '';
            let new_search = '';
            let new_sort = '';

            new_filter = '?filter=' + filter_param;

            if (price_min && price_max) {
                if (price_min < 0) {
                    price_min = 0;
                }
                if (price_max < 0) {
                    price_max = 0;
                }
                new_price = '&min=' + price_min + '&max=' + price_max;
            }

            if (tag_param) {
                new_search = '&tag=' + tag_param;
            }

            if (search_param) {
                new_search = '&search=' + search_param;
            }

            if (sort_param) {
                new_sort = '&sort=' + sort_param;
            }

            window.location.href = path + new_filter + new_price + new_tag + new_search + new_sort;
        }

        function tag_assign(value) {
            let path = window.location.pathname;
            let tag_param = getUrlParameter('tag') ? getUrlParameter('tag') + '-' : '';
            let search_param = getUrlParameter('search');
            let filter_param = getUrlParameter('filter');
            let new_tag = '';
            let new_search = '';
            let new_filter = '';

            new_tag = '?tag=' + tag_param + value;

            if (filter_param) {
                new_path += '&filter=' + filter_param;
            }

            if (search_param) {
                new_path += '&search=' + search_param;
            }

            if (sort_param) {
                new_path = '&sort=' + sort_param;
            }

            window.location.href = path + new_tag + new_search + new_filter;
        }

        function filter_remove(name, value) {
            let path = window.location.pathname;
            let filter_param = getUrlParameter('filter');
            let tag_param = getUrlParameter('tag');
            let search_param = getUrlParameter('search');
            let sort_param = getUrlParameter('sort');
            let new_filter = '';
            let new_path = '';

            if (filter_param) {
                $.each(JSON.parse(filter_param), function(key, val) {
                    if (value != val.value) {
                        new_filter += '{"name":"' + val.name + '","value":"' + val.value + '"},';
                    }
                });

                new_filter = new_filter.replace(/,\s*$/, "");
                filter_param = '[' + new_filter + ']';

                if (new_filter) {
                    new_path = '?filter=' + filter_param;

                    if (tag_param) {
                        new_path += '&tag=' + tag_param;
                    }

                    if (search_param) {
                        new_path += '&search=' + search_param;
                    }

                    if (sort_param) {
                        new_path += '&sort=' + sort_param;
                    }
                } else if (tag_param) {
                    new_path = '?tag=' + tag_param;

                    if (search_param) {
                        new_path += '&search=' + search_param;
                    }

                    if (sort_param) {
                        new_path += '&sort=' + sort_param;
                    }
                } else if (search_param) {
                    new_path = '?search=' + search_param;

                    if (sort_param) {
                        new_path += '&sort=' + sort_param;
                    }
                } else if (sort_param) {
                    new_path = '?sort=' + sort_param;
                }

                window.location.href = path + new_path;
            }
        }

        function tag_remove(value) {
            let path = window.location.pathname;
            let filter_param = getUrlParameter('filter');
            let tag_param = getUrlParameter('tag');
            let search_param = getUrlParameter('search');
            let sort_param = getUrlParameter('sort');
            let tag_arr = tag_param.split('-');
            let new_path = '';

            if (tag_param) {
                tag_param = '';
                $.each(tag_arr, function(index, tag) {
                    if (tag != value) {
                        tag_param += tag + '-';
                    }
                });

                tag_param = tag_param.replace(/-$/, '');

                if (tag_param) {
                    new_path = '?tag=' + tag_param;

                    if (filter_param) {
                        new_path += '&filter=' + filter_param;
                    }

                    if (search_param) {
                        new_path += '&search=' + search_param;
                    }

                    if (sort_param) {
                        new_path += '&sort=' + sort_param;
                    }
                } else if (filter_param) {
                    new_path = '?filter=' + filter_param;

                    if (search_param) {
                        new_path += '&search=' + search_param;
                    }

                    if (sort_param) {
                        new_path += '&sort=' + sort_param;
                    }
                } else if (search_param) {
                    new_path = '?search=' + search_param;

                    if (sort_param) {
                        new_path += '&sort=' + sort_param;
                    }
                } else if (sort_param) {
                    new_path = '?sort=' + sort_param;
                }
            }

            window.location.href = path + new_path;
        }

        function search_remove(value) {
            let path = window.location.pathname;
            let filter_param = getUrlParameter('filter');
            let tag_param = getUrlParameter('tag');
            let sort_param = getUrlParameter('sort');
            let new_path = '';

            if (filter_param) {
                new_path = '?filter=' + filter_param;

                if (tag_param) {
                    new_path += '&tag=' + tag_param;
                }

                if (sort_param) {
                    new_path += '&sort=' + sort_param;
                }
            } else if (tag_param) {
                new_path = '?tag=' + tag_param;

                if (sort_param) {
                    new_path += '&sort=' + sort_param;
                }
            } else if (sort_param) {
                new_path = '?sort=' + sort_param;
            }

            window.location.href = path + new_path;
        }

        function sort_filter(value) {
            let path = window.location.pathname;
            let sort_param = value
            let filter_param = getUrlParameter('filter');
            let tag_param = getUrlParameter('tag');
            let search_param = getUrlParameter('search');
            let new_path = '';

            new_path = '?sort=' + sort_param;

            if (filter_param) {
                new_path += '&filter=' + filter_param;
            }

            if (tag_param) {
                new_path += '&tag=' + tag_param;
            }

            if (search_param) {
                new_path += '&search=' + search_param;
            }

            window.location.href = path + new_path;
        }

        var nonLinearSlider = document.getElementById('nonlinear');
        var price_min = getUrlParameter('min') ? getUrlParameter('min') : 0;
        var price_max = getUrlParameter('max') ? getUrlParameter('max') : 1000;

        noUiSlider.create(nonLinearSlider, {
            connect: true,
            behaviour: 'tap',
            start: [price_min, price_max],
            range: {
                'min': [0],
                // '10%': [30, 500],
                // '50%': [4000, 1000],
                'max': [1000]
            }
        });

        var nodes = [
            document.getElementById('lower-value'), // 0
            document.getElementById('upper-value') // 1
        ];

        nonLinearSlider.noUiSlider.on('update', function(values, handle, unencoded, isTap, positions) {
            nodes[handle].innerHTML = values[handle] + 'TL';
            $('[name="price_min"]').val(values[0]);
            $('[name="price_max"]').val(values[1]);
        });
    </script>

</body>

</html>