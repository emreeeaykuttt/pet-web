<?php  
    $tag_text = '';
    if (!empty($tags)) {
        foreach ($tags as $key => $tag) {
            $tag_text .= $tag . ', ';
        }
        $tag_text = rtrim($tag_text, ', ');
    }
?>
<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= $product['ProductName'] ?>">
    <meta name="keywords" content="<?= $tag_text ?>">
    <title><?= $product['ProductName'] ?> | petshopevinde.com</title>

    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/owlCarousel2/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/owlCarousel2/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/css/lightbox.min.css" rel="stylesheet" />

    <?php include(dirname(__DIR__) . '/inc/head.php'); ?>

</head>

<body>

    <?php include(dirname(__DIR__) . '/inc/nav_top.php'); ?>

    <div class="sub-page single-product">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>">Ana Sayfa</a></li>
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>kategori/<?= $product['CategorySlug'] ?>"><?= $product['CategoryName'] ?></a>
                    </li>
                    <li class="breadcrumb-item"><?= $product['ProductName'] ?></li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-md-6">
                    <div class="single-product-carousel owl-carousel owl-theme">

                        <?php
                        if (!empty($product['ProductPhoto'])) {
                            $image = SERVER_URL . 'resize/sm/' . $product['ProductPhoto'];
                            $imageBig = SERVER_URL . 'resize/lg/' . $product['ProductPhoto'];
                        } else {
                            $image = NULL;
                        }
                        ?>

                        <?php if (!empty($image)): ?>
                        <div class="item">
                            <a href="<?= $imageBig ?>" data-lightbox="single-item"><img src="<?= $image ?>" class="w-100" alt="<?= $product['ProductPhotoAlt'] ?>" /></a>
                        </div>
                        <?php endif ?>
                        
                        <?php if ($gallery) : ?>
                        <?php foreach ($gallery as $key => $value) : ?>
                        <div class="item">
                        <a href="<?= SERVER_URL . 'resize/lg/' .  $value['PhotoName'] ?>"  data-lightbox="multiple-item"><img src="<?= SERVER_URL . 'resize/sm/' .  $value['PhotoName'] ?>" class="w-100" alt="<?= $value['PhotoTitle'] ?>" /></a>
                        </div>
                        <?php endforeach ?>
                        <?php endif ?>

                        <?php if (empty($image) && empty($gallery)): ?>
                        <div class="item">
                            <img src="<?= SERVER_URL . 'upload/product/null-photo.png' ?>" class="w-100" />
                        </div>
                        <?php endif ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <form action="#" id="form-product" class="h-100">
                        <input type="hidden" value="<?= $product['ProductID'] ?>" name="ProductID" />
                        <input type="hidden" value="<?= $product['ProductContentID'] ?>" name="ProductContentID" />
                        <input type="hidden" value="<?= $product['ProductOptionID'] ?>" name="ProductOptionID" />
                        <input type="hidden" value="<?= $product['ProductOptionContentID'] ?>" name="ProductOptionContentID" />
                        <div class="single-product-detail d-flex flex-column">
                            <h1>
                                <?= $product['ProductName'] ?>
                                <?php if (!empty($this->session->userdata('UserID')) && !empty($product['FavoriteID'])) { ?>
                                    <a href="javascript:void(0)" class="favorite-btn" id="product-favorite" onclick="destroy_favorite(<?= $product['FavoriteID'] . ', ' . $product['ProductID'] . ', ' . $product['ProductContentID'] ?>)">
                                        <i class="las la-thumbs-up"></i>
                                    </a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" class="favorite-btn" id="product-favorite" onclick="add_to_favorite(<?= $product['ProductID'] . ', ' . $product['ProductContentID'] ?>)">
                                        <i class="las la-heart"></i>
                                    </a>
                                <?php } ?>
                            </h1>
                            <?php 
                                $p_options = json_decode($product['ProductOptionDetails'], true); 
                                $brand = $p_options['marka']['group'];
                                $brand_desc = $p_options['marka']['option'];
                                $brand_id = $p_options['marka']['option_id'];
                            ?>
                            <p><?= $brand ?> : 
                                <a href='<?=base_url()?>kategori/tumu?filter=[{"name":"<?=$brand?>","value":"<?=$brand_id?>"}]'>
                                    <?= $brand_desc ?>
                                </a>
                            </p>
                            <p>Ürün Kodu : <?= $product['ProductOptionSKU'] ?></p>

                            <?php $option_content_id_arr = json_decode($product['ProductOptionOptionContentIDs'], true); ?>

                            <?php if ($option_groups): ?>
                            <?php foreach ($option_groups as $key => $group): ?>

                                <input type="hidden" name="ProductOptionGroupContentIDs[]" value="<?=$group['GroupContentID']?>">

                                <div class="mb-3 d-none">
                                    <h4><?=$group['GroupName']?></h4>

                                    <select 
                                        name="ProductOptionOptionContentIDs[]" 
                                        data-value="<?=$group['GroupContentID']?>" 
                                        class="group-select" 
                                        id="group<?=$group['GroupContentID']?>">
                                        <?php foreach ($join_option_groups as $option_groups): ?>
                                            <?php foreach ($option_groups as $key => $option): ?>
                                                <?php if ($group['GroupContentID'] == $option['OptionGroupContentID'] && in_array($option['OptionContentID'], $options)): ?>
                                                    <option 
                                                        value="<?=$option['OptionContentID']?>" 
                                                        data-data='{"color": "<?=$option['OptionColor']?>"}'
                                                        <?=in_array($option['OptionContentID'], $option_content_id_arr) ? 'selected' : ''?>>
                                                            <?=$option['OptionName']?>
                                                    </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endforeach ?>
                                    </select>

                                </div>

                            <?php endforeach ?>
                            <?php endif ?>

                            <hr class="w-100 mb-4">
                            <?php
                            $currency = $this->session->userdata('UserCurrency');
                            $currency_icon = $this->session->userdata('UserCurrencyCode');

                            if ($product['ProductOption' . $currency . 'LatestPrice'] < $product['ProductOption' . $currency . 'VatInclusivePrice']) {
                                $discount_display = 'block';
                            } else {
                                $discount_display = 'none';
                            }
                            ?>

                            <?php if ($product['ProductOption' . $currency . 'LatestPrice'] >= FREE_SHIPPING_PRICE): ?>
                            <div class="free-shipping">
                                <span>ÜCRETSİZ<br />KARGO</span>
                            </div>
                            <?php endif ?>

                            <?php if ($product['ProductOptionQuantity'] > 0 || $product['ProductOptionUnlimited'] == 1) { ?>
                            <div class="discount-price" style="display: <?= $discount_display ?>">
                                <span class="amount" id="product-discount-price">
                                    <?= number_format_render($product['ProductOption' . $currency . 'VatInclusivePrice']) ?>
                                </span>
                                <span class="currency"><?= $currency_icon ?></span>
                            </div>

                            <div class="latest-price">
                                <span id="product-latest-price">
                                    <?= number_format_render($product['ProductOption' . $currency . 'LatestPrice']) ?>
                                </span>
                                <span class="currency"><?= $currency_icon ?></span>
                            </div>
                            <?php } ?>

                            <div class="d-flex justify-content-between mt-auto">
                                <div class="d-flex">
                                    <button type="button" class="btn btn-light btn-minus" id="quantity-minus">
                                        <i class="la la-minus m-0"></i>
                                    </button>
                                    <input type="number" min="1" oninput="validity.valid||(value='');" name="ProductOptionQuantity" value="1" class="count">
                                    <button type="button" class="btn btn-light btn-plus" id="quantity-plus">
                                        <i class="la la-plus m-0"></i>
                                    </button>
                                </div>
                                <?php if ($product['ProductOptionQuantity'] == 0 && $product['ProductOptionUnlimited'] == 0) { ?>
                                    <a href="javascript:void(0)" onclick="false" class="btn add-to-cart" id="basket-btn" <?= $product['ProductOptionQuantity'] == 0 ? 'style="opacity: 0.3; cursor: not-allowed;"' : '' ?>>
                                        STOKTA YOK
                                    </a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="add_to_basket()" class="btn add-to-cart" id="basket-btn">
                                        <i class="la la-shopping-cart"></i> Sepete Ekle
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </form>

                </div>

                <div class="container detail-bottom mt-5">
                    <nav>
                        <div class="nav nav-tabs d-flex" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active show flex-fill text-center" data-toggle="tab" href="#aboutProduct" role="tab" aria-selected="true"><i class="la la-info"></i> ÜRÜN HAKKINDA</a>
                            <a class="nav-item nav-link flex-fill text-center" data-toggle="tab" href="#cancellation-and-refund-conditions" role="tab" aria-selected="false"><i class="las la-undo-alt"></i> İPTAL VE İADE ŞARTLARI</a>
                            <a class="nav-item nav-link flex-fill text-center" data-toggle="tab" href="#shipping-and-delivery-information" role="tab" aria-selected="false"><i class="la la-truck"></i> KARGO VE TESLİMAT BİLGİLERİ</a>
                        </div>
                    </nav>
                    <div class="tab-content border p-3" id="nav-tabContent">
                        <div class="tab-pane fade active show" id="aboutProduct" role="tabpanel" aria-labelledby="nav-home-tab">
                            <?= $product['ProductDescription'] ?>
                        </div>
                        <div class="tab-pane fade" id="cancellation-and-refund-conditions" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="tab-area">
                                <b>SİPARİŞ İPTALİ VE İADE ŞARTLARI</b><br />
                                <p>Kanvas tablo siparişlerinizde, siparişini onayladığınız ürünü 2 saat içinde iptal etme hakkına sahipsiniz. Sipariş panelinde gerekli iptal seçeneği, sipariş verildikten sonraki 2 saat boyunca aktif durumda olacaktır. Diğer ürünlerimizde ürün kargoya teslim edilene kadar siparişi iptal etme hakkına sahipsiniz.
                                    Yukarıdaki gibi durumlar ve daha farklı koşullar adına iptal, iade ve cayma hakları prosedürü Satış Sözleşmesi Madde 6 ve Madde 9’da açıklanmıştır. </p>
                                <b>Madde 6- Alıcının Hak ve Yükümlülükleri</b><br />
                                <p>6.1. Alıcı, sözleşmede kendisine yüklenen edimleri mücbir sebepler dışında eksiksiz yerine getirmeyi kabul ve taahhüt eder.</p>
                                <p>6.2. Alıcı, sipariş vermekle birlikte iş sözleşme hükümlerini kabul etmiş sayıldığını ve sözleşmede belirtilen ödeme şekline uygun ödemeyi yapacağını kabul ve taahhüt eder.</p>
                                <p>6.3. Alıcı, <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a> internet sitesinden satıcının isim, unvan, açık adres, telefon ve diğer erişim bilgileri, satışa konu malın temel nitelikleri, vergiler dahil olmak üzere satış fiyatı, ödeme sekli, teslimat koşulları ve masrafları vs. satışa konu mal ile ilgili tüm ön bilgiler ve “cayma” hakkının kullanılması ve bu hakkın nasıl kullanılacağı, şikayet ve itirazlarını iletebilecekleri resmi makamlar vs. konusunda açık , anlaşılır ve internet ortamına uygun şekilde bilgi sahibi olduğunu bu ön bilgileri elektronik ortamda teyit ettiğini kabul ve beyan eder.</p>
                                <p>6.4. Bir önceki maddeye bağlı olarak Alıcı, ürün sipariş ve ödeme koşullarının, ürün kullanım talimatlarının , olası durumlara karşı alınan tedbirlerin ve yapılan uyarıların olduğu <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a> sipariş/ödeme/kullanım prosedürü bilgilerini okuyup bilgi sahibi olduğunu ve elektronik ortamda gerekli teyidi verdiğini beyan eder.</p>
                                <p>6.5. Alıcı, üretim hattından geçen ürünleri, stoktan almadığı ve özel isteğine bağlı sipariş oluşturduğu için <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a> tarafından kaynaklanan hatalar (üretim kusurları, sipariş verilen koddan farklı bir ürünün üretilip teslim edilmesi vs) olmadığı sürece iade edemez. Alıcı, nakliye kaynaklı kusurları teslim aldığı anda tespit etme ve tutanak tutma mecburiyetindedir. Aksi durumlarda <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a> hiçbir şekilde sorumluluk kabul etmez.</p>
                                <p>6.6 Alıcı, hazır ürünler grubundaki ürünlerden, “ürünü görmek istiyorum” seçeneğiyle istediği ürünükapısına çağırabilir. Satıcı, kendi belirlediği tarihte ürünü alıcıya götürmek zorundadır. Alıcı, nakliye bedelini peşinen ödemekle mükelleftir. Alıcı teslim tarihini satıcının sunduğu randevu günlerinden seçecektir. Ziyaret gününü seçmiş alıcı, ziyaret gününde belirttiği adreste olmakla yükümlüdür. Alıcı, ziyaret gününden en az 1 iş günü önce tarih değişimi talebinde bulunabilir. Bu talebini yazılı veya sözlü olarak <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a>’un belirttiği iletişim kanallardan yerine getirebilir. Alıcı, mücbir sebepler veya önceden haber vermeksizin belirttiği adreste olmadığı zaman, satıcı, ürünü geri götürecektir. Alıcı tekrar gün talep ederse, yeniden nakliye ücreti ödemeyi de kabul etmiş olur. Alıcı, ziyaret gününde ürünü satın almak zorunda değildir, ancak her zaman nakliye bedelini ödemekle yükümlüdür.</p>
                                <b>Madde 9- Ürün İade ve Cayma Hakkına İlişkin Prosedür</b><br />
                                <b>Ürün İade:</b><br />
                                <p>Üretim hattından geçen ürünler, alıcının siparişi üzerine ona özel üretileceği için, satıcıdan kaynaklanan kusurlar (yanlış kodlu ürün üretimi, üretim hataları vs) ve/veya nakliyeden kaynaklanan kusurlar dışında iade veya değişim yapılamaz. Nakliyeden kaynaklanan kusurlar için alıcı ürünü aldığı anda, pakedi, teslim edenin yanında açmalı ve bir kusur varsa tutanak tutturmalıdır. Sevkiyatçı veya kargo şirketi personeli gittikten sonra yapılan itirazlar kabul edilemez. </p>
                                <p>385 sayılı vergi usul kanunu genel tebliği uyarınca iade işlemlerinin yapılabilmesi için alıcının mal ile birlikte teslim edilen satıcıya ait 2 adet faturanın alt kısmındaki iade bölümlerini eksiksiz ve doğru şekilde doldurduktan sonra imzalayarak bir nüshasını ürün ile birlikte satıcıya göndermesi diğer nüshasını da uhdesinde tutması gerekmektedir.</p>
                                <p>Alıcı hazır ürünler grubundan yaptığı alışverişlerde cayma hakkı ve iade hakkını kullanabilir. Cayma hakkı süresi alıcıya malın teslim edildiği günden itibaren başlar. İade edilen ürün veya ürünlerin geri gönderim bedeli alıcı tarafından karşılanmalıdır. İade edilen ürünlerin ambalajı ve pakedi zarar görmemiş olmalıdır. İade edilen ürün, teslim alındığı gibi kusursuz ve hatasız olmalıdır. Satıcı, iade aldığı üründe, teslim ederken olmayan kusurları yakalarsa ürünü iade almama hakkına sahiptir. </p>
                                <p>Alıcının istekleri ve/veya açıkça onun kişisel ihtiyaçları doğrultusunda hazırlanan ürünler için (Üretim hattından geçen ürünler) cayma hakkı ve iade söz konusu değildir.</p>
                                <p>Alıcının cayma hakkını kullanması halinde satıcı, cayma bildirimini içeren faturanın ürünle birlikte kendisine ulaşmasından itibaren en geç on gün içerisinde almış olduğu toplam bedeli ve varsa tüketiciyi borç altına sokan her türlü belgeyi tüketiciye hiçbir masraf yüklemeden iade edecektir.</p>
                                <p>Teslim alınmış olan ürünün değerinin azalması veya iadeyi imkânsız kılan bir nedenin varlığı cayma hakkının kullanılmasına engel değildir. Ancak değer azalması veya iadenin imkânsızlaşması tüketicinin kusurundan kaynaklanıyorsa satıcıya ürünün değerini veya değerindeki azalmayı tazmin etmesi gerekir.</p>
                                <p>Sehven alınan her ürün için de genel iade süresi 7 gündür. Bu süre içerisinde, Ambalajı açılmış, kullanılmış, tahrip edilmiş vesaire şekildeki ürünlerin iadesi kabul edilmez. Sehven alınmış ürünlerin iadesi, orijinal ambalajı açılmamış halde yapılmalıdır.</p>
                                <p>Sehven alınan üründe ve ambalajında herhangi bir açılma, bozulma, kırılma, tahrip, yırtılma, kullanılma ve sair durumlar tespit edildiği hallerde ve ürünün alıcıya teslim edildiği andaki hali ile iade edilememesi durumunda ürün iade alınmaz ve bedeli iade edilmez.</p>
                                <p>Ürün iadesi için, durum öncelikli olarak müşteri hizmetlerine telefon veya bilgi@petshopevinde.com e-posta adresi üzerinden iletilmelidir. Ürünün iade olarak gönderilme bilgisi, satıcı tarafından müşteriye iletilir. Bu görüşmeden sonra ürün iade ile ilgili bilgileri içeren fatura ile birlikte alıcı adresine teslimatı yapan Kargo şirketi kanalıyla satıcıya ulaştırmalıdır. Satıcıya ulaşan iade ürün iş bu sözleşmede belirtilen koşulları sağladığı takdirde iade olarak kabul edilir, geri ödemesi de alıcı kredi kartına/hesabına yapılır. Ürün iade edilmeden bedel iadesi yapılmaz. Kredi Kartına yapılan iadelerin kredi kartı hesaplarına yansıma süresi ilgili bankanın tasarrufundadır.</p>
                                <p>Alışveriş kredi kartı ile ve taksitli olarak yapılmışsa, kredi kartına iade prosedürü şu şekilde uygulanacaktır: Alıcı ürünü kaç taksit ile satın alma talebini iletmiş ise, Banka alıcıya geri ödemesini taksitle yapmaktadır. Satıcı,bankaya ürün bedelinin tamamını tek seferde ödedikten sonra, Banka poslarından yapılan taksitli harcamaların alıcının kredi kartına iadesi durumundakonuya müdahil tarafların mağdur duruma düşmemesi için talep edilen iade tutarları,yine taksitli olarak hamil taraf hesaplarına Banka tarafından aktarılır.Alıcının satış iptaline kadar ödemiş olduğu taksit tutarları, eğer iade tarihi ile kartın hesap kesim tarihleri çakışmazsa her ay karta 1(bir) iade yansıyacak ve alıcı iade öncesinde ödemiş olduğu taksitleri satışın taksitleri bittikten sonra, iade öncesinde ödemiş olduğu taksit sayısı kadar ay daha alacak ve mevcut borçlarından düşmüş olacaktır.</p>
                                <p>Kart ile alınmış mal ve hizmetin iadesi durumunda satıcı, Banka ile yapmış olduğu sözleşme gereği alıcıya nakit para ile ödeme yapamaz. Üye işyeri yani satıcı, bir iade işlemi söz konusu olduğunda ilgili yazılım aracılığı ile iadesini yapacak olup, üye işyeri yani satıcı ilgili tutarı Bankaya nakden veya mahsuben ödemekle yükümlü olduğundan yukarıda detayları belirtilen prosedür gereğince alıcıya nakit olarak ödeme yapılamamaktadır. Kredi kartına iade, alıcının Bankaya bedeli tek seferde ödemesinden sonra, Banka tarafından yukarıdaki prosedür gereğince yapılacaktır.</p>
                                <p>Alıcı havale/eft ile ürünü almışsa, satıcı tarafından aynı hesaba geri ödenecektir. </p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="shipping-and-delivery-information" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="tab-area">
                                <b>KARGO VE TESLİMAT BİLGİLERİ </b><br />
                                <p>Satın aldığınız ürünler sizlere güncel kargo kampanyasına bağlı kalınarak anlaşmalı kargomuz ile gönderilecektir. En geç 3 (üç) iş günü içinde ürünler sevk edilir. Kanvas tablo haricindeki siparişlerinizde tahmini teslimat süresi sipariş verdiğiniz ekranda size bildirilecektir. </p>
                                <p>Ürünlerimizi Kapıya Özel Teslim olarak da elinize ulaşmasını sağlayabilirsiniz. Kapıya Özel Teslim seçeneği ile seçilmiş ürünlerde teslim günü ve/veya teslim adresi değişiklikleri için 0212 474 1920'yi arayarak müşteri temsilcimize ulaşabilirsiniz.</p>
                                <p>Teslimat koşulları ile ilgili uygulanan prosedür Satış Sözleşmesi Madde 3, Madde 5, Madde 6 ve Madde 8’de açıklanmıştır. </p>
                                <b>Madde 3- Sözleşme Konusu Ürün/Ödeme/Teslimat Bilgileri</b><br />
                                <p>Elektronik ortamda alınan ürün/ürünlerin cinsi ve türü, miktarı, marka/modeli, satış bedeli, ödeme şekli, teslim alacak kişi, teslimat adresi, fatura bilgileri, kargo ücreti aşağıda belirtildiği gibidir.Fatura edilecek kişi ile sözleşmeyi yapan kişi aynı olmak zorundadır.Aşağıda yer alan bilgiler doğru ve eksiksiz olmalıdır. Bu bilgilerin doğru olmadığı veya noksan olduğu durumlardan doğacak zararları tamamıyla karşılamayı alıcı kabul eder ve ayrıca bu durumdan oluşabilecek her türlü sorumluluğu alıcı kabul eder.</p>
                                <p>SATICI gerekli gördüğü durumlarda, ALICI’nın vermiş olduğu bilgiler gerçekle örtüşmediğinde, siparişi durdurma hakkını saklı tutar. SATICI siparişte sorun tespit ettiği durumlarda ALICI’nın vermiş olduğu telefon, e-posta ve posta adreslerinden ALICI’ya ulaşamadığı takdirde siparişin yürürlüğe koyulmasını 15 (onbeş) gün süreyle dondurur.ALICI’nın bu süre zarfında SATICI ile konuyla ilgili olarak iletişime geçmesi beklenir. Bu süre içerisinde ALICI’dan herhangi bir cevap alınamazsa SATICI, her iki tarafın da zarar görmemesi için siparişi iptal eder.</p>
                                <b>Alınan Ürün/Ürünler</b><br />
                                <p>Satılan ürünler, satıcının inisiyatifinde olup, satıcı dilediği zamanistediği ürünü satıştan çekme, fiyat veya kampanya değiştirme hakkını saklı tutar. <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a> sitesinde satılan ürünler iki gruba ayrılır. </p>
                                <p>1) Üretim hattından geçen ürünler; kanvas tablolar ve kendin tasarla başlığı altındaki ürünlerin hepsini kapsar. </p>
                                <p>2) Hazır ürünler; yukarıda sayılan başlıklar haricindeki ürünler kapsar. </p>
                                </p>
                                Adı , kodu :  … adet<br />
                                 Toplam Satış Bedeli : …. -TL<br />
                                 Ödeme Şekli : Kredi Kartı/Banka Havalesi (EFT)<br />
                                 Teslim Şekli:<br /><br />
                                Teslim Edilecek Kişi :<br />
                                 Telefon numarası :<br />
                                 Teslim Edilecek Adres :<br />
                                 Fatura Edilecek Kişi/Kurum :<br />
                                 Fatura Adresi :<br />
                                 Vergi Dairesi :<br />
                                 Vergi Sicil Numarası :<br />
                                 Kargo Ücreti : … -TL</p>
                                <b>Madde 5- Satıcının Hak ve Yükümlülükleri</b><br />
                                <p>5.1. Satıcı, 4077 sayılı Tüketicilerin Korunması Hakkındaki Kanun ve Mesafeli Sözleşmelere Dair Yönetmelik hükümleri uyarınca sözleşmede kendisine yüklenen edimleri mücbir haller dışında eksiksiz yerine getirmeyi kabul ve taahhüt eder.</p>
                                <p>5.2. 18 (on sekiz) yaşından küçük kişiler <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a>’dan alışveriş yapamaz. Satıcı, alıcının sözleşmede belirttiği yaşının doğru olduğunu esas alacaktır. Ancak alıcının yaşını yanlış yazmasından dolayı satıcıya hiçbir şekilde sorumluluk yüklenemeyecektir.</p>
                                <p>5.2. Sistem hatalarından meydana gelen fiyat yanlışlıklarından <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a> sorumlu değildir. Buna istinaden satıcı, internet sitesindeki sistemden, dizayndan veya yasadışı yollarla internet sitesine yapılabilecek müdahaleler sebebiyle ortaya çıkabilecek tanıtım, fiyat hatalarından sorumlu değildir. Sistem hatalarına dayalı olarak alıcı satıcıdan hak iddiasında bulunamaz.</p>
                                <p>5.3. <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a>dankredi kartı (Visa, MasterCard , vs. ) ya da banka EFTsi/havalesiile alışveriş yapılabilir. Sipariş tarihinden itibaren bir hafta içinde kredi kartı ödemesi ve EFTsi/havalesi yapılmayan siparişler iptal edilir. Siparişlerin işleme alınma zamanı, siparişin verildiği an değil, kredi kartı hesabından gerekli tahsilatın yapıldığı ya da EFTnin/havalenin banka hesaplarına ulaştığı belirlenen andır. <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a> sadece kendi belirlediği ödeme kanallarıyla ödemeyi kabul eder. </p>
                                <p>5.4. Kapıya özel teslim siparişlerde, nakliye bedeli dahil tüm sipariş bedelinin ödenmesi şarttır. Telefon, e-posta veya diğer interaktif yollarla SATICI hiçbir şekilde bunun dışında bir şey talep edemez.</p>
                                <p>5.5.Alıcı yalnızca hazır ürünler grubundan aldığı ürünleri iade edebilir. Üretim hattından geçen ürünlerin siparişi, sipariş onayından itibaren alıcı'ya aittir. Satıcı, üretim hataları veya nakliyede oluşabilecek olumsuzluklar haricinde iade kabul edemez. </p>
                                <b>Madde 6- Alıcının Hak ve Yükümlülükleri</b><br />
                                <p>6.1. Alıcı, sözleşmede kendisine yüklenen edimleri mücbir sebepler dışında eksiksiz yerine getirmeyi kabul ve taahhüt eder.</p>
                                <p>6.2. Alıcı, sipariş vermekle birlikte iş sözleşme hükümlerini kabul etmiş sayıldığını ve sözleşmede belirtilen ödeme şekline uygun ödemeyi yapacağını kabul ve taahhüt eder.</p>
                                <p>6.3. Alıcı, <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a> internet sitesinden satıcının isim, unvan, açık adres, telefon ve diğer erişim bilgileri, satışa konu malın temel nitelikleri, vergiler dahil olmak üzere satış fiyatı, ödeme sekli, teslimat koşulları ve masrafları vs. satışa konu mal ile ilgili tüm ön bilgiler ve “cayma” hakkının kullanılması ve bu hakkın nasıl kullanılacağı, şikayet ve itirazlarını iletebilecekleri resmi makamlar vs. konusunda açık , anlaşılır ve internet ortamına uygun şekilde bilgi sahibi olduğunu bu ön bilgileri elektronik ortamda teyit ettiğini kabul ve beyan eder.</p>
                                <p>6.4. Bir önceki maddeye bağlı olarak Alıcı, ürün sipariş ve ödeme koşullarının, ürün kullanım talimatlarının , olası durumlara karşı alınan tedbirlerin ve yapılan uyarıların olduğu <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a> sipariş/ödeme/kullanım prosedürü bilgilerini okuyup bilgi sahibi olduğunu ve elektronik ortamda gerekli teyidi verdiğini beyan eder.</p>
                                <p>6.5. Alıcı, üretim hattından geçen ürünleri, stoktan almadığı ve özel isteğine bağlı sipariş oluşturduğu için <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a> tarafından kaynaklanan hatalar (üretim kusurları, sipariş verilen koddan farklı bir ürünün üretilip teslim edilmesi vs) olmadığı sürece iade edemez. Alıcı, nakliye kaynaklı kusurları teslim aldığı anda tespit etme ve tutanak tutma mecburiyetindedir. Aksi durumlarda <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a> hiçbir şekilde sorumluluk kabul etmez. <p>
                                <p>6.6 Alıcı, hazır ürünler grubundaki ürünlerden, “ürünü görmek istiyorum” seçeneğiyle istediği ürünükapısına çağırabilir. Satıcı, kendi belirlediği tarihte ürünü alıcıya götürmek zorundadır. Alıcı, nakliye bedelini peşinen ödemekle mükelleftir. Alıcı teslim tarihini satıcının sunduğu randevu günlerinden seçecektir. Ziyaret gününü seçmiş alıcı, ziyaret gününde belirttiği adreste olmakla yükümlüdür. Alıcı, ziyaret gününden en az 1 iş günü önce tarih değişimi talebinde bulunabilir. Bu talebini yazılı veya sözlü olarak <a href="<?= base_url() ?>" target="_blank">www.petshopevinde.com</a>’un belirttiği iletişim kanallardan yerine getirebilir. Alıcı, mücbir sebepler veya önceden haber vermeksizin belirttiği adreste olmadığı zaman, satıcı, ürünü geri götürecektir. Alıcı tekrar gün talep ederse, yeniden nakliye ücreti ödemeyi de kabul etmiş olur. Alıcı, ziyaret gününde ürünü satın almak zorunda değildir, ancak her zaman nakliye bedelini ödemekle yükümlüdür.</p>
                                <b>Madde 8- Sevkiyat/Teslimat Prosedürü</b><br />
                                <b>Sevkiyat:</b><br />
                                <p>Sipariş onayı mailinin gönderilmesiyle birlikte, ürün/ürünler satıcının anlaşmalı olduğu kargo Şirketine 3 (üç) iş günü içinde teslim edilir. Kapıya özel teslim seçeneği istenildiğinde, satıcının belirttiği teslimat günlerinden biri seçilecek, teslimat için satıcının belirttiği günler haricinde bir tarih isteniyorsa Alıcı, sipariş numarasıyla birlikte satıcının belirttiği iletişim kanallarından satıcıya ulaşılacaktır. </p>
                                <b>Teslimat:</b><br />
                                <p>Ürün/ürünler satıcının anlaşmalı olduğu kargo ile alıcının adresine teslim edilecektir. Ürünün kargoya verilme süresi, sipariş onayı mailinin gönderilmesinden ve sözleşmenin kurulmasından itibaren 2 (iki) - 3 (üç) iş günüdür. Alıcıya önceden yazılı olarak veya bir sürekli veri taşıyıcısıyla bildirilmek koşuluyla, bu süre en fazla on gün uzatılabilir.ALICI, kendisinin üye olurken veya üye olmadan belirtmiş olduğu adrese ve bu adreste teslim sırasında bulunan kişiye yapılan teslimatın, ALICI’ya yapılmış teslimat sayılacağını kabul etmektedir. </p>
                                <p>Ürünler, Kargo şirketlerinin adres teslimatı yapmadığı bölgelere telefon ihbarlı olarak gönderilir.</p>
                                <p>Kargo Şirketinin haftada bir gün teslimat yaptığı bölgelerde, sevk bilgilerindeki yanlışlık ve eksiklik olduğu hallerde, bazı sosyal olaylar ve doğal afetler gibi durumlarda belirtilen gün süresinde sarkma olabilir. Bu sarkmalardan dolayı alıcı satıcıya herhangi bir sorumluluk yükleyemez. Ürün, Alıcı’dan başka bir kişi/kuruluşa teslim edilecek ise, teslim edilecek kişi/kuruluşun teslimatı kabul etmemesinden, sevk bilgilerindeki yanlışlık ve/veya Alıcının yerinde olmamasından doğabilecek ekstra kargo bedellerinden satıcı sorumlu değildir. Belirtilen günler içeriğinde ürün/ürünler müşteriye ulaşmadıysa teslimat problemleri müşteri hizmetlerine <a href="mailto:bilgi@petshopevinde.com">bilgi@petshopevinde.com</a> e-posta adresi kullanılmak sureti ile derhal bildirilmelidir.</p>
                                <p>Zarar görmüş paket durumunda; zarar görmüş paketler teslim alınmayarak Kargo Şirketi yetkilisine tutanak tutturulmalıdır. Eğer Kargo Şirketi yetkilisi paketin hasarlı olmadığı görüşünde ise, paketin orada açılarak ürünlerin hasarsız teslim edildiğini kontrol ettirme ve durumun yine bir tutanakla tespit edilmesini isteme hakkı alıcıda vardır. Paket Alıcı tarafından teslim alındıktan sonra Kargo Şirketinin görevini tam olarak yaptığı kabul edilmiş olur. Paket kabul edilmemiş ve tutanak tutulmuş ise, durum, tutanağın Alıcı’da kalan kopyasıyla birlikte en kısa zamanda satıcı Müşteri Hizmetlerine bildirilmelidir.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container products">
                    <div class="title"><span>İlginizi Çekebilecek Ürünler</span></div>
                    <div class="product-carousel owl-theme owl-carousel">

                        <?php if ($products_by_category) {
                            $currency = $this->session->userdata('UserCurrency');
                            $currency_icon = $this->session->userdata('UserCurrencyCode');
                        ?>
                            <?php foreach ($products_by_category as $key => $product) : ?>
                                <a class="item" href="<?= base_url() . 'urun/' . $product['ProductSlug'] ?>">
                                    <div class="name"><?= $product['ProductName'] ?></div>
                                    <!-- <div class="desc">Yetişkin Köpek Maması 15KG</div> -->
                                    <div class="image">

                                        <?php
                                        if (!empty($product['ProductPhoto'])) {
                                            $image = SERVER_URL . 'resize/xs/' . $product['ProductPhoto'];
                                        } elseif (!empty($product['PhotoName'])) {
                                            $image = SERVER_URL . 'resize/xs/' . $product['PhotoName'];
                                        } else {
                                            $image = SERVER_URL . 'upload/product/null-photo.png';
                                        }
                                        ?>
                                        <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                            <span class="discount">-%<?= discount_render($product['ProductOption' . $currency . 'DiscountRate']) ?></span>
                                        <?php endif ?>
                                    </div>
                                    <img src="<?= $image ?>" alt="<?= $product['PhotoTitle'] ? $product['PhotoTitle'] : $product['ProductPhotoAlt'] ?>">

                                    <div class="add-to-cart">
                                        <?php if ($product['ProductOption' . $currency . 'DiscountRate'] > 0) : ?>
                                            <span class="amount" id="product-discount-price">
                                                <?= number_format_render($product['ProductOption' . $currency . 'VatInclusivePrice']) ?>
                                            </span> 
                                        <?php endif ?>
                                        <?php if ($product['ProductOptionQuantity'] > 0 || $product['ProductOptionUnlimited'] == 1) { ?>
                                        <div class="price"> <?= number_format_render($product['ProductOption' . $currency . 'LatestPrice']) ?>
                                            <span><?= $currency_icon ?></span>
                                        </div>
                                        <?php } else { ?>
                                        <div class="price">STOKTA YOK</div>
                                        <?php } ?>

                                        <div class="go-to-product">ÜRÜNÜ İNCELE<i class="las la-long-arrow-alt-right"></i></div>
                                    </div>

                                </a>
                            <?php endforeach ?>
                        <?php } ?>

                    </div>
                </div>

            </div>
        </div>
    </div>


    <?php include(dirname(__DIR__) . '/inc/footer.php'); ?>
    <?php include(dirname(__DIR__) . '/inc/script.php'); ?>
    <script src="<?php echo base_url() ?>assets/frontend/js/lightbox.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/owlCarousel2/js/owl.carousel.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('.product-carousel').owlCarousel({
                loop: true,
                margin: 20,
                autoplay: false,
                autoplayTimeout: 7000,
                autoplayHoverPause: true,
                nav: true,
                dots: false,
                animateOut: "fadeOut",
                animateIn: "fadeIn",
                navText: ["<i class='la la-angle-left'></i>", "<i class='la la-angle-right'></i>"],

                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    1000: {
                        items: 5
                    }
                }
            });

            $('.group-select').change(function() {
                product_option_change()
            });

            $('#quantity-plus').click(function() {
                let quantity = $('[name="ProductOptionQuantity"]').val();
                $('[name="ProductOptionQuantity"]').val(parseInt(quantity) + 1);
            });

            $('#quantity-minus').click(function() {
                let quantity = $('[name="ProductOptionQuantity"]').val();
                if (parseInt(quantity) > 1) {
                    $('[name="ProductOptionQuantity"]').val(parseInt(quantity) - 1);
                }
            });

        });

        function product_option_change() {
            $('#preloader').fadeIn();

            let notify_type = '';
            let message = '';
            let icon = '';
            let formData = new FormData($('#form-product')[0]);

            $.ajax({
                url: base_url + 'ajax/product_option_change',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    $('#preloader').fadeOut();

                    if (data) {
                        let product = data.product;
                        let gallery = data.gallery;

                        $('.single-product-carousel').html('');

                        if (gallery) {
                            $.each(data.gallery, function(count, item) {

                                $('.single-product-carousel').append(
                                    '<div class="item">' +
                                        '<img src="' + server_url + 'resize/xs/' + item.PhotoName + '" class="w-100" alt="' + item.PhotoTitle + '" />' +
                                    '</div>'
                                );

                            });
                        } else if (product.ProductPhoto) {
                            $('.single-product-carousel').append(
                                '<div class="item">' +
                                    '<img src="' + server_url + 'resize/xs/' +product.ProductPhoto + '" class="w-100" alt="' + product.ProductPhotoAlt + '" />' +
                                '</div>'
                            );
                        } else {
                            $('.single-product-carousel').append(
                                '<div class="item">' +
                                    '<img src="' + server_url + 'upload/product/null-photo.png" class="w-100" />' +
                                '</div>'
                            );
                        }

                        if (product.ProductOptionLatestPrice < product.ProductOptionVatInclusivePrice) {
                            $('.discount-price').css('display', 'block');
                        } else {
                            $('.discount-price').css('display', 'none');
                        }

                        $('#basket-btn').attr('onclick', 'add_to_basket()');
                        $('#basket-btn').attr('style', 'opacity: 1;');
                        $('#basket-info').text('');
                        $('#product-discount-price').text(number_format(product.ProductOptionVatInclusivePrice));
                        $('#product-latest-price').text(number_format(product.ProductOptionLatestPrice));
                        $('[name="ProductOptionID"]').val(product.ProductOptionID);
                        $('[name="ProductOptionContentID"]').val(product.ProductOptionContentID);
                    } else {
                        $('#basket-btn').attr('onclick', false);
                        $('#basket-btn').attr('style', 'opacity: 0.3; cursor: not-allowed;');
                        $('#basket-info').text('Bu varyasyonda ürün bulunmamktadır.');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error get data from ajax');
                }
            });
        }

        function add_to_basket() {
            let notify_type = '';
            let message = '';
            let icon = '';
            let formData = new FormData($('#form-product')[0]);
            $('#basket-btn').text('Sepete Ekleniyor...');
            $('#basket-btn').attr('onclick', false);

            $.ajax({
                type: "POST",
                url: base_url + 'ajax/add_to_basket',
                data: formData,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    message = data.message;
                    icon = data.status ? 'success' : 'warning';

                    Swal.fire({
                        position: 'center',
                        icon: icon,
                        text: message,
                        showCancelButton: false,
                        showConfirmButton: false,
                        timer: 4000,
                        footer:`<a class="btn btn-primary" href="<?=base_url('sepet')?>">SEPETE GİT <i class="la la-angle-right"></i></a>`

                    })

                    $('[name="ProductOptionQuantity"]').val('1');
                    $('#basket-btn').text('Sepete Ekle');
                    $('#basket-btn').attr('onclick', 'add_to_basket()');
                    $('#basket-count').text(data.basket_total_product);

                    // basket_view('open');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Ürün sepete eklenirken bir sorun oluştu.');
                }
            });
        }

        function add_to_favorite(product_id, product_content_id) {
            let notify_type = '';
            let message = '';
            let icon = '';

            $.ajax({
                url: base_url + 'ajax/add_favorite/' + product_id + '/' + product_content_id,
                type: 'GET',
                dataType: 'JSON',
                success: function(data) {
                    if (typeof(data.status) != "undefined" && data.status !== null) {
                        if (data.status == 'login') {
                            window.location.href = base_url + 'auth/login';
                        }
                    }

                    message = data.message;
                    icon = data.status ? 'success' : 'warning';

                    Swal.fire({
                        position: 'center',
                        icon: icon,
                        text: message,
                        showConfirmButton: false,
                        timer: 2000
                    });

                    $('#product-favorite').attr('onclick', 'destroy_favorite(' + data.FavoriteID + ', ' + product_id + ', ' + product_content_id + ' )');
                    $('#product-favorite').html('<i class="las la-thumbs-up"></i>');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Ürün favorilere eklenirken bir sorun oluştu.');
                }
            });
        }

        function destroy_favorite(favorite_id, product_id, product_content_id) {
            let notify_type = '';
            let message = '';
            let icon = '';

            $.ajax({
                url: base_url + 'ajax/destroy_favorite/' + favorite_id,
                type: 'GET',
                dataType: 'JSON',
                success: function(data) {
                    if (typeof(data.status) != "undefined" && data.status !== null) {
                        if (data.status == 'login') {
                            window.location.href = base_url + 'auth/login';
                        }
                    }

                    message = data.message;
                    icon = data.status ? 'success' : 'warning';

                    Swal.fire({
                        position: 'center',
                        icon: icon,
                        text: message,
                        showConfirmButton: false,
                        timer: 2000
                    });

                    $('#product-favorite').attr('onclick', 'add_to_favorite(' + product_id + ', ' + product_content_id + ')');
                    $('#product-favorite').html('<i class="las la-heart"></i>');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Ürün favorilere eklenirken bir sorun oluştu.');
                }
            });
        }
    </script>

</body>

</html>