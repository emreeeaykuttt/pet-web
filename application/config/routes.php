<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['category/(:any)'] = 'category/index/$1';
$route['kategori/(:any)'] = 'category/index/$1';
$route['category/(:any)/(:any)'] = 'category/index/$1/$2';
$route['kategori/(:any)/(:any)'] = 'category/index/$1/$2';
$route['product/(:any)'] = 'product/index/$1';
$route['urun/(:any)'] = 'product/index/$1';
$route['about'] = 'pages/about';
$route['hakkimizda'] = 'pages/about';
$route['privacy-policy'] = 'pages/privacy_policy';
$route['gizlilik-politikasi'] = 'pages/privacy_policy';
$route['user-agreement'] = 'pages/user_agreement';
$route['kullanici-sozlesmesi'] = 'pages/user_agreement';
$route['distance-sales-contract'] = 'pages/distance_sales_contract';
$route['mesafeli-satis-sozlesmesi'] = 'pages/distance_sales_contract';
$route['cancellation-and-refund-conditions'] = 'pages/cancellation_and_refund_conditions';
$route['iptal-ve-iade-sartlari'] = 'pages/cancellation_and_refund_conditions';
$route['shipping-and-delivery-information'] = 'pages/shipping_and_delivery_information';
$route['kargo-ve-teslimat-bilgileri'] = 'pages/shipping_and_delivery_information';
$route['cookie-policy'] = 'pages/cookie_policy';
$route['cerez-politikasi'] = 'pages/cookie_policy';
$route['contact'] = 'pages/contact';
$route['iletisim'] = 'pages/contact';
$route['sitemap.xml'] = 'home/sitemap';
$route['sepet'] = 'basket';
$route['profil'] = 'profile';
$route['profil/adreslerim'] = 'profile/address';
$route['profil/siparislerim'] = 'profile/orders';
$route['profil/kuponlarim'] = 'profile/coupons';
$route['profil/favorilerim'] = 'profile/favorites';

$route['uyelik/giris-yap'] = 'auth/login';
$route['uyelik/uye-ol'] = 'auth/register';
$route['uyelik/misafir'] = 'auth/guest';
$route['uyelik/cikis-yap'] = 'auth/logout';
$route['uyelik/sifremi-unuttum'] = 'auth/forgot';
