const { src, dest, watch } = require('gulp');
const minifyJs = require('gulp-uglify');
const concat = require('gulp-concat')
const minifyCss = require('gulp-css')

const bundleCss = () => {
    return src([
            './assets/plugins/bootstrap-v4/css/bootstrap.min.css',
            './assets/frontend/css/owl.carousel.min.css',
            './assets/frontend/css/nouislider.min.css',
            './assets/frontend/css/owl.theme.default.min.css',
            './assets/frontend/css/animate.min.css',
        ])
        .pipe(minifyCss())
        .pipe(concat('bundle.css'))
        .pipe(dest('./dist/'))
}
const bundleJs = () => {
    return src([
            './assets/plugins/jquery/jquery-3.4.1.min.js',
            './assets/plugins/bootstrap-v4/js/tether.min.js',
            './assets/plugins/bootstrap-v4/js/popper.min.js',
            './assets/plugins/bootstrap-v4/js/bootstrap.min.js',
            './assets/plugins/bootstrap-v4/js/bootstrap-notify.min.js',
            './assets/plugins/sweetalert2/js/sweetalert2.all.min.js',
            './assets/frontend/js/headroom.min.js',
            './assets/frontend/js/gsap.min.js',
            './assets/frontend/js/jQuery.headroom.js',
            './assets/frontend/js/owl.carousel.min.js',
            './assets/frontend/js/owl.carousel2.thumbs.min.js',
            './assets/frontend/js/InstagramFeed.min.js',
            './assets/frontend/js/js.cookie.min.js',
        ])
        .pipe(minifyJs())
        .pipe(concat('bundle.js'))
        .pipe(dest('./dist/'))
}

const devWatch = () => {
    watch('./assets/frontend/js/*.js')
}
exports.bundleJs = bundleJs;
exports.bundleCss = bundleCss;
exports.devWatch = devWatch